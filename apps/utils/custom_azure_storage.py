from django.conf import settings
from storages.backends.azure_storage import AzureStorage


class AzureMediaStorage(AzureStorage):
    azure_container = settings.AZURE_CONTAINER_PUBLIC
    expiration_secs = None
