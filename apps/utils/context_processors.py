from django.conf import settings

from apps.tm.models import Major
from apps.tm.models import Session
from apps.tm.views import current_session


def settings_context(_request):
    return {"settings": settings}


def select_session(_request):
    session = current_session(_request)
    sessions = Session.objects.all().order_by("start_date")
    return {"current_session": session, "sessions": sessions}


def set_current_major(request):
    major_id = request.session.get("tm_major_id", None)
    major = None
    if major_id:
        major = Major.objects.get(pk=major_id)
    elif request.user and request.user.is_authenticated:
        major = request.user.majors.first()
        if major:
            request.session["tm_major_id"] = major.pk
    return {"current_major": major}
