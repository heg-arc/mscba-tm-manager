from django.conf import settings
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.utils import translation


def set_language(request, user_language):
    translation.activate(user_language)
    if request.headers.get("referer", None):
        response = HttpResponseRedirect(request.headers.get("referer"))
    else:
        response = HttpResponseRedirect(reverse("tm:dashboard"))
    response.set_cookie(settings.LANGUAGE_COOKIE_NAME, user_language)
    if request.user.is_authenticated:
        request.user.language = user_language
        request.user.save()
    return response
