{% load static i18n %}

{% block subject %}[MScBA] {% if update %}UPDATE{% endif %} {% trans "Master's thesis defense" %} - {{ defense.master_thesis.attempt.student.name }} - {{ defense.defense_date|date:"SHORT_DATETIME_FORMAT" }}{% endblock %}

{% block html %}
  {% if update %}<p>######  UPDATE  ######</p>{% endif %}

  {% blocktranslate %}
  <p>Dear student,<br/>
     Dear expert,<br/>
     Dear supervisor,</p>
  {% endblocktranslate %}

  <p>
    {% blocktranslate with student=defense.master_thesis.attempt.student.name %}
    We are pleased to inform you that the date for the defense of {{ student }} has been set.
    {% endblocktranslate %}
  </p>

  <dl>
    <dt>{% translate "place"|capfirst %}:</dt>
    {% if defense.place == "Other" %}
      <dd>{{ defense.other_location }}</dd>
    {% else %}
      <dd>{{ defense.get_place_display }}</dd>
      <dt>{% translate "room"|capfirst %}:</dt>
      <dd>{% if defense.room %}{{ defense.room }}{% else %}{% translate 'room to be confirmed'|capfirst %}{% endif %}</dd>
    {% endif %}
    <dt>{% trans "date"|capfirst %}:</dt>
    <dd>{{ defense.defense_date|date:"SHORT_DATE_FORMAT" }} {{ defense.defense_date|time:"H:i" }}-{{ defense.defense_date_end|time:"H:i" }}</dd>
  </dl>

  <p>
    {% blocktranslate with date=defense.master_thesis.attempt.current_calendar.thesis_end|date:"SHORT_DATE_FORMAT" %}
    You will receive the necessary documents as soon as the master's thesis has been handed in (i.e. after {{ date }}). If you have requested a hard copy, this will be sent directly to the address indicated a few days after the date of submission.
    {% endblocktranslate %}
  </p>

  <p>
    {% trans 'Best regards' %}<br/>
    {% if defense.master_thesis.attempt.major.secretary %}
      {{ defense.master_thesis.attempt.major.secretary }}
    {% endif %}
  </p>

  {% include "templated_email/footer.email" with major=defense.master_thesis.attempt.major %}
{% endblock %}
