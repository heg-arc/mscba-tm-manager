{% load static i18n %}

{% block subject %}[MScBA] {% trans "reminder"|upper %} > {% trans "submission of the master thesis"|capfirst %} - {{ master_thesis.attempt.student.name }} - {{ master_thesis.attempt.current_calendar.thesis_end|date:"SHORT_DATETIME_FORMAT" }}{% endblock %}

{% block html %}
  <p>{% translate "Dear student"|capfirst %},<br>
     {% translate "Dear supervisor"|capfirst %}</p>

  <p>{% blocktranslate with deadline=master_thesis.attempt.current_calendar.thesis_end|date:"SHORT_DATETIME_FORMAT" submit_time=master_thesis.attempt.current_calendar.thesis_end|time:"H:i"%}
    We would like to remind you that the deadline for submitting the master's thesis is {{ deadline }}. Please submit the Master Thesis tomorrow before
    {{ submit_time }} using the link below.
    {% endblocktranslate %}
  </p>

  <h3><a href="{{ url }}">{% trans "sumbit the master thesis"|upper %}</a></h3>

  <p>
     {% translate "Please note that you need to submit the following items" %}:
     <ul>
        <li>{% translate "The thesis in a single PDF file" %}</li>
        <li>{% translate "All appendices in a single ZIP file" %}</li>
        <li>{% translate "Public abstract of the thesis (will be published)" %}</li>
        {% if master_thesis.confidential %}
            <li>{% translate "Confidentiality agreement signed by all three parties" %}</li>
        {% endif %}
     </ul>
  </p>

  <p>{% translate "Upon submission, you will receive the plagiarism analysis report and the official invitation to the thesis defense." %}</p>

  <p>{% trans 'We wish you success in completing your thesis' %}</p>

  {% include "templated_email/footer.email" with major=master_thesis.attempt.major %}
{% endblock %}
