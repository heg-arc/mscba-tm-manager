{% load static i18n %}

{% block subject %}[MScBA]
  {% if reminder %}{% translate 'reminder'|upper %} | {% endif %}
  {% blocktranslate with student=assignment.proposal.attempt.student.name %}
    Invitation to Supervise the Master's Thesis of {{ student }}
  {% endblocktranslate %}
{% endblock %}

{% block html %}
  <p>
    {% translate 'dear'|capfirst %} {{ assignment.supervisor.name }},<br/>
    {% if reminder %}
        {% blocktranslate with session=assignment.proposal.attempt.session %}
            <strong>This is a reminder that you have been invited to supervise a master's thesis for the {{ session }} session</strong>.<br/>
            We are still waiting for your decision regarding the supervision of this thesis. Please let us know your
            decision as soon as possible so that we can find another supervisor, if necessary. Please do not hesitate
            to contact us if you have any questions regarding the supervision of master's theses.
        {% endblocktranslate %}
    {% else %}
        {% blocktranslate with session=assignment.proposal.attempt.session %}
            You have been invited to supervise a master's thesis for the {{ session }} session.
        {% endblocktranslate %}
    {% endif %}
  </p>

  <p>{% translate 'We kindly ask you to accept or decline this invitation in the next few days.' %}</p>
      <dl>
        <dt>{% translate 'student'|capfirst %}</dt><dd>{{assignment.proposal.attempt.student}}</dd>
        <dt>{% translate 'major'|capfirst %}</dt><dd>{{assignment.proposal.attempt.major}}</dd>
        <dt>{% translate 'subject'|capfirst %}</dt><dd>{{assignment.proposal.title}}</dd>
      </dl>
  <p>{% translate 'Accept / decline this invitation' %}: <a href="{{url}}">{{url}}</a></p>

  <p>{% translate 'Please note that this request can be sent to several supervisors simultaneously.' %}</p>

  <p>{% translate 'Best regards' %}<br/>
  {{ assignment.proposal.attempt.major.coordinator.name }}</p>

  {% include "templated_email/footer.email" with major=assignment.proposal.attempt.major %}
{% endblock %}
