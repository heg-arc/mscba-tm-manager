{% load static i18n %}

{% block subject %}[MScBA] {% trans "expertise request"|capfirst %} - {{ master_thesis.attempt.student.name }}{% endblock %}

{% block html %}
  <p>{% trans 'Dear' %} {{ master_thesis.expert.name }},</p>

  {% blocktrans with supervisor=master_thesis.supervisor.name title=master_thesis.title student=master_thesis.attempt.student.name start=master_thesis.attempt.current_calendar.defense_start|date:"SHORT_DATE_FORMAT" end=master_thesis.attempt.current_calendar.defense_end|date:"SHORT_DATE_FORMAT" %}
  <p>{{supervisor}} invites you to evaluate {{student}}'s master's thesis entitled "{{title}}". The defense of the thesis is scheduled during the session from <strong>{{start}} to {{end}}</strong>.</p>
  <p>We would be delighted to have your expertise in evaluating this work.</p>
  <p>To accept or decline this proposal, please use the link below:</p>
  {% endblocktrans %}

  <h3><a href="{{ url }}">{% trans "accept or decline this expertise"|upper %}</a></h3>

  {% blocktrans with supervisor=master_thesis.supervisor.name email=master_thesis.supervisor.email %}
  <p>If you have any questions regarding this request, please contact {{supervisor}} directly (<a href="mailto:{{email}}">{{email}}</a>).</p>
  {% endblocktrans %}
  </p>

  {% if sesame_url %}
  <p style="color=red;"><strong>{% translate "WARNING - Please note that this message contains a link that will allow you to authenticate yourself on the management platform. Under no circumstances should you forward this email with the link it contains!" %}</strong></p>
  {% endif %}

  <p>{% trans 'Best regards' %}</p>

  {% include "templated_email/footer.email" with major=master_thesis.attempt.major %}
{% endblock %}
