{% load static i18n %}

{% block subject %}[MScBA]
  {% if assignment.accepted %}
    {% translate 'accepted'|upper %}
  {% else %}
    {% translate 'rejected'|upper %}
  {% endif %} - {{ assignment.proposal.attempt.student.name}} - {% translate "Master's thesis supervision" %}
{% endblock %}

{% block html %}
  <p>{% translate 'dear'|capfirst %} {{ assignment.proposal.attempt.student.name }},</p>
  {% if assignment.accepted %}
    <p>
      {% blocktranslate with supervisor=assignment.supervisor.name %}
        We confirm that {{ supervisor }} has agreed to supervise your master's thesis. You may contact him/her as needed to finalize your pre-proposal.
      {% endblocktranslate %}
    </p>
  {% else %}
    <p>
      {% blocktranslate with supervisor=assignment.supervisor.name %}
        We regret to inform you that {{ supervisor }} will not be able to supervise your Master's thesis.
      {% endblocktranslate %}
    </p>
    {% if assignment.comments != "" %}
      <p>{% translate 'He/she has left you the following message' %}:</p>
      <p><i>{{ assignment.comments }}</i></p>
    {% else %}
      <p>{% translate 'We have no further information on this decision, but you can contact him/her for more details if necessary.' %}</p>
    {% endif %}
  {% endif %}

  <p>
    {% translate 'Access your pre-proposal' %}: <a href="{{url}}">{{url}}</a>
  </p>

  <p>{% translate 'Best regards' %}<br/>
  {{ assignment.proposal.attempt.major.coordinator.name }}</p>

  {% include "templated_email/footer.email" with major=assignment.proposal.attempt.major %}
{% endblock %}
