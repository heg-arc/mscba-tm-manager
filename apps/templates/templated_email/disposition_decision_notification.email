{% load static i18n %}

{% block subject %}[MScBA] {% trans 'Assessment of your master thesis project' %} - {{ project.display_public_status|upper }}{% endblock %}

{% block html %}
  <p>{% trans 'Dear' %} {{ project.attempt.student.name }},</p>
  {% if project.accepted %}
    <p>{% trans 'We confirm that your master thesis project has been accepted! You can now start writing your master thesis.' %}<p>
    <p>{% trans 'Your upcoming deadlines:' %}
      <ul>
        <li>{% trans 'Progress report' %}: <u>{{ project.attempt.current_calendar.report_end }}</u></li>
        <li>{% trans 'Submission of the master thesis' %}: <u>{{ project.attempt.current_calendar.thesis_end }}</u></li>
      </ul>
    </p>
  {% elif project.status == 'rework' %}
    <p>{% trans 'Unfortunately, we must inform you that your master thesis project has not been accepted.' %}
      <ul>
        <li>{% trans 'Content of the project' %}: <b>{{ project.display_content_status|upper }}</b></li>
        <li>{% trans 'Methodology section' %}: <b>{{ project.display_methodology_status|upper }}</b></li>
      </ul>
    </p>
    <p>{% blocktranslate with date=project.attempt.current_calendar.project_rework_end %}You must rework the relevant parts and submit a new thesis proposal by <u>{{ date }}</u>. Please note that a new insufficiency on the methodological part will result in the failure of this attempt.{% endblocktranslate %}</p>
  {% else %}
    <p>{% trans 'Unfortunately, we must inform you that your master thesis project has not been accepted.' %}</p>
    <p>{% trans "The methodology of the project is not sufficient for a master's thesis. It deviates significantly from the best practices taught in the relevant courses. This is tantamount to failing the Research Methodology module and leading to a repeat of the Master's thesis." %}</p>
  {% endif %}

  <p>{% trans 'The reviewers left the following comments' %}:</p>
  <hr />
  <p><b>{% trans 'Overall content' %}</b><br/>
  <u>{% trans 'reviewer'|capfirst %}</u>: {{ project.reviewer_content.name }}<br/>
  {{ project.feedback_content.html | safe }}</p>
  <hr />
  <p><b>{% trans 'Methodology section' %}</b><br/>
  <u>{% trans 'reviewer'|capfirst %}</u>: {{ project.reviewer_methodology.name }}<br/>
  {{ project.feedback_methodology.html | safe }}</p>
  <hr />
  <p>{% trans 'Review your project' %}: <a href="{{url}}">{{url}}</a></p>

  <p>{% trans 'Best regards' %}</p>

  {% include "templated_email/footer.email" with major=project.attempt.major %}
{% endblock %}
