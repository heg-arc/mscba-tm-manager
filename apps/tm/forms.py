from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from dal import autocomplete
from django import forms
from django.conf import settings
from django.forms import Form
from django.forms import ModelForm
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from django_addanother.widgets import AddAnotherWidgetWrapper

from . import models


class ProposalForm(ModelForm):
    class Meta:
        model = models.Proposal
        fields = [
            "title",
            "background",
            "problem_statement",
            "research_questions",
            "methodology",
            "partner",
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.add_input(Submit("submit", _("Submit")))


class StudentWorkflowForm(ModelForm):
    class Meta:
        model = models.ProposalWorkflow
        fields = [
            "comments",
        ]


class WorkflowForm(ModelForm):
    class Meta:
        model = models.ProposalWorkflow
        fields = [
            "comments",
            "status",
        ]
        widgets = {
            "status": forms.HiddenInput(),
        }
        labels = {
            "comments": _("Add review"),
        }


class SupervisorSuggestionForm(ModelForm):
    class Meta:
        model = models.Assignment
        fields = [
            "supervisor",
            "suggestion",
        ]


class ProjectCoSupervisorForm(ModelForm):
    class Meta:
        model = models.Project
        fields = [
            "co_supervisors",
        ]
        widgets = {
            "co_supervisors": autocomplete.ModelSelect2Multiple(
                "tm:supervisors-autocomplete",
            ),
        }


class SupervisorAcceptationForm(ModelForm):
    class Meta:
        model = models.Assignment
        fields = [
            "accepted",
            "comments",
            "comments_mh",
        ]
        widgets = {
            "accepted": forms.HiddenInput(),
            "comments": forms.Textarea(attrs={"cols": 80, "rows": 10}),
            "comments_mh": forms.Textarea(attrs={"cols": 80, "rows": 5}),
        }


class LinkAccountForm(forms.Form):
    hesso_id = forms.CharField(max_length=20, widget=forms.HiddenInput)
    submit_action = forms.CharField(max_length=10, widget=forms.HiddenInput)


class CreateAccountForm(forms.Form):
    hesso_id = forms.CharField(max_length=20, widget=forms.HiddenInput)
    is_student = forms.BooleanField(required=False)
    is_expert = forms.BooleanField(required=False)
    is_supervisor = forms.BooleanField(required=False)
    is_mh = forms.BooleanField(required=False)
    majors = forms.ModelMultipleChoiceField(
        queryset=models.Major.objects.all(),
        required=False,
        widget=forms.CheckboxSelectMultiple(),
    )

    def __init__(self, *args, **kwargs):
        user = kwargs.pop("user", None)
        super().__init__(*args, **kwargs)

        # Add 'is_mh' field only for admin users
        if user and user.is_superuser:
            self.fields["is_mh"] = forms.BooleanField(required=False)
        # Only display majors which the user is member of (if not admin)
        if user and not user.is_superuser:
            self.fields["majors"].queryset = user.majors.all()


class CreateProposalFromProject(ModelForm):
    class Meta:
        model = models.Project
        fields = [
            "proposal",
        ]


class UpdateProjectForm(ModelForm):
    class Meta:
        model = models.Project
        fields = [
            "title",
            "confidential",
            "pdf_file",
            "comments",
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.add_input(Submit("save", _("Save")))


class SubmitProjectForm(ModelForm):
    class Meta:
        model = models.Project
        fields = [
            "comments",
        ]


class EvaluateProjectForm(ModelForm):
    class Meta:
        model = models.Project
        fields = [
            "pass_content",
            "feedback_content",
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.add_input(Submit("save", _("Save")))


class EvaluateProjectMethodologyForm(ModelForm):
    class Meta:
        model = models.Project
        fields = [
            "grade_methodology",
            "feedback_methodology",
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.add_input(Submit("save", _("Save")))


class PersonalLeaveForm(ModelForm):
    class Meta:
        model = models.PersonalLeave
        fields = ["type_of_leave", "first_day_leave", "last_day_leave", "basis"]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.add_input(Submit("save", _("Save")))


class StudentExceptionForm(ModelForm):
    class Meta:
        model = models.StudentException
        fields = ["reason", "block_tm", "block_defense", "probable_resolution_date"]

    def clean(self):
        cleaned_data = super().clean()
        block_tm = cleaned_data.get("block_tm")
        block_defense = cleaned_data.get("block_defense")

        if not block_tm and not block_defense:
            raise forms.ValidationError(
                _(
                    "You must select at least one of the two options 'cannot start master's thesis' or 'cannot defend master's thesis'",  # noqa: E501
                ),
            )

        return cleaned_data

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.add_input(Submit("save", _("Save")))


class ProgressReportStudentForm(ModelForm):
    class Meta:
        model = models.ProgressReport
        fields = [
            "global_assessment",
            "abstract",
            "progress_report",
            "problems_encountered",
            "ability_to_achieve",
            "comments_for_mh",
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.add_input(Submit("save", _("Save")))


class ProgressReportSupervisorForm(ModelForm):
    class Meta:
        model = models.ProgressReport
        fields = [
            "global_assessment",
            "progress_report",
            "problems_encountered",
            "ability_to_achieve",
            "comments_for_mh",
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.add_input(Submit("save", _("Save")))


class ProgressReportSubmitForm(ModelForm):
    class Meta:
        model = models.ProgressReport
        fields = ["submitted"]
        widgets = {
            "submitted": forms.HiddenInput(),
        }


class MasterThesisSupervisorForm(ModelForm):
    class Meta:
        model = models.MasterThesis
        fields = [
            "expert",
            "paper_submission_supervisor",
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["expert"].widget = AddAnotherWidgetWrapper(
            autocomplete.ModelSelect2(
                url=reverse_lazy(
                    "users:expert-autocomplete",
                    kwargs={"attempt_id": self.instance.attempt.id},
                ),
            ),
            add_related_url=reverse_lazy("users:expert-create"),
        )
        self.helper = FormHelper()
        self.helper.add_input(Submit("save", _("Save")))


class MasterThesisStudentForm(ModelForm):
    class Meta:
        model = models.MasterThesis
        fields = [
            "title",
            "confidential",
            "confidential_agreement_file",
            "abstract",
            "pdf_file",
            "zip_file",
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.add_input(Submit("save", _("Save")))


class MasterThesisExpertAcceptForm(ModelForm):
    class Meta:
        model = models.MasterThesis
        fields = [
            "paper_submission_expert",
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.add_input(Submit("save", _("Save")))


class MasterThesisExpertDeclineForm(ModelForm):
    message = forms.CharField(
        label=_("Message"),
        widget=forms.Textarea,
        required=False,
        help_text=_(
            "You can message the supervisor to explain why you declined to review.",
        ),
    )

    class Meta:
        model = models.MasterThesis
        fields = ["expert_confirmed"]
        widgets = {
            "expert_confirmed": forms.HiddenInput(),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.add_input(Submit("save", _("Save")))


class ThesisSubmitForm(ModelForm):
    class Meta:
        model = models.MasterThesis
        fields = ["submitted"]
        widgets = {
            "submitted": forms.HiddenInput(),
        }


class DefenseSupervisorForm(ModelForm):
    class Meta:
        model = models.Defense
        fields = ["defense_date", "place", "other_location"]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.add_input(Submit("save", _("Save")))


class DefenseAdminForm(ModelForm):
    class Meta:
        model = models.Defense
        fields = ["defense_date", "place", "room", "other_location"]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.add_input(Submit("save", _("Save")))


class AuthorizationPublishForm(ModelForm):
    class Meta:
        model = models.MasterThesis
        fields = ["authorization_publish_file"]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.add_input(Submit("save", _("Submit")))


class RecordAuthorizationPublishForm(ModelForm):
    class Meta:
        model = models.MasterThesis
        fields = ["authorization_publish"]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.add_input(Submit("save", _("Submit")))


class StatementFeesForm(ModelForm):
    class Meta:
        model = models.Defense
        fields = ["statement_of_fees_file"]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.add_input(Submit("save", _("Submit")))


class ThesisAssessmentForm(ModelForm):
    class Meta:
        model = models.Defense
        fields = ["grade", "assessment_file", "remediation_objectives"]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.add_input(Submit("save", _("Submit")))


class ImportStudentsForm(Form):
    isa_ids = forms.CharField(
        label=_("ISA IDs"),
        widget=forms.Textarea(attrs={"placeholder": _("Paste ISA IDs, one per line")}),
        help_text=_("Enter ISA IDs, one per line"),
    )
    language_code = forms.ChoiceField(
        label=_("language"),
        choices=settings.LANGUAGES,
        initial="fr",
    )
    session_id = forms.ModelChoiceField(
        label="Session",
        queryset=models.Session.objects.all(),
        initial=lambda: models.Session.objects.last(),
        to_field_name="id",
    )
    major_id = forms.ModelChoiceField(
        label="Major",
        queryset=models.Major.objects.all(),
        to_field_name="id",
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.add_input(Submit("save", _("Import students")))


class AnonymizeDataForm(Form):
    anonymize = forms.BooleanField(
        label=_("I fully understand the consequences of data anonymization"),
        required=True,
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.add_input(Submit("save", _("Anonymize the data")))
