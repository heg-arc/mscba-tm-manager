from django.contrib import admin

from .models import Assignment
from .models import Attempt
from .models import Calendar
from .models import Defense
from .models import Major
from .models import MasterThesis
from .models import PersonalLeave
from .models import ProgressReport
from .models import Project
from .models import Proposal
from .models import ProposalWorkflow
from .models import Session
from .models import StudentException


@admin.register(Calendar)
class CalendarAdmin(admin.ModelAdmin):
    model = Calendar
    search_fields = ["label"]


@admin.register(Session)
class SessionAdmin(admin.ModelAdmin):
    model = Session
    search_fields = ["name"]
    autocomplete_fields = ["calendar"]


@admin.register(Major)
class MajorAdmin(admin.ModelAdmin):
    model = Major
    search_fields = ["name", "code"]
    autocomplete_fields = ["secretary", "coordinator"]


@admin.register(Attempt)
class AttemptAdmin(admin.ModelAdmin):
    model = Attempt
    search_fields = ["student__name", "student__username"]
    autocomplete_fields = ["student", "major", "session", "calendar"]


@admin.register(PersonalLeave)
class PersonalLeaveAdmin(admin.ModelAdmin):
    model = PersonalLeave
    search_fields = ["attempt__student__name", "attempt__student__username"]
    autocomplete_fields = ["attempt", "editor"]


@admin.register(Proposal)
class ProposalAdmin(admin.ModelAdmin):
    model = Proposal
    search_fields = ["title", "attempt__student__name", "attempt__student__username"]
    autocomplete_fields = ["attempt"]


@admin.register(ProposalWorkflow)
class ProposalWorkflowAdmin(admin.ModelAdmin):
    model = ProposalWorkflow
    autocomplete_fields = ["proposal", "user"]
    search_fields = [
        "proposal__attempt__student__name",
        "proposal__attempt__student__username",
    ]


@admin.register(Assignment)
class AssignmentAdmin(admin.ModelAdmin):
    model = Assignment
    autocomplete_fields = ["proposal", "supervisor"]
    search_fields = [
        "proposal__attempt__student__name",
        "proposal__attempt__student__username",
    ]


@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    model = Project
    search_fields = ["title", "attempt__student__name", "attempt__student__username"]
    autocomplete_fields = [
        "attempt",
        "supervisor",
        "proposal",
        "reviewer_content",
        "reviewer_methodology",
    ]


@admin.register(MasterThesis)
class MasterThesisAdmin(admin.ModelAdmin):
    model = MasterThesis
    autocomplete_fields = [
        "attempt",
        "project",
        "expert",
        "supervisor",
        "hard_copies_received_user",
    ]
    search_fields = ["title", "attempt__student__name", "attempt__student__username"]


@admin.register(Defense)
class DefenseAdmin(admin.ModelAdmin):
    model = Defense
    autocomplete_fields = ["master_thesis"]


@admin.register(ProgressReport)
class ProgressReportAdmin(admin.ModelAdmin):
    model = ProgressReport
    autocomplete_fields = ["master_thesis", "user", "forced_submission_user"]


@admin.register(StudentException)
class StudentExceptionAdmin(admin.ModelAdmin):
    model = StudentException
    autocomplete_fields = ["created_by", "cleared_by", "attempt"]
