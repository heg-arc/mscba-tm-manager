import contextlib

import requests
from bs4 import BeautifulSoup


def query_user_by_name(query_name):
    """
    Query the HES-SO directory to get the user id from the name of the people

    :param query_name: name of the people
    :return: list of user id matching the name (can be empty) {"id", "display_name", "first_name", "last_name",
             "email", "gender", "schools"}
    """  # noqa: E501
    webservice_url = "https://annuaire.hes-so.ch/Axis_HES_ssl/services/HESSO_WS/searchPeopleAndContract?filterPeople=(displayName=*{}*)"
    r = requests.get(webservice_url.format(query_name), timeout=5)
    soup = BeautifulSoup(r.text, "xml")
    elements = soup.find_all("element")
    results = []
    for element in elements:
        try:
            display_name = element.find("attribut", {"name": "displayName"}).text
        except AttributeError:
            display_name = None
        try:
            first_name = element.find("attribut", {"name": "givenName"}).text
        except AttributeError:
            first_name = None
        try:
            last_name = element.find("attribut", {"name": "sn"}).text
        except AttributeError:
            last_name = None
        try:
            email = element.find("attribut", {"name": "mail"}).text
        except AttributeError:
            email = None
        try:
            gender = element.find("attribut", {"name": "swissEduPersonGender"}).text
        except AttributeError:
            gender = None
        contracts = element.find_all("contract")
        schools = []
        for contract in contracts:
            with contextlib.suppress(AttributeError):
                schools.append(contract.find("attribut", {"name": "o"}).text)
        results.append(
            {
                "id": element["id"],
                "display_name": display_name,
                "first_name": first_name,
                "last_name": last_name,
                "email": email,
                "gender": gender,
                "schools": ", ".join(schools),
            },
        )
    return results


def query_user_by_id(isa_id):
    """
    Query the HES-SO directory to get the user information from the isa_id of the people

    :param isa_id: ISAcademia ID of the people
    :return: dict of attributes matching the ISA ID (can be empty) {"id", "display_name", "first_name", "last_name",
             "email", "gender", "schools"}
    """  # noqa: E501
    webservice_url = "https://annuaire.hes-so.ch/Axis_HES_ssl/services/HESSO_WS/searchPeopleAndContract?filterPeople=(cn={})"
    r = requests.get(webservice_url.format(isa_id), timeout=5)
    soup = BeautifulSoup(r.text, "xml")
    elements = soup.find_all("element")

    if len(elements) == 1:
        # We retrieved one people from the directory, we can return it
        try:
            display_name = elements[0].find("attribut", {"name": "displayName"}).text
        except AttributeError:
            display_name = None
        try:
            first_name = elements[0].find("attribut", {"name": "givenName"}).text
        except AttributeError:
            first_name = None
        try:
            last_name = elements[0].find("attribut", {"name": "sn"}).text
        except AttributeError:
            last_name = None
        try:
            email = elements[0].find("attribut", {"name": "mail"}).text
        except AttributeError:
            email = None
        try:
            gender = elements[0].find("attribut", {"name": "swissEduPersonGender"}).text
        except AttributeError:
            gender = None

        return {
            "id": isa_id,
            "display_name": display_name,
            "first_name": first_name,
            "last_name": last_name,
            "email": email,
            "gender": gender,
        }
    # We retrieved no or more than one people from the directory, it's an error
    return None
