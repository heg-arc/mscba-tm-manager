from factory.django import DjangoModelFactory

from apps.tm.models import Calendar


class CalendarFactory(DjangoModelFactory):
    class Meta:
        model = Calendar
