from django.urls import path
from django.views.generic import TemplateView

from .views import AddReviewProposal
from .views import AnonymizeDataProgressView
from .views import AnonymizeDataView
from .views import AssignmentAcceptance
from .views import AssignmentsListView
from .views import AssignmentsToValidateListView
from .views import AttemptAutocomplete
from .views import AttemptDetailView
from .views import CalendarDetailView
from .views import DashboardView
from .views import DefenseBookRoomUpdateView
from .views import DefenseCreateView
from .views import DefenseRemediationObjectives
from .views import DefensesListView
from .views import DefenseSubmitAssessment
from .views import DefenseSubmitStatementOfFees
from .views import DeleteStepFromProposal
from .views import DispositionCreateView
from .views import DispositionDetailView
from .views import DispositionEvaluateMethodologyView
from .views import DispositionEvaluateView
from .views import DispositionListView
from .views import DispositionSubmitView
from .views import DispositionUpdateView
from .views import ExpertListView
from .views import GroupsListView
from .views import ImportExportDocumentsView
from .views import ImportStudentsProgressView
from .views import ImportStudentsView
from .views import LibraryDetailView
from .views import LibraryListView
from .views import OverviewView
from .views import PersonalLeaveCreateView
from .views import PersonalLeaveDeleteView
from .views import PersonalLeaveListView
from .views import PersonalLeaveUpdateView
from .views import ProgressReportCreateView
from .views import ProgressReportDetailView
from .views import ProgressReportListView
from .views import ProgressReportSubmitView
from .views import ProgressReportUpdateView
from .views import ProgressReportView
from .views import ProposalCreateView
from .views import ProposalDetailView
from .views import ProposalExportView
from .views import ProposalListView
from .views import ProposalUpdateView
from .views import StudentExceptionCreateView
from .views import StudentExceptionDetailView
from .views import StudentExceptionListView
from .views import StudentExceptionUpdateView
from .views import SubmitProposal
from .views import SupervisorAutocomplete
from .views import SupervisorSuggestion
from .views import ThesesListView
from .views import ThesesMailingView
from .views import ThesisDetailView
from .views import ThesisExpertAcceptView
from .views import ThesisExpertDeclineView
from .views import ThesisExpertDetailView
from .views import ThesisRecordAuthorizationPublish
from .views import ThesisSubmitAuthorizationPublish
from .views import ThesisSubmitView
from .views import ThesisUpdateView
from .views import WithdrawProposal
from .views import add_supervisor
from .views import adopt_expert
from .views import authorization_publish2img
from .views import cancel_defense
from .views import clear_exception
from .views import create_user_hes_so
from .views import download_project_zip
from .views import download_theses_zip
from .views import download_thesis_zip
from .views import download_zip_assessment
from .views import download_zip_authorization_publish
from .views import download_zip_statement_fees
from .views import excel_addresses
from .views import excel_master_theses
from .views import excel_projects
from .views import file_upload
from .views import force_submission_progress_report
from .views import get_pdf_confidentiality_agreement
from .views import get_pdf_publishing_authorization
from .views import get_pdf_statement_of_fees
from .views import get_pdf_thesis_assessment
from .views import link_account
from .views import publish_evaluation
from .views import publish_evaluations
from .views import receive_hard_copies
from .views import search_hesso_directory
from .views import send_defense_invitations
from .views import send_reminder_defense_documents
from .views import send_reminder_defenses
from .views import send_reminder_expert_profile
from .views import send_reminder_experts
from .views import send_reminder_progress_reports
from .views import set_major
from .views import set_session
from .views import submit_remediation_objectives
from .views import toggle_assignment_reminders
from .views import validate_expert
from .views import validate_progress_reports
from .views import validate_supervisor

app_name = "tm"

urlpatterns = [
    path("", view=DashboardView.as_view(), name="dashboard"),
    path("overview", view=OverviewView.as_view(), name="overview"),
    path("add-proposal", view=ProposalCreateView.as_view(), name="add-proposal"),
    path("proposals", view=ProposalListView.as_view(), name="proposals"),
    path(
        "proposals/export",
        view=ProposalExportView.as_view(),
        name="proposals-export",
    ),
    path("proposal/<int:pk>/", view=ProposalDetailView.as_view(), name="proposal"),
    path(
        "proposal/<int:pk>/change",
        view=ProposalUpdateView.as_view(),
        name="change-proposal",
    ),
    path(
        "proposal/<int:pk>/submit",
        view=SubmitProposal.as_view(),
        name="submit-proposal",
    ),
    path(
        "proposal/<int:pk>/withdraw",
        view=WithdrawProposal.as_view(),
        name="withdraw-proposal",
    ),
    path(
        "proposal/<int:pk>/review",
        view=AddReviewProposal.as_view(),
        name="review-proposal",
    ),
    path(
        "proposal/<int:pk>/supervisor",
        view=SupervisorSuggestion.as_view(),
        name="supervisor-suggestion-proposal",
    ),
    path("assignments", view=AssignmentsListView.as_view(), name="assignments"),
    path(
        "assignments/validation",
        view=AssignmentsToValidateListView.as_view(),
        name="assignments-to-validate",
    ),
    path(
        "assignment/<int:pk>/validated",
        validate_supervisor,
        {"decision": "validated"},
        name="assignments-decision-validated",
    ),
    path(
        "assignment/<int:pk>/rejected",
        validate_supervisor,
        {"decision": "rejected"},
        name="assignments-decision-rejected",
    ),
    path(
        "assignment/<int:pk>/acceptance",
        view=AssignmentAcceptance.as_view(),
        name="assignments-acceptance",
    ),
    path(
        "assignment/<int:pk>/link-account",
        link_account,
        name="assignments-link-account",
    ),
    path(
        "assignment/<int:pk>/reminders",
        toggle_assignment_reminders,
        name="assignments-toggle-reminders",
    ),
    path("dispositions", view=DispositionListView.as_view(), name="dispositions"),
    path(
        "disposition/create",
        view=DispositionCreateView.as_view(),
        name="add-disposition",
    ),
    path(
        "disposition/<int:pk>/",
        view=DispositionDetailView.as_view(),
        name="disposition",
    ),
    path(
        "disposition/<int:pk>/change",
        view=DispositionUpdateView.as_view(),
        name="change-disposition",
    ),
    path(
        "disposition/<int:pk>/evaluate",
        view=DispositionEvaluateView.as_view(),
        name="evaluate-disposition",
    ),
    path(
        "disposition/<int:pk>/evaluate-methodology",
        view=DispositionEvaluateMethodologyView.as_view(),
        name="evaluate-methodology-disposition",
    ),
    path(
        "disposition/<int:pk>/submit",
        view=DispositionSubmitView.as_view(),
        name="submit-disposition",
    ),
    path(
        "disposition/<int:pk>/publish",
        publish_evaluation,
        name="publish-evaluation-disposition",
    ),
    path(
        "disposition/<int:pk>/add-co-supervisor",
        add_supervisor,
        name="add-co-supervisor-disposition",
    ),
    path("dispositions/download", download_project_zip, name="download-dispositions"),
    path("dispositions/export", excel_projects, name="excel-export-dispositions"),
    path(
        "dispositions/publish",
        publish_evaluations,
        name="publish-evaluations-dispositions",
    ),
    path("library", view=LibraryListView.as_view(), name="library"),
    path("library/<int:pk>/", view=LibraryDetailView.as_view(), name="library-detail"),
    path(
        "calendar/<int:pk>/",
        view=CalendarDetailView.as_view(),
        name="calendar-detail",
    ),
    path(
        "step/<int:pk>/delete",
        view=DeleteStepFromProposal.as_view(),
        name="delete-step",
    ),
    path("set-session/<int:session_id>/", set_session, name="set-session"),
    path("set-major/<int:major_id>/", set_major, name="set-major"),
    path(
        "confirm/",
        TemplateView.as_view(template_name="tm/proposalworkflow_confirm.html"),
        name="confirm",
    ),
    path("attempt/", view=AttemptDetailView.as_view(), name="attempt-resolver"),
    path("attempt/<int:pk>/", view=AttemptDetailView.as_view(), name="attempt"),
    path(
        "attempt/<int:pk>/add-leave",
        view=PersonalLeaveCreateView.as_view(),
        name="add-leave",
    ),
    path("leaves", view=PersonalLeaveListView.as_view(), name="leaves"),
    path(
        "leave/<int:pk>/change",
        view=PersonalLeaveUpdateView.as_view(),
        name="change-leave",
    ),
    path(
        "leave/<int:pk>/delete",
        view=PersonalLeaveDeleteView.as_view(),
        name="delete-leave",
    ),
    path(
        "attempt/<int:pk>/add-exception",
        view=StudentExceptionCreateView.as_view(),
        name="add-exception",
    ),
    path(
        "exception/<int:pk>/",
        view=StudentExceptionDetailView.as_view(),
        name="exception",
    ),
    path(
        "exception/<int:pk>/change",
        view=StudentExceptionUpdateView.as_view(),
        name="change-exception",
    ),
    path("exception/<int:pk>/clear", clear_exception, name="clear-exception"),
    path("exceptions", view=StudentExceptionListView.as_view(), name="exceptions"),
    path(
        "attempt/<int:pk>/add-progress-report",
        view=ProgressReportCreateView.as_view(),
        name="add-progress-report",
    ),
    path(
        "attempt/<int:pk>/check-progress-report",
        view=ProgressReportView.as_view(),
        name="check-progress-report",
    ),
    path(
        "attempt/<int:pk>/validate-progress-report",
        validate_progress_reports,
        name="validate-progress-report",
    ),
    path(
        "attempt/<int:pk>/send-reminder",
        send_reminder_progress_reports,
        name="reminder-progress-report",
    ),
    path(
        "progress-report/<int:pk>/",
        view=ProgressReportDetailView.as_view(),
        name="view-progress-report",
    ),
    path(
        "progress-report/<int:pk>/change",
        view=ProgressReportUpdateView.as_view(),
        name="change-progress-report",
    ),
    path(
        "progress-report/<int:pk>/submit",
        view=ProgressReportSubmitView.as_view(),
        name="submit-progress-report",
    ),
    path(
        "progress-report/<int:pk>/force-submission",
        force_submission_progress_report,
        name="force-submission-progress-report",
    ),
    path(
        "progress-reports",
        view=ProgressReportListView.as_view(),
        name="progress-reports",
    ),
    path(
        "progress-reports/reminders",
        send_reminder_progress_reports,
        name="reminder-all-progress-reports",
    ),
    path(
        "theses",
        view=ThesesListView.as_view(),
        name="theses",
    ),
    path(
        "theses/send-expert-reminders",
        send_reminder_experts,
        name="reminder-experts",
    ),
    path(
        "theses/send-defense-reminders",
        send_reminder_defenses,
        name="reminder-defenses",
    ),
    path("theses/download", download_theses_zip, name="download-theses"),
    path(
        "theses/mailing",
        view=ThesesMailingView.as_view(),
        name="mailing-theses",
    ),
    path(
        "theses/mailing/excel",
        view=excel_addresses,
        name="mailing-excel-theses",
    ),
    path(
        "theses/assessments",
        view=excel_master_theses,
        name="excel-theses-assessments",
    ),
    path(
        "thesis/<int:pk>/",
        view=ThesisDetailView.as_view(),
        name="view-thesis",
    ),
    path(
        "thesis/<int:pk>/change",
        view=ThesisUpdateView.as_view(),
        name="change-thesis",
    ),
    path(
        "thesis/<int:pk>/submit",
        view=ThesisSubmitView.as_view(),
        name="submit-thesis",
    ),
    path(
        "thesis/<int:pk>/expert",
        view=ThesisExpertDetailView.as_view(),
        name="expert-view-thesis",
    ),
    path(
        "thesis/<int:pk>/expert-acceptance",
        view=ThesisExpertAcceptView.as_view(),
        name="expert-acceptance-thesis",
    ),
    path(
        "thesis/<int:pk>/expert-decline",
        view=ThesisExpertDeclineView.as_view(),
        name="expert-decline-thesis",
    ),
    path(
        "thesis/<int:pk>/validate-expert",
        validate_expert,
        name="validate-expert",
    ),
    path(
        "thesis/<int:pk>/send-expert-reminder",
        send_reminder_experts,
        name="reminder-expert",
    ),
    path(
        "thesis/<int:pk>/hard-copies-reception",
        receive_hard_copies,
        name="receive_hard_copies",
    ),
    path(
        "thesis/<int:pk>/create-defense",
        view=DefenseCreateView.as_view(),
        name="create-defense",
    ),
    path(
        "thesis/<int:pk>/send-defense-reminder",
        send_reminder_defenses,
        name="reminder-defense",
    ),
    path(
        "thesis/<int:pk>/publishing-authorization",
        get_pdf_publishing_authorization,
        name="get-publishing-authorization",
    ),
    path(
        "thesis/<int:pk>/confidentiality-agreement",
        get_pdf_confidentiality_agreement,
        name="get-confidentiality-agreement",
    ),
    path(
        "thesis/<int:pk>/download",
        download_thesis_zip,
        name="thesis-download",
    ),
    path(
        "thesis/<int:pk>/submit-publishing-authorization",
        view=ThesisSubmitAuthorizationPublish.as_view(),
        name="submit-publishing-authorization",
    ),
    path(
        "thesis/<int:pk>/record-publishing-authorization",
        view=ThesisRecordAuthorizationPublish.as_view(),
        name="record-publishing-authorization",
    ),
    path(
        "thesis/<int:pk>/authorization_publish2img",
        authorization_publish2img,
        name="publishing-authorization-pdf2img",
    ),
    path(
        "defense/<int:pk>/statement-of-fees",
        get_pdf_statement_of_fees,
        name="get-statement-of-fees",
    ),
    path(
        "defense/<int:pk>/submit-statement-of-fees",
        view=DefenseSubmitStatementOfFees.as_view(),
        name="submit-statement-of-fees",
    ),
    path(
        "defense/<int:pk>/thesis-assessment",
        get_pdf_thesis_assessment,
        name="get-thesis-assessment",
    ),
    path(
        "defense/<int:pk>/submit-thesis-assessment",
        view=DefenseSubmitAssessment.as_view(),
        name="submit-thesis-assessment",
    ),
    path(
        "defense/<int:pk>/remediation-objectives",
        view=DefenseRemediationObjectives.as_view(),
        name="remediation-objectives",
    ),
    path(
        "defense/<int:pk>/submit-remediation-objectives",
        submit_remediation_objectives,
        name="submit-remediation-objectives",
    ),
    path(
        "defense/<int:pk>/book-room",
        view=DefenseBookRoomUpdateView.as_view(),
        name="book-room",
    ),
    path(
        "defense/<int:pk>/change",
        view=DefenseBookRoomUpdateView.as_view(),
        name="defense-change",
    ),
    path(
        "defense/<int:pk>/reminder",
        send_reminder_defense_documents,
        name="defense-documents-reminder",
    ),
    path(
        "defense/<int:pk>/cancel",
        cancel_defense,
        name="defense-cancel",
    ),
    path(
        "defenses",
        view=DefensesListView.as_view(),
        name="defenses",
    ),
    path(
        "defenses/send-invitations",
        send_defense_invitations,
        name="send-defense-invitations",
    ),
    path(
        "import-students/",
        ImportStudentsView.as_view(),
        name="import-students",
    ),
    path(
        "import-students/<uuid:task_id>",
        ImportStudentsProgressView.as_view(),
        name="import-students-progress",
    ),
    path(
        "groups/",
        GroupsListView.as_view(),
        name="groups",
    ),
    path(
        "downloads/publishing-authorization/",
        download_zip_authorization_publish,
        name="download-publishing-authorization",
    ),
    path(
        "downloads/statements-of-fees/",
        download_zip_statement_fees,
        name="download-statements-of-fees",
    ),
    path(
        "downloads/theses-assessments/",
        download_zip_assessment,
        name="download-theses-assessments",
    ),
    path(
        "downloads/upload/",
        file_upload,
        name="file-upload",
    ),
    path(
        "downloads/",
        ImportExportDocumentsView.as_view(),
        name="import-export",
    ),
    path(
        "anonymize/",
        AnonymizeDataView.as_view(),
        name="anonymize-data",
    ),
    path(
        "anonymize/<uuid:task_id>/",
        AnonymizeDataProgressView.as_view(),
        name="anonymize-data-progress",
    ),
    path(
        "attempts-autocomplete/",
        AttemptAutocomplete.as_view(),
        name="attempts-autocomplete",
    ),
    path(
        "people-search",
        search_hesso_directory,
        name="people-search",
    ),
    path(
        "supervisors-autocomplete/",
        SupervisorAutocomplete.as_view(),
        name="supervisors-autocomplete",
    ),
    path(
        "create-user-hes-so/",
        create_user_hes_so,
        name="create-user-hes-so",
    ),
    path(
        "expert/<int:user_id>/send-reminder",
        send_reminder_expert_profile,
        name="send-reminder-expert-profile",
    ),
    path(
        "expert/<int:user_id>/adopt",
        adopt_expert,
        name="adopt-expert",
    ),
    path(
        "experts/",
        ExpertListView.as_view(),
        name="experts",
    ),
]
