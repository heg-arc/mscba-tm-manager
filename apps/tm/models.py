import json
import logging
from datetime import timedelta
from pathlib import Path
from uuid import uuid4

import gviz_api
from django.contrib.auth.models import Group
from django.db import models
from django.db import transaction
from django.urls import reverse
from django.utils import timezone
from django.utils.formats import date_format
from django.utils.functional import cached_property
from django.utils.translation import gettext_lazy as _
from django_quill.fields import QuillField

from apps.users.models import User

from .tasks import exception_cleared_notification
from .tasks import exception_notification
from .tasks import send_assessment_confirmation
from .tasks import send_compilatio_certificate
from .tasks import send_defense_schedule
from .tasks import send_notification_archive
from .tasks import send_notification_decision
from .tasks import send_notification_disposition
from .tasks import send_notification_validated
from .tasks import send_notification_workflow
from .tasks import send_record_publication_notification
from .tasks import send_room_booking
from .tasks import send_thesis_submitted_notification
from .tasks import update_calendar

logger = logging.getLogger(__name__)


def query_directory(query):
    import requests
    from bs4 import BeautifulSoup

    url = f"https://annuaire.hes-so.ch/Axis_HES_ssl/services/HESSO_WS/searchPeopleAndContract?filterPeople=(displayName=*{query}*)"
    r = requests.get(url, timeout=100)
    soup = BeautifulSoup(r.text, "xml")
    elements = soup.find_all("element")
    results = []
    for person in elements:
        per = {"id": person["id"], "contracts": []}
        if person.find(attrs={"name": "displayName"}):
            per["displayName"] = person.find(attrs={"name": "displayName"}).text
        if person.find(attrs={"name": "mail"}):
            per["mail"] = person.find(attrs={"name": "mail"}).text
        if person.find(attrs={"name": "hessoFonction"}):
            per["hessoFonction"] = person.find(attrs={"name": "hessoFonction"}).text
        contracts = person.find_all("contract")
        for contract in contracts:
            c = {}
            if contract.find(attrs={"name": "o"}):
                c["o"] = contract.find(attrs={"name": "o"}).text
            if contract.find(attrs={"name": "hessoPersonType"}):
                c["hessoPersonType"] = contract.find(
                    attrs={"name": "hessoPersonType"},
                ).text
            if c:
                per["contracts"].append(c)
        results.append(per)
    return results


def path_and_rename(path):
    def wrapper(instance, filename):
        ext = filename.split(".")[-1]
        filename = f"{uuid4().hex}.{ext}"
        return Path(path, filename)

    return wrapper


# Hack to support migrations
project_pdf_upload_path = path_and_rename("project/")
project_pdf_upload_path.__qualname__ = "project_pdf_upload_path"

thesis_pdf_upload_path = path_and_rename("thesis/")
thesis_pdf_upload_path.__qualname__ = "thesis_pdf_upload_path"

defense_pdf_upload_path = path_and_rename("defense/")
defense_pdf_upload_path.__qualname__ = "defense_pdf_upload_path"

major_pdf_upload_path = path_and_rename("major/")
major_pdf_upload_path.__qualname__ = "major_pdf_upload_path"


def get_current_session():
    try:
        current = Session.objects.filter(
            start_date__lte=timezone.now().date(),
            end_date__gte=timezone.now().date(),
        ).get()
    except Session.DoesNotExist:
        # There is no current session, we retrieve the last session available
        current = Session.objects.all().order_by("start_date").last()
        logger.exception("Session DoesNotExist There are no current session defined!")
    except Session.MultipleObjectsReturned:
        # There are multiple "current" sessions, we retrieve the last session available
        current = Session.objects.all().order_by("start_date").last()
        logger.exception(
            f"Session MultipleObjectsReturned Multiple session defined for the {timezone.now().date()}!",  # noqa: E501, G004
        )

    return current


def get_current_calendar(current_session, attempt=None):
    if attempt:
        if attempt.current_calendar:
            return attempt.current_calendar
    if current_session and current_session.calendar:
        return current_session.calendar
    return None


def get_gantt_from_calendar(calendar):
    if not calendar:
        return None
    description = {
        "task_group": ("string", "Task Group"),
        "task_name": ("string", "Task Name"),
        "start_date": ("date", "Start Date"),
        "end_date": ("date", "End Date"),
    }

    data = []

    if calendar.proposal_start and calendar.proposal_end:
        data.append(
            {
                "task_group": "proposal",
                "task_name": "{} ({} {})".format(
                    _("Pre-proposal"),
                    _("due"),
                    calendar.proposal_end.strftime("%d.%m.%Y"),
                ),
                "start_date": calendar.proposal_start,
                "end_date": calendar.proposal_end,
            },
        )
    if calendar.proposal_rework_start and calendar.proposal_rework_end:
        data.append(
            {
                "task_group": "proposal",
                "task_name": "{} ({} {})".format(
                    _("Pre-proposal Rework"),
                    _("due"),
                    calendar.proposal_rework_end.strftime("%d.%m.%Y"),
                ),
                "start_date": calendar.proposal_rework_start,
                "end_date": calendar.proposal_rework_end,
            },
        )
    if calendar.project_start and calendar.project_end:
        data.append(
            {
                "task_group": "project",
                "task_name": "{} ({} {})".format(
                    _("Final Proposal"),
                    _("due"),
                    calendar.project_end.strftime("%d.%m.%Y"),
                ),
                "start_date": calendar.project_start,
                "end_date": calendar.project_end,
            },
        )
    if calendar.project_rework_start and calendar.project_rework_end:
        data.append(
            {
                "task_group": "project",
                "task_name": "{} ({} {})".format(
                    _("Final Proposal Rework"),
                    _("due"),
                    calendar.project_rework_end.strftime("%d.%m.%Y"),
                ),
                "start_date": calendar.project_rework_start,
                "end_date": calendar.project_rework_end,
            },
        )
    if calendar.report_start and calendar.report_end:
        data.append(
            {
                "task_group": "thesis",
                "task_name": "{} ({} {})".format(
                    _("Progress Report"),
                    _("due"),
                    calendar.report_end.strftime("%d.%m.%Y"),
                ),
                "start_date": calendar.report_start,
                "end_date": calendar.report_end,
            },
        )
    if calendar.thesis_start and calendar.thesis_end:
        data.append(
            {
                "task_group": "thesis",
                "task_name": "{} ({} {})".format(
                    _("Master Thesis"),
                    _("due"),
                    calendar.thesis_end.strftime("%d.%m.%Y"),
                ),
                "start_date": calendar.thesis_start,
                "end_date": calendar.thesis_end,
            },
        )
    if calendar.defense_start and calendar.defense_end:
        data.append(
            {
                "task_group": "defence",
                "task_name": "{} ({} {})".format(
                    _("Thesis Defense"),
                    _("due"),
                    calendar.defense_end.strftime("%d.%m.%Y"),
                ),
                "start_date": calendar.defense_start,
                "end_date": calendar.defense_end,
            },
        )

    # Loading it into gviz_api.DataTable
    data_table = gviz_api.DataTable(description)
    data_table.LoadData(data)

    return data_table.ToJSon(
        columns_order=("task_group", "task_name", "start_date", "end_date"),
    )


class Calendar(models.Model):
    proposal_start = models.DateField(blank=True, null=True)
    proposal_end = models.DateTimeField(blank=True, null=True)
    proposal_rework_start = models.DateField(blank=True, null=True)
    proposal_rework_end = models.DateTimeField(blank=True, null=True)
    project_start = models.DateField(blank=True, null=True)
    project_end = models.DateTimeField(blank=True, null=True)
    project_rework_start = models.DateField(blank=True, null=True)
    project_rework_end = models.DateTimeField(blank=True, null=True)
    report_start = models.DateField(blank=True, null=True)
    report_end = models.DateTimeField(blank=True, null=True)
    thesis_start = models.DateField(blank=True, null=True)
    thesis_end = models.DateTimeField(blank=True, null=True)
    thesis_rework_start = models.DateField(blank=True, null=True)
    thesis_rework_end = models.DateTimeField(blank=True, null=True)
    defense_start = models.DateField(blank=True, null=True)
    defense_end = models.DateField(blank=True, null=True)
    defense_rework_start = models.DateField(blank=True, null=True)
    defense_rework_end = models.DateField(blank=True, null=True)
    session_end = models.DateField(blank=True, null=True)
    label = models.CharField(verbose_name=_("label"), max_length=50)
    comment = models.TextField(verbose_name=_("comment"), blank=True, null=True)
    custom = models.BooleanField(default=False, verbose_name=_("custom calendar"))
    last_update = models.DateTimeField(verbose_name=_("last update"), auto_now=True)

    class Meta:
        verbose_name = _("calendar")
        verbose_name_plural = _("calendars")
        ordering = ["proposal_start", "defense_end"]

    def __str__(self):
        return f"{self.label} ({self.proposal_start}-{self.defense_end})"

    def get_absolute_url(self):
        return reverse("tm:calendar-detail", kwargs={"pk": self.id})

    def __repr__(self):
        return f"<Calendar {self.id} : {self.label} ({self.proposal_start}-{self.defense_end})>"  # noqa: E501

    @property
    def proposal_due(self):
        return self.proposal_end and self.proposal_end < timezone.now()

    @property
    def proposal_rework_due(self):
        return self.proposal_rework_end and self.proposal_rework_end < timezone.now()

    @property
    def project_due(self):
        return self.project_end and self.project_end < timezone.now()

    @property
    def project_rework_due(self):
        return self.project_rework_end and self.project_rework_end < timezone.now()

    @property
    def report_open(self):
        return self.report_start and self.report_start <= timezone.now().date()

    @property
    def report_due(self):
        return self.report_end and self.report_end < timezone.now()

    @property
    def thesis_due(self):
        return self.thesis_end and self.thesis_end < timezone.now()

    @property
    def defense_due(self):
        return self.defense_end and self.defense_end < timezone.now().date()

    @property
    def expert_deadline(self):
        return self.defense_start - timedelta(days=50)

    @property
    def over_session(self):
        if self.session_end and self.thesis_end.date() > self.session_end:
            return _(
                "The calendar exceeds the end date of the semester ({})! "  # noqa: INT002
                "Registration for an additional semester may be required.".format(
                    date_format(
                        self.session_end,
                        format="SHORT_DATE_FORMAT",
                        use_l10n=True,
                    ),
                ),
            )
        return False


class PersonalLeave(models.Model):
    MEDICAL = "medical"
    ACADEMIC = "academic"
    PERSONAL = "personal"
    LEAVES = [
        (MEDICAL, _("medical leave")),
        (ACADEMIC, _("academic leave")),
        (PERSONAL, _("personal leave")),
    ]

    LEAVES_ICONS = {
        MEDICAL: "fa-first-aid",
        ACADEMIC: "fa-graduation-cap",
        PERSONAL: "fa-user",
    }

    attempt = models.ForeignKey(
        "Attempt",
        related_name="leaves",
        on_delete=models.CASCADE,
        verbose_name=_("attempt"),
    )
    first_day_leave = models.DateField(verbose_name=_("first day of leave"))
    last_day_leave = models.DateField(
        verbose_name=_("last day of leave"),
        help_text=_(
            "We consider that the student resumes work the day after the last day "
            "of his/her leave.",
        ),
    )
    duration_days = models.IntegerField(_("duration (days)"), blank=True)
    effective_days = models.IntegerField(
        _("effective duration (days)"),
        blank=True,
        default=0,
    )
    type_of_leave = models.CharField(
        max_length=20,
        choices=LEAVES,
        default=MEDICAL,
        verbose_name=_("leave type"),
    )
    basis = models.CharField(
        verbose_name=_("basis for the decision"),
        max_length=255,
        blank=True,
        null=True,
        help_text=_("For example: medical certificate issued on XX.XX.202X."),
    )
    editor = models.ForeignKey(
        User,
        related_name="leaves",
        on_delete=models.SET_NULL,
        verbose_name=_("editor"),
        null=True,
    )
    creation_date = models.DateTimeField(
        verbose_name=_("creation date"),
        auto_now_add=True,
    )

    __original_duration_days = None

    class Meta:
        verbose_name = _("personal leave")
        verbose_name_plural = _("personal leaves")
        ordering = ["creation_date"]
        permissions = [
            ("can_view_all_personalleaves", _("Can view all personal leaves")),
        ]

    def __str__(self):
        return f"{self.attempt.student} - {self.type_of_leave} ({self.first_day_leave} to {self.last_day_leave})"  # noqa: E501

    def save(self, *args, **kwargs):
        duration = self.last_day_leave - self.first_day_leave
        self.duration_days = duration.days + 1
        # We update/create the calendar accordingly
        if self.__original_duration_days != self.duration_days:
            transaction.on_commit(lambda: update_calendar.delay(self.id))
        super().save(*args, **kwargs)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__original_duration_days = self.duration_days

    def __repr__(self):
        return f"<PersonalLeave {self.id} : {self.attempt.student} - {self.type_of_leave} ({self.first_day_leave} to {self.last_day_leave})>"  # noqa: E501

    @property
    def icon(self):
        return self.LEAVES_ICONS[self.type_of_leave]


class Session(models.Model):
    name = models.CharField(verbose_name=_("academic year"), max_length=20)
    start_date = models.DateField(verbose_name=_("first day of the academic year"))
    end_date = models.DateField(verbose_name=_("last day of the academic year"))
    calendar = models.ForeignKey(
        Calendar,
        related_name="session",
        on_delete=models.PROTECT,
        verbose_name=_("calendar"),
    )
    require_printing_thesis = models.BooleanField(
        verbose_name=_("require printing"),
        default=False,
    )

    class Meta:
        verbose_name = _("session")
        verbose_name_plural = _("sessions")
        ordering = ["start_date"]
        permissions = [
            ("can_export_documents", _("Can export documents")),
            ("can_import_documents", _("Can import documents")),
        ]

    def __str__(self):
        return f"{self.name}"

    def __repr__(self):
        return f"<Session {self.id} : {self.name}>"


class Major(models.Model):
    name = models.CharField(verbose_name=_("major"), max_length=50)
    code = models.CharField(verbose_name=_("code"), max_length=5)
    admin_group = models.ForeignKey(
        Group,
        related_name="majors_admin",
        on_delete=models.SET_NULL,
        null=True,
    )
    academic_group = models.ForeignKey(
        Group,
        related_name="majors_academic",
        on_delete=models.SET_NULL,
        null=True,
    )
    secretary = models.ForeignKey(
        User,
        related_name="major_secretary",
        on_delete=models.SET_NULL,
        verbose_name=_("secretary"),
        blank=True,
        null=True,
    )
    coordinator = models.ForeignKey(
        User,
        related_name="major_coordinator",
        on_delete=models.SET_NULL,
        verbose_name=_("coordinator"),
        blank=True,
        null=True,
    )
    expert_documents_pack = models.FileField(
        upload_to=major_pdf_upload_path,
        verbose_name=_("expert documents pack"),
        null=True,
        blank=True,
        help_text=_(
            "Upload a PDF file containing all the documents required for the defense",
        ),
    )
    supervisor_documents_pack = models.FileField(
        upload_to=major_pdf_upload_path,
        verbose_name=_("supervisor documents pack"),
        null=True,
        blank=True,
        help_text=_(
            "Upload a PDF file containing all the documents required for the defense, at the exception of the evaluation form.",  # noqa: E501
        ),
    )
    statement_fees_form = models.FileField(
        upload_to=major_pdf_upload_path,
        verbose_name=_("statement of fees form"),
        null=True,
        blank=True,
    )

    class Meta:
        verbose_name = _("major")
        verbose_name_plural = _("majors")
        ordering = ["code"]

    def __str__(self):
        return f"{self.name}"

    def __repr__(self):
        return f"<Major {self.id} : {self.code}>"


class AttemptManager(models.Manager):
    def get_queryset(self):
        return (
            super()
            .get_queryset()
            .select_related("session", "major", "student", "calendar")
        )


class Attempt(models.Model):
    session = models.ForeignKey(
        "Session",
        related_name="attempts",
        on_delete=models.CASCADE,
        verbose_name=_("session"),
    )
    major = models.ForeignKey(
        "Major",
        related_name="attempts",
        on_delete=models.CASCADE,
        verbose_name=_("major"),
    )
    student = models.ForeignKey(
        User,
        related_name="attempts",
        on_delete=models.CASCADE,
        verbose_name=_("student"),
    )
    calendar = models.ForeignKey(
        "Calendar",
        related_name="attempt",
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        verbose_name=_("calendar"),
    )
    retake = models.BooleanField(
        default=False,
        verbose_name=_("retake of the master's thesis"),
    )
    done = models.BooleanField(default=False, verbose_name=_("attempt done"))
    archived = models.BooleanField(default=False, verbose_name=_("attempt archived"))
    creation_date = models.DateTimeField(auto_now_add=True)
    archive_date = models.DateTimeField(blank=True, null=True)

    objects = AttemptManager()

    class Meta:
        verbose_name = _("attempt")
        verbose_name_plural = _("attempts")
        ordering = ["session", "major", "student"]
        permissions = [
            ("can_view_all_attempts", _("Can view all attempts")),
            ("can_import_students", _("Can import students")),
            ("can_search_attempt", _("Can search attempt")),
        ]

    def __str__(self):
        return f"{self.session} - {self.major.code} - {self.student}"

    def save(self, *args, **kwargs):
        if self._state.adding:
            # We can only have one active attempt for each student
            attempts = Attempt.objects.filter(student=self.student, archived=False)
            if attempts.count() > 0:
                # This is a retake of the master's thesis
                self.retake = True
            for attempt in attempts:
                attempt.archived = True
                attempt.archive_date = timezone.now()
                attempt.save()
        super().save(*args, **kwargs)
        # We also set the attempt on the user model
        if not self.archived:
            self.student.attempt = self
            self.student.save()

    def get_absolute_url(self):
        return reverse("tm:attempt", kwargs={"pk": self.id})

    def __repr__(self):
        return (
            f"<Attempt {self.id} : {self.session} - {self.major.code} - {self.student}>"
        )

    @cached_property
    def accepted_proposals(self):
        return Proposal.objects.filter(
            attempt=self,
            workflow__status=ProposalWorkflow.ACCEPTED,
        ).exclude(workflow__status=ProposalWorkflow.WITHDRAWN)

    @cached_property
    def proposal_done(self):
        return self.accepted_proposals.exists()

    @cached_property
    def accepted_project(self):
        return Project.objects.filter(attempt=self, accepted=True)

    @cached_property
    def project_done(self):
        return self.accepted_project.exists()

    @cached_property
    def place(self):
        return self.projects.filter(accepted=True).exists()

    @cached_property
    def thesis_done(self):
        return self.theses.filter(submitted=True).exists()

    @cached_property
    def defense_done(self):
        return (
            Defense.objects.filter(
                master_thesis__attempt=self,
            )
            .exclude(
                result=MasterThesis.NOT_ASSESSED,
            )
            .exists()
        )

    @cached_property
    def current_calendar(self):
        if self.calendar:
            return self.calendar
        return self.session.calendar

    @cached_property
    def supervisor(self):
        if self.thesis_done:
            supervisor = (
                self.theses.filter(submitted=True)
                .order_by("-submission_date")[0]
                .supervisor
            )
        elif self.project_done:
            supervisor = (
                self.projects.filter(accepted=True).order_by("-revision")[0].supervisor
            )
        elif self.proposal_done:
            supervisor = (
                self.proposals.filter(workflow__status=ProposalWorkflow.ACCEPTED)
                .order_by("-id")[0]
                .supervisor
            )
        else:
            supervisor = None
        return supervisor

    @cached_property
    def co_supervisors(self):
        if self.thesis_done:
            co_supervisors = (
                self.theses.filter(submitted=True)
                .order_by("-submission_date")[0]
                .co_supervisors
            )
        elif self.project_done:
            co_supervisors = (
                self.projects.filter(accepted=True)
                .order_by("-revision")[0]
                .co_supervisors
            )
        else:
            co_supervisors = None

        if co_supervisors and co_supervisors.count() > 0:
            return co_supervisors
        return None

    @property
    def next_deadline(self):
        calendar = self.current_calendar
        # We check each deadline
        if self.defense_done:
            return _("Congratulations, you are done with your master's thesis!")
        if self.thesis_done:
            return _(f"master's thesis defense on {calendar.defense_start}")  # noqa: INT001
        if (
            self.project_done
            or self.proposal_done
            or not self.proposal_done
            and calendar.project_due
        ):
            pass
        elif calendar.proposal_due and not self.proposal_done:
            # Must rework on the proposal
            deliverable = _("master's thesis subject proposal")
            deadline = calendar.project_end
        else:
            # Nothing started, we are at the begining
            deliverable = _("master's thesis subject proposal")
            deadline = calendar.project_end

        deadlines = {
            "proposal_end": calendar.proposal_end,
            "proposal_rework_end": calendar.proposal_rework_end,
            "project_end": calendar.project_end,
            "project_rework_end": calendar.project_rework_end,
            "report_end": calendar.report_end,
            "thesis_end": calendar.thesis_end,
        }

        return {
            "deliverable": deliverable,
            "deadline": deadline,
            "deadlines": deadlines,
        }

    @property
    def has_exceptions(self):
        return bool(self.block_tm or self.block_defense)

    @property
    def block_tm(self):
        return self.exceptions(manager="current").filter(block_tm=True).count() > 0

    @property
    def block_defense(self):
        return self.exceptions(manager="current").filter(block_defense=True).count() > 0

    @property
    def exception_probable_resolution_date(self):
        exception = self.exceptions(manager="current").latest(
            "probable_resolution_date",
        )
        if exception:
            return exception.probable_resolution_date
        return None


class ProposalManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().select_related("attempt__student")


class Proposal(models.Model):
    attempt = models.ForeignKey(
        "Attempt",
        related_name="proposals",
        on_delete=models.CASCADE,
        verbose_name=_("attempt"),
    )
    title = models.CharField(verbose_name=_("title"), max_length=500)
    background = QuillField(verbose_name=_("background"), blank=True, null=True)
    problem_statement = QuillField(
        verbose_name=_("problem statement"),
        blank=True,
        null=True,
    )
    research_questions = QuillField(
        verbose_name=_("research questions"),
        blank=True,
        null=True,
    )
    methodology = QuillField(verbose_name=_("methodology"), blank=True, null=True)
    partner = QuillField(verbose_name=_("partner"), blank=True, null=True)
    accepted = models.BooleanField(verbose_name=_("proposal accepted"), default=False)
    creation_date = models.DateTimeField(auto_now_add=True)

    objects = ProposalManager()

    class Meta:
        verbose_name = _("pre-proposal")
        verbose_name_plural = _("pre-proposals")
        ordering = ["attempt", "title"]
        permissions = [
            ("can_view_all_proposals", _("Can view all pre-proposals")),
        ]

    def __str__(self):
        return f"{self.attempt.student} - {self.title}"

    def get_absolute_url(self):
        return reverse("tm:proposal", kwargs={"pk": self.id})

    def __repr__(self):
        return f"<Proposal {self.id} : {self.attempt.student} - {self.title}>"

    @property
    def current_status(self):
        # Get the last status of the proposal
        return (
            self.workflow.exclude(status=ProposalWorkflow.COMMENT)
            .order_by("-creation_date")
            .first()
        )

    @property
    def supervisors(self):
        supervisor_ids = (
            self.assignments.exclude(validated=False)
            .exclude(accepted=False)
            .exclude(archive_date__isnull=False)
            .values_list("supervisor_id", flat=True)
        )
        return User.objects.filter(id__in=supervisor_ids)

    @property
    def external_supervisors(self):
        external_supervisors = (
            self.assignments.exclude(validated=False)
            .exclude(accepted=False)
            .exclude(supervisor__isnull=False)
        )
        if external_supervisors.exists():
            supervisors_list = [
                assignment.suggestion for assignment in external_supervisors
            ]
            return ", ".join(supervisors_list)
        return None

    @property
    def supervisor(self):
        supervisor_queryset = self.assignments.filter(accepted=True)
        if supervisor_queryset.count() == 1:
            return supervisor_queryset[0].supervisor
        return None


class ProposalWorkflow(models.Model):
    DRAFT = "draft"
    SUBMITTED = "submitted"
    REWORK = "rework"
    ACCEPTED = "accepted"
    REJECTED = "rejected"
    WITHDRAWN = "withdrawn"
    COMMENT = "comment"
    WORKFLOW_STATES = [
        (DRAFT, _("draft")),
        (SUBMITTED, _("submitted")),
        (REWORK, _("rework")),
        (ACCEPTED, _("accepted")),
        (REJECTED, _("rejected")),
        (WITHDRAWN, _("withdrawn")),
        (COMMENT, _("comment")),
    ]
    WORKFLOW_BADGES = {
        DRAFT: "bg-secondary",
        SUBMITTED: "bg-primary",
        REWORK: "bg-warning text-dark",
        ACCEPTED: "bg-success",
        REJECTED: "bg-danger",
        WITHDRAWN: "bg-dark",
        COMMENT: "bg-info text-dark",
    }
    STUDENT = "student"
    SUPERVISOR = "supervisor"
    MH = "mh"
    ROLES = [
        (STUDENT, _("student")),
        (SUPERVISOR, _("supervisor")),
        (MH, _("module head")),
    ]
    proposal = models.ForeignKey(
        "Proposal",
        related_name="workflow",
        on_delete=models.CASCADE,
        verbose_name=_("proposal"),
    )
    status = models.CharField(
        max_length=20,
        choices=WORKFLOW_STATES,
        default=DRAFT,
        verbose_name=_("status"),
    )
    user = models.ForeignKey(
        User,
        related_name="decisions",
        on_delete=models.CASCADE,
        verbose_name=_("user"),
    )
    role = models.CharField(
        verbose_name=_("role"),
        max_length=10,
        choices=ROLES,
        blank=True,
        null=True,
    )
    comments = QuillField(verbose_name=_("comments"), blank=True, null=True)
    creation_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = _("proposal workflow")
        verbose_name_plural = _("proposal workflows")
        ordering = ["proposal", "creation_date"]

    def __str__(self):
        return f"{self.proposal} - {self.status}"

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        # We change the accepted field of the proposal
        if self.status == self.ACCEPTED:
            self.proposal.accepted = True
            self.proposal.save()
        elif self.status != self.COMMENT:
            self.proposal.accepted = False
            self.proposal.save()
        # We cancel the assignments if the proposal is rejected or withdrawn
        if self.status in [self.REJECTED, self.WITHDRAWN]:
            assignments = self.proposal.assignments.exclude(accepted=False)
            for assignment in assignments:
                if not assignment.archive_date:
                    assignment.archive_date = timezone.now()
                    assignment.save()
        # We notify the user
        if self.status != self.DRAFT:
            transaction.on_commit(lambda: send_notification_workflow.delay(self.id))

    def __repr__(self):
        return f"<ProposalWorkflow {self.id} : {self.proposal} - {self.status}>"

    @property
    def badge_class(self):
        return self.WORKFLOW_BADGES[self.status]


class AssignmentManager(models.Manager):
    def get_queryset(self):
        return (
            super()
            .get_queryset()
            .select_related("proposal__attempt__student", "supervisor")
        )


class Assignment(models.Model):
    proposal = models.ForeignKey(
        "Proposal",
        related_name="assignments",
        on_delete=models.CASCADE,
        verbose_name=_("proposal"),
    )
    supervisor = models.ForeignKey(
        User,
        related_name="assignments",
        blank=True,
        null=True,
        on_delete=models.CASCADE,
        verbose_name=_("supervisor"),
    )
    suggestion = models.CharField(
        max_length=100,
        verbose_name=_("other proposal"),
        blank=True,
        null=True,
        help_text=_(
            "If you do not find the supervisor in the list, please indicate his/her "
            "name and the name of the school",
        ),
    )
    validated = models.BooleanField(verbose_name=_("validated"), blank=True, null=True)
    request_date = models.DateField(
        verbose_name=_("request date"),
        blank=True,
        null=True,
    )
    accepted = models.BooleanField(verbose_name=_("accepted"), blank=True, null=True)
    response_date = models.DateField(
        verbose_name=_("response date"),
        blank=True,
        null=True,
    )
    comments = QuillField(
        verbose_name=_("comments for the student"),
        blank=True,
        null=True,
    )
    comments_mh = models.TextField(
        verbose_name=_("comments for the master theses coordinator"),
        blank=True,
        null=True,
    )
    reminders = models.BooleanField(default=True, verbose_name=_("reminders"))
    last_reminder = models.DateField(
        verbose_name=_("last reminder"),
        blank=True,
        null=True,
    )
    creation_date = models.DateTimeField(auto_now_add=True)
    archive_date = models.DateTimeField(blank=True, null=True)

    objects = AssignmentManager()

    __original_validated = None
    __original_accepted = None
    __original_archive_date = None

    class Meta:
        verbose_name = _("assignment")
        verbose_name_plural = _("assignments")
        ordering = ["proposal", "supervisor", "accepted"]
        permissions = [
            ("can_view_all_assignments", _("Can view all assignments")),
        ]

    def __str__(self):
        return f"{self.supervisor} - {self.accepted} - {self.proposal}"

    def save(self, *args, **kwargs):  # noqa: C901
        if self.accepted != self.__original_accepted and not self.accepted:
            self.archive_date = timezone.now()
        super().save()
        if self.accepted != self.__original_accepted:
            send_notification_decision(self.id)
            if self.accepted:
                # We cancel all other assignments for the proposal
                assignments = self.proposal.assignments.exclude(id=self.id).exclude(
                    accepted=False,
                )
                for assignment in assignments:
                    if not assignment.archive_date:
                        assignment.archive_date = timezone.now()
                        assignment.save()
        if self.archive_date != self.__original_archive_date:
            # We archived the assignment and we must notify the user
            # only to validated assignments
            if self.validated and self.accepted is None:
                send_notification_archive.delay(self.id)
            elif self.accepted:
                # We accepted the assignment and we must notify the user
                send_notification_archive.delay(self.id, supervisor=True)
        if self.validated != self.__original_validated:
            if self.validated:
                # We validated the assignment and we must notify the user
                send_notification_validated.delay(self.id)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__original_accepted = self.accepted
        self.__original_validated = self.validated
        self.__original_archive_date = self.archive_date

    def __repr__(self):
        return f"<Assignment {self.id} : {self.supervisor} - {self.accepted} - {self.proposal}>"  # noqa: E501

    @property
    def waiting_response(self):
        if (
            self.request_date
            and self.accepted is None
            and self.validated
            and not self.archive_date
        ):
            return timezone.now().date() - self.request_date
        return None


class ProjectManager(models.Manager):
    def get_queryset(self):
        return (
            super()
            .get_queryset()
            .select_related(
                "attempt__student",
                "proposal__attempt__student",
                "supervisor",
            )
            .prefetch_related("co_supervisors")
        )


class Project(models.Model):
    DRAFT = "draft"
    SUBMITTED = "submitted"
    UNDER_REVIEW = "under review"
    PASS = "pass"  # noqa: S105
    REWORK = "rework"
    FAIL = "fail"

    PROJECT_STATES = {
        DRAFT: _("draft"),
        SUBMITTED: _("submitted"),
        UNDER_REVIEW: _("under review"),
        PASS: _("pass"),
        REWORK: _("rework"),
        FAIL: _("fail"),
    }
    PROJECT_BADGES = {
        DRAFT: "bg-secondary",
        SUBMITTED: "bg-primary",
        UNDER_REVIEW: "bg-dark",
        PASS: "bg-success",
        REWORK: "bg-warning text-dark",
        FAIL: "bg-danger",
    }

    attempt = models.ForeignKey(
        "Attempt",
        related_name="projects",
        on_delete=models.CASCADE,
        verbose_name=_("attempt"),
    )
    proposal = models.ForeignKey(
        "Proposal",
        related_name="projects",
        on_delete=models.CASCADE,
        verbose_name=_("proposal"),
    )
    revision = models.IntegerField(blank=True, null=True, verbose_name=_("revision"))
    title = models.CharField(verbose_name=_("title"), max_length=500)
    confidential = models.BooleanField(
        default=False,
        verbose_name=_("confidential"),
        help_text=_(
            "Check if the master thesis will be confidential. You can always change this setting when submitting the thesis. If the thesis is confidential, it will not be published and may be subject to a confidentiality agreement.",  # noqa: E501
        ),
    )
    supervisor = models.ForeignKey(
        User,
        related_name="projects",
        on_delete=models.CASCADE,
        verbose_name=_("supervisor"),
    )
    co_supervisors = models.ManyToManyField(
        User,
        related_name="co_projects",
        verbose_name=_("co-supervisors"),
        blank=True,
    )
    pdf_file = models.FileField(
        upload_to=project_pdf_upload_path,
        verbose_name=_("thesis disposition (PDF file)"),
        help_text=_("Please submit one PDF file with all attachments included."),
        null=True,
        blank=True,
    )
    submitted = models.BooleanField(default=False, verbose_name=_("submitted"))
    submission_date = models.DateField(
        verbose_name=_("submission date"),
        blank=True,
        null=True,
    )
    accepted = models.BooleanField(verbose_name=_("accepted"), blank=True, null=True)
    response_date = models.DateField(
        verbose_name=_("response date"),
        blank=True,
        null=True,
    )
    comments = QuillField(
        verbose_name=_("comments"),
        blank=True,
        null=True,
        help_text=_(
            "If this is not the first revision, please explain the changes that have been made since the previous version.",  # noqa: E501
        ),
    )
    pass_content = models.BooleanField(
        verbose_name=_("pass content"),
        blank=True,
        null=True,
    )
    pass_methodology = models.BooleanField(
        verbose_name=_("pass methodology"),
        blank=True,
        null=True,
    )
    grade_methodology = models.DecimalField(
        verbose_name=_("grade methodology"),
        max_digits=2,
        decimal_places=1,
        blank=True,
        null=True,
    )
    feedback_content = QuillField(
        verbose_name=_("feedback content"),
        blank=True,
        null=True,
    )
    feedback_methodology = QuillField(
        verbose_name=_("feedback methodology"),
        blank=True,
        null=True,
    )
    reviewer_content = models.ForeignKey(
        User,
        related_name="content_reviews",
        blank=True,
        null=True,
        on_delete=models.CASCADE,
        verbose_name=_("content reviewer"),
    )
    reviewer_methodology = models.ForeignKey(
        User,
        related_name="methodology_reviews",
        blank=True,
        null=True,
        on_delete=models.CASCADE,
        verbose_name=_("methodology reviewer"),
    )
    creation_date = models.DateTimeField(auto_now_add=True)
    publish_feedbacks = models.BooleanField(
        default=False,
        verbose_name=_("publish feedback"),
    )

    objects = ProjectManager()

    __original_publish_feedbacks = None

    class Meta:
        verbose_name = _("final proposal")
        verbose_name_plural = _("final proposals")
        ordering = ["proposal", "supervisor", "-revision"]
        permissions = [
            ("can_view_unpublished_feedbacks", _("Can view unpublished feedbacks")),
            ("can_view_all_projects", _("Can view all final proposals")),
            ("can_grade_projects", _("Can grade final proposals for majors")),
        ]

    def __str__(self):
        return f"{self.supervisor} - {self.accepted} - {self.proposal}"

    def save(self, *args, **kwargs):
        if not self.revision:
            last_revision = self.proposal.projects.order_by("-revision").first()
            if last_revision:
                revision = last_revision.revision + 1
                if last_revision.grade_methodology >= 4:  # noqa: PLR2004
                    self.pass_methodology = last_revision.pass_methodology
                    self.grade_methodology = last_revision.grade_methodology
                    self.feedback_methodology = json.dumps(
                        {
                            "delta": "",
                            "html": str(
                                _(
                                    "There is no new evaluation of the methodological part. The original assessment is retained.",  # noqa: E501
                                ),
                            ),
                        },
                    )
                    self.reviewer_methodology = last_revision.reviewer_methodology
            else:
                revision = 1
            self.revision = revision
        if self.pass_content is None or self.pass_methodology is None:
            self.accepted = None
        elif self.pass_content and self.pass_methodology:
            self.accepted = True
        else:
            self.accepted = False
        super().save(*args, **kwargs)
        if self.__original_publish_feedbacks != self.publish_feedbacks:
            if self.publish_feedbacks is True and self.accepted is not None:
                send_notification_disposition(self.id)
                if self.accepted:
                    # We create the master thesis
                    thesis = MasterThesis(
                        attempt=self.attempt,
                        project=self,
                        title=self.title,
                        confidential=self.confidential,
                        supervisor=self.supervisor,
                    )
                    thesis.save()
                    thesis.co_supervisors.add(*self.co_supervisors.all())

    def get_absolute_url(self):
        return reverse("tm:disposition", kwargs={"pk": self.id})

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__original_publish_feedbacks = self.publish_feedbacks

    def __repr__(self):
        return f"<Project {self.id} : {self.supervisor} - {self.accepted} - {self.proposal}>"  # noqa: E501

    @property
    def status(self):
        # Get the current status of the project
        if not self.submitted:
            return Project.DRAFT
        if self.accepted is not None:
            if self.accepted:
                return Project.PASS
            if self.grade_methodology >= 3.5:  # noqa: PLR2004
                return Project.REWORK
            return Project.FAIL
        return Project.UNDER_REVIEW

    @property
    def display_status(self):
        return self.PROJECT_STATES[self.status]

    @property
    def display_public_status(self):
        # Display the current public status of the project (for students and
        # supervisors)
        if not self.submitted:
            return self.PROJECT_STATES[Project.DRAFT]
        if self.publish_feedbacks:
            return self.PROJECT_STATES[self.status]
        return self.PROJECT_STATES[Project.UNDER_REVIEW]

    @property
    def badge_class(self):
        return self.PROJECT_BADGES[self.status]

    @property
    def public_badge_class(self):
        if not self.submitted:
            return self.PROJECT_BADGES[Project.DRAFT]
        if self.publish_feedbacks:
            return self.PROJECT_BADGES[self.status]
        return self.PROJECT_BADGES[Project.UNDER_REVIEW]

    @property
    def content_status(self):
        if not self.submitted:
            return Project.DRAFT
        if self.pass_content:
            return Project.PASS
        if self.pass_content is False:
            return Project.REWORK
        return Project.UNDER_REVIEW

    @property
    def display_content_status(self):
        return self.PROJECT_STATES[self.content_status]

    @property
    def badge_content_status(self):
        return self.PROJECT_BADGES[self.content_status]

    @property
    def methodology_status(self):
        if not self.submitted:
            return Project.DRAFT
        if self.pass_methodology:
            return Project.PASS
        if self.grade_methodology:
            if self.grade_methodology >= 3.5:  # noqa: PLR2004
                return Project.REWORK
            if self.grade_methodology < 3.5:  # noqa: PLR2004
                return Project.FAIL
        return Project.UNDER_REVIEW

    @property
    def display_methodology_status(self):
        return self.PROJECT_STATES[self.methodology_status]

    @property
    def badge_methodology_status(self):
        return self.PROJECT_BADGES[self.methodology_status]


class MasterThesisManager(models.Manager):
    def get_queryset(self):
        return (
            super()
            .get_queryset()
            .select_related("attempt__student", "attempt__session")
        )


class MasterThesis(models.Model):
    NOT_ASSESSED = 0
    PASS = 1
    REMEDIATION = 2
    FAIL = 3
    ASSESSMENTS = [
        (NOT_ASSESSED, _("not assessed")),
        (PASS, _("passed")),
        (REMEDIATION, _("remediation")),
        (FAIL, _("failed")),
    ]

    RESULT_CLASSES = {
        NOT_ASSESSED: "bg-secondary",
        PASS: "bg-success",
        REMEDIATION: "bg-warning text-dark",
        FAIL: "bg-danger",
    }

    APPROVED = True
    REFUSED = False
    UNDEFINED = None
    PUBLICATION = [
        (APPROVED, _("approved")),
        (REFUSED, _("refused")),
        (UNDEFINED, _("undefined")),
    ]

    attempt = models.ForeignKey(
        "Attempt",
        related_name="theses",
        on_delete=models.CASCADE,
        verbose_name=_("attempt"),
    )
    project = models.ForeignKey(
        "Project",
        related_name="theses",
        on_delete=models.CASCADE,
        verbose_name=_("project"),
    )
    title = models.CharField(verbose_name=_("title"), max_length=500)
    confidential = models.BooleanField(default=False, verbose_name=_("confidential"))
    authorization_publish = models.BooleanField(
        default=UNDEFINED,
        choices=PUBLICATION,
        blank=True,
        null=True,
        verbose_name=_("authorization to publish"),
    )
    published = models.BooleanField(default=False, verbose_name=_("published"))
    abstract = QuillField(verbose_name=_("abstract"), blank=True, null=True)
    supervisor = models.ForeignKey(
        User,
        related_name="theses",
        on_delete=models.CASCADE,
        verbose_name=_("supervisor"),
    )
    co_supervisors = models.ManyToManyField(
        User,
        related_name="co_theses",
        verbose_name=_("co-supervisors"),
        blank=True,
    )
    pdf_file = models.FileField(
        upload_to=thesis_pdf_upload_path,
        verbose_name=_("pdf file"),
        null=True,
        blank=True,
        help_text=_(
            "Upload of a PDF file with the entire master's thesis and appendices.",
        ),
    )
    zip_file = models.FileField(
        upload_to=thesis_pdf_upload_path,
        verbose_name=_("zip file"),
        null=True,
        blank=True,
        help_text=_(
            "If applicable, upload a ZIP file containing additional materials related to the master's thesis (transcripts, computer code, surveys data, etc.).",  # noqa: E501
        ),
    )
    pdf_retake_file = models.FileField(
        upload_to=thesis_pdf_upload_path,
        verbose_name=_("pdf file retake"),
        null=True,
        blank=True,
        help_text=_(
            "Upload a new PDF file with the entire master's thesis and appendices for "
            "the remediation.",
        ),
    )
    zip_retake_file = models.FileField(
        upload_to=thesis_pdf_upload_path,
        verbose_name=_("zip file retake"),
        null=True,
        blank=True,
        help_text=_(
            "If applicable, upload a new ZIP file containing additional materials related to the remediation of the master's thesis (transcripts, computer code, surveys data, etc.).",  # noqa: E501
        ),
    )
    confidential_agreement_file = models.FileField(
        upload_to=thesis_pdf_upload_path,
        verbose_name=_("confidential agreement form"),
        null=True,
        blank=True,
        help_text=_(
            "If the thesis is confidential, you must upload the signed confidentiality agreement form before submitting the thesis. You must have the document signed by all parties involved.",  # noqa: E501
        ),
    )
    authorization_publish_file = models.FileField(
        upload_to=thesis_pdf_upload_path,
        verbose_name=_("authorization to publish form"),
        null=True,
        blank=True,
        help_text=_(
            "Complete and upload the authorization to publish the thesis before the defense. Department signature will be added later.",  # noqa: E501
        ),
    )

    submitted = models.BooleanField(default=False, verbose_name=_("submitted"))
    submission_date = models.DateTimeField(
        verbose_name=_("submission date"),
        blank=True,
        null=True,
    )

    defense_allowed = models.BooleanField(
        default=True,
        verbose_name=_("defence allowed"),
    )
    defense_delay_reason = models.TextField(
        verbose_name=_("reason for defense delay"),
        blank=True,
        null=True,
    )
    expert = models.ForeignKey(
        User,
        related_name="evaluations",
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        verbose_name=_("expert"),
    )
    expert_name = models.CharField(
        verbose_name=_("expert name"),
        max_length=250,
        blank=True,
        null=True,
        help_text=_(
            "This field is used as a fallback if the expert is deleted from the database.",  # noqa: E501
        ),
    )
    expert_confirmed = models.BooleanField(
        default=False,
        verbose_name=_("expert confirmed"),
    )
    expert_confirmation_date = models.DateField(
        verbose_name=_("expert confirmation date"),
        blank=True,
        null=True,
    )
    expert_validation = models.BooleanField(
        default=False,
        verbose_name=_("expert validated"),
    )
    expert_validation_date = models.DateField(
        verbose_name=_("expert validation date"),
        blank=True,
        null=True,
    )

    paper_submission = models.BooleanField(
        verbose_name=_("hard copies required"),
        blank=True,
        null=True,
    )
    paper_submission_supervisor = models.BooleanField(
        verbose_name=_("hard copy required for supervisor"),
        blank=True,
        null=True,
        help_text=_(
            "Indicate if you wish to receive a hard copy of the dissertation prior to the defense.",  # noqa: E501
        ),
    )
    paper_submission_expert = models.BooleanField(
        verbose_name=_("hard copy required for expert"),
        blank=True,
        null=True,
        help_text=_(
            "Indicate if you wish to receive a hard copy of the dissertation prior to the defense.",  # noqa: E501
        ),
    )

    hard_copies = models.IntegerField(
        verbose_name=_("number of hard copies"),
        blank=True,
        null=True,
    )

    hard_copies_received = models.BooleanField(
        verbose_name=_("hard copies received"),
        default=False,
    )
    hard_copies_received_date = models.DateTimeField(
        verbose_name=_("hard copies received date"),
        blank=True,
        null=True,
    )
    hard_copies_received_user = models.ForeignKey(
        User,
        related_name="receptions",
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        verbose_name=_("hard copies received secretary"),
    )

    compilatio_id = models.CharField(
        verbose_name=_("compilatio document id"),
        max_length=250,
        blank=True,
        null=True,
    )
    compilatio_score = models.FloatField(
        verbose_name=_("compilatio score"),
        blank=True,
        null=True,
    )
    compilatio_report = models.FileField(
        upload_to=thesis_pdf_upload_path,
        verbose_name=_("compilatio report"),
        blank=True,
        null=True,
    )

    result = models.IntegerField(
        verbose_name=_("result"),
        choices=ASSESSMENTS,
        default=NOT_ASSESSED,
    )

    creation_date = models.DateTimeField(auto_now_add=True)

    objects = MasterThesisManager()

    __original_compilatio_score = None
    __original_authorization_publish_file = None
    __original_submitted = None

    class Meta:
        verbose_name = _("master thesis")
        verbose_name_plural = _("master theses")
        ordering = ["attempt__session", "attempt__student"]
        permissions = [
            ("can_view_confidential_theses", _("Can view confidential theses")),
            ("can_view_all_theses", _("Can view all theses")),
        ]

    def __str__(self):
        return f"{self.attempt.student} ({self.attempt.session})"

    def save(self, *args, **kwargs):
        hard_copies = 0
        if (
            self.attempt.session.require_printing_thesis
            or self.paper_submission_supervisor
            or self.paper_submission_expert
        ):
            self.paper_submission = True
            if self.attempt.session.require_printing_thesis:
                hard_copies += 1
            if self.paper_submission_supervisor:
                hard_copies += 1
            if self.paper_submission_expert:
                hard_copies += 1
            self.hard_copies = hard_copies
        if self.expert and self.expert_confirmed:
            self.expert_name = self.expert.name
        if self.compilatio_score != self.__original_compilatio_score:
            transaction.on_commit(lambda: send_compilatio_certificate.delay(self.id))
        if (
            self.authorization_publish_file
            != self.__original_authorization_publish_file
        ):
            transaction.on_commit(
                lambda: send_record_publication_notification.delay(self.id),
            )
        if self.submitted != self.__original_submitted and self.submitted is True:
            transaction.on_commit(
                lambda: send_thesis_submitted_notification.delay(self.id),
            )
        self.published = self.update_publish()
        super().save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse("tm:view-thesis", kwargs={"pk": self.id})

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__original_compilatio_score = self.compilatio_score
        self.__original_authorization_publish_file = self.authorization_publish_file
        self.__original_submitted = self.submitted

    def __repr__(self):
        return f"<MasterThesis {self.id} : {self.attempt.student} ({self.attempt.session})>"  # noqa: E501

    @cached_property
    def student_report(self):
        return self.reports.filter(role=ProgressReport.STUDENT).first()

    @cached_property
    def supervisor_report(self):
        return self.reports.filter(role=ProgressReport.SUPERVISOR).first()

    @cached_property
    def reports_agreement(self):
        if self.student_report and self.supervisor_report:
            if (
                abs(
                    self.student_report.global_assessment
                    - self.supervisor_report.global_assessment,
                )
                <= 1
            ):
                return True
        return False

    @cached_property
    def reports_validated(self):
        return (
            self.student_report
            and self.supervisor_report
            and self.student_report.validated
            and self.supervisor_report.validated
        )

    @cached_property
    def ready(self):
        if not self.confidential:
            return self.pdf_file and not self.attempt.current_calendar.thesis_due
        return (
            self.pdf_file
            and self.confidential_agreement_file
            and not self.attempt.current_calendar.thesis_due
        )

    @cached_property
    def hard_copies_status(self):
        return bool(
            self.paper_submission
            and self.hard_copies_received
            or not self.paper_submission,
        )

    def update_publish(self):
        if not self.confidential:
            if self.authorization_publish:
                if self.defenses.exists():
                    if self.defenses.last().grade:
                        if self.defenses.last().grade >= 5:  # noqa: PLR2004
                            return True
        return False

    def get_result_class(self):
        return self.RESULT_CLASSES[self.result]


class ProgressReport(models.Model):
    STUDENT = "student"
    SUPERVISOR = "supervisor"
    MH = "mh"
    EXPERT = "expert"
    ROLES = [
        (STUDENT, _("student")),
        (SUPERVISOR, _("supervisor")),
        (MH, _("module head")),
        (EXPERT, _("expert")),
    ]
    ASSESSMENT = [
        (
            1,
            _(
                "1 - The thesis is not progressing well. It may not meet the minimum requirements for completion.",  # noqa: E501
            ),
        ),
        (
            2,
            _(
                "2 - The thesis is progressing, but has significant gaps or limitations that must be addressed before it can be satisfactorily completed.",  # noqa: E501
            ),
        ),
        (
            3,
            _(
                "3 - The thesis is progressing satisfactorily. It is on track to meet the minimum requirements for completion.",  # noqa: E501
            ),
        ),
        (
            4,
            _(
                "4 - The thesis is progressing well. It is likely to meet the requirements for completion with a high quality",  # noqa: E501
            ),
        ),
        (
            5,
            _(
                "5 - The thesis is progressing exceptionally well. It is likely to exceed the completion requirements",  # noqa: E501
            ),
        ),
    ]

    master_thesis = models.ForeignKey(
        "MasterThesis",
        related_name="reports",
        on_delete=models.CASCADE,
        verbose_name=_("thesis"),
    )
    user = models.ForeignKey(
        User,
        related_name="reports",
        on_delete=models.CASCADE,
        verbose_name=_("user"),
    )
    role = models.CharField(
        verbose_name=_("role"),
        max_length=10,
        choices=ROLES,
        blank=True,
        null=True,
    )
    global_assessment = models.IntegerField(
        verbose_name=_("global assessment"),
        blank=True,
        null=True,
        choices=ASSESSMENT,
        help_text=_("Sur une échelle de 1 à 5, comment se déroule le travail ?"),
    )
    abstract = QuillField(
        verbose_name=_("abstract"),
        blank=True,
        null=True,
        help_text=_("Rappel de la problématique"),
    )
    progress_report = QuillField(
        verbose_name=_("progress report"),
        blank=True,
        null=True,
        help_text=_("Etat d'avancement du travail"),
    )
    problems_encountered = QuillField(
        verbose_name=_("problems encountered"),
        blank=True,
        null=True,
        help_text=_("Problèmes rencontrés"),
    )
    ability_to_achieve = QuillField(
        verbose_name=_("ability to achieve the objectives"),
        blank=True,
        null=True,
        help_text=_("Estimation de la capacité à atteindre l'objectif final"),
    )
    comments_for_mh = QuillField(
        verbose_name=_("comments for the mh"),
        blank=True,
        null=True,
        help_text=_("Eléments importants à signaler au/à la Responsable d'orientation"),
    )
    submitted = models.BooleanField(default=False, verbose_name=_("submitted"))
    submission_date = models.DateField(
        verbose_name=_("submission date"),
        blank=True,
        null=True,
    )
    forced_submission = models.BooleanField(
        verbose_name=_("forced submission"),
        default=False,
    )
    forced_submission_user = models.ForeignKey(
        User,
        related_name="forced_reports",
        on_delete=models.SET_NULL,
        verbose_name=_("forced by"),
        blank=True,
        null=True,
    )
    creation_date = models.DateTimeField(auto_now_add=True)
    validated = models.BooleanField(default=False, verbose_name=_("validated"))
    validation_date = models.DateTimeField(
        verbose_name=_("validation date"),
        blank=True,
        null=True,
    )

    class Meta:
        verbose_name = _("progress report")
        verbose_name_plural = _("progress reports")
        ordering = ["master_thesis"]
        permissions = [
            ("can_view_all_progressreports", _("Can view all progress reports")),
        ]

    def __str__(self):
        return f"{self.master_thesis} ({self.user} [{self.role}])"

    def get_absolute_url(self):
        return reverse("tm:view-progress-report", kwargs={"pk": self.id})

    def __repr__(self):
        return f"<ProgressReport {self.id} : {self.master_thesis} ({self.user} [{self.role}])>"  # noqa: E501

    @property
    def to_validate(self):
        return bool(self.submitted and not self.validated)


class CurrentExceptionManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(cleared=False)


class StudentException(models.Model):
    attempt = models.ForeignKey(
        "Attempt",
        related_name="exceptions",
        on_delete=models.CASCADE,
        verbose_name=_("attempt"),
    )
    reason = models.CharField(
        verbose_name=_("reason"),
        max_length=255,
        blank=True,
        null=True,
        help_text=_(
            "If there are several exceptions, create an exception for each specific situation",  # noqa: E501
        ),
    )
    block_tm = models.BooleanField(
        default=False,
        verbose_name=_("cannot start master's thesis"),
    )
    block_defense = models.BooleanField(
        default=False,
        verbose_name=_("cannot defend master's thesis"),
    )
    probable_resolution_date = models.DateField(
        verbose_name=_("probable resolution date"),
        blank=True,
        null=True,
        help_text=_("A message will be sent on this date to reassess the situation."),
    )
    created_by = models.ForeignKey(
        User,
        related_name="created_exceptions",
        on_delete=models.SET_NULL,
        verbose_name=_("user"),
        blank=True,
        null=True,
    )
    creation_date = models.DateTimeField(auto_now_add=True)
    cleared = models.BooleanField(default=False, verbose_name=_("exception is lifted"))
    cleared_by = models.ForeignKey(
        User,
        related_name="cleared_exceptions",
        on_delete=models.SET_NULL,
        verbose_name=_("user"),
        blank=True,
        null=True,
    )
    cleared_date = models.DateTimeField(
        verbose_name=_("cleared date"),
        blank=True,
        null=True,
    )

    objects = models.Manager()
    current = CurrentExceptionManager()

    class Meta:
        verbose_name = _("student exception")
        verbose_name_plural = _("students exceptions")
        ordering = ["creation_date"]
        permissions = [
            ("can_view_all_studentsexceptions", _("Can view all students exceptions")),
            ("can_clear_studentsexceptions", _("Can clear students exceptions")),
        ]

    __original_cleared = None

    def __str__(self):
        return f"{self.reason}"

    def save(self, *args, **kwargs):
        # We notify the student and the supervisor when a new exception is created
        if self._state.adding:
            transaction.on_commit(lambda: exception_notification.delay(self.id))
        # We notify the user if the exception is lifted
        elif self.__original_cleared != self.cleared and self.cleared:
            transaction.on_commit(lambda: exception_cleared_notification.delay(self.id))
        super().save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse("tm:exception", kwargs={"pk": self.id})

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__original_cleared = self.cleared

    def __repr__(self):
        return f"<StudentException {self.id} : {self.attempt} ({self.reason})>"


class DefenseManager(models.Manager):
    def get_queryset(self):
        return (
            super()
            .get_queryset()
            .select_related(
                "master_thesis__attempt__student",
                "master_thesis__attempt__session",
            )
        )


class Defense(models.Model):
    PROVENCE = "Provence"
    OTHER = "Other"
    PLACES = [
        (PROVENCE, _("HES-SO Master, Avenue de Provence 6, 1007 Lausanne")),
        (OTHER, _("Other location (please specify)")),
    ]

    master_thesis = models.ForeignKey(
        "MasterThesis",
        related_name="defenses",
        on_delete=models.CASCADE,
        verbose_name=_("thesis"),
    )

    defense_date = models.DateTimeField(
        verbose_name=_("defense date"),
        blank=True,
        null=True,
        help_text=_("The defense date can only be set during the defense period."),
    )
    place = models.CharField(
        verbose_name=_("defense place"),
        max_length=100,
        blank=True,
        null=True,
        choices=PLACES,
        help_text=_(
            "If you select 'Other location', please specify the location in "
            "the field below.",
        ),
    )
    other_location = models.CharField(
        verbose_name=_("other location"),
        max_length=250,
        blank=True,
        null=True,
        help_text=_(
            "Enter the address, including level and room number, if applicable.",
        ),
    )
    room = models.CharField(
        verbose_name=_("defense room"),
        max_length=50,
        blank=True,
        null=True,
        help_text=_(
            "Rooms are reserved by the administration. All changes must be "
            "communicated to the administration.",
        ),
    )
    confirmed = models.BooleanField(default=False, verbose_name=_("defense confirmed"))
    confirmation_date = models.DateField(
        verbose_name=_("confirmation date"),
        blank=True,
        null=True,
    )

    statement_of_fees_file = models.FileField(
        upload_to=defense_pdf_upload_path,
        verbose_name=_("statement of fees file"),
        null=True,
        blank=True,
        help_text=_(
            "Complete and upload the statement of fees form before the defense. Department signature will be added later.",  # noqa: E501
        ),
    )

    assessment_file = models.FileField(
        upload_to=defense_pdf_upload_path,
        verbose_name=_("thesis assessment file"),
        null=True,
        blank=True,
        help_text=_(
            "Complete and upload the evaluation form after the defense. Department "
            "signature will be added later.",
        ),
    )
    assessment_submitted = models.BooleanField(
        default=False,
        verbose_name=_("assessment submitted"),
    )
    assessment_submission_date = models.DateTimeField(
        verbose_name=_("assessment submission date"),
        blank=True,
        null=True,
    )
    remediation_confirmed = models.DateTimeField(
        verbose_name=_("remdiation objectives confirmation"),
        blank=True,
        null=True,
    )

    result = models.IntegerField(
        verbose_name=_("result"),
        choices=MasterThesis.ASSESSMENTS,
        default=MasterThesis.NOT_ASSESSED,
    )
    grade = models.DecimalField(
        verbose_name=_("grade"),
        max_digits=2,
        decimal_places=1,
        blank=True,
        null=True,
    )
    remediation_objectives = QuillField(
        verbose_name=_("remediation objectives"),
        blank=True,
        null=True,
    )

    invitation_sent = models.DateTimeField(
        verbose_name=_("invitation sent"),
        blank=True,
        null=True,
    )

    creation_date = models.DateTimeField(auto_now_add=True)

    objects = DefenseManager()

    __original_result = None
    __original_place = None
    __original_place_display = None
    __original_room = None
    __original_defense_date = None
    __original_grade = None
    __original_assessment_submitted = None

    class Meta:
        verbose_name = _("thesis defense")
        verbose_name_plural = _("thesis defenses")
        ordering = ["defense_date"]
        permissions = [
            (
                "can_override_defense_session_dates",
                _("Can override the defense session dates"),
            ),
            (
                "can_cancel_defense",
                _("Can cancel a defense"),
            ),
        ]

    def __str__(self):
        return f"{self.defense_date} {self.master_thesis.attempt.student} ({self.master_thesis.attempt.session})"  # noqa: E501

    def save(self, *args, **kwargs):  # noqa: C901, PLR0912
        if self._state.adding:
            # We notify everybody that a defense has been created
            transaction.on_commit(lambda: send_defense_schedule.delay(self.id))
        elif (
            self.__original_defense_date != self.defense_date
            or self.__original_place != self.place
            or self.__original_room != self.room
        ):
            # We notify everybody that the defense has been updated
            transaction.on_commit(
                lambda: send_defense_schedule.delay(self.id, update=True),
            )

        # We need to book / cancel the room if the defense takes place at HES-SO Master
        if (
            self.__original_place == self.PROVENCE
            and self.__original_place != self.place
            and self.__original_room
        ):
            # We cancel the room booking
            self.room = None
            transaction.on_commit(
                lambda: send_room_booking.delay(
                    self.id,
                    self.__original_defense_date,
                    self.__original_place_display,
                    self.__original_room,
                ),
            )
        elif self.place == self.PROVENCE and self.__original_place != self.place:
            # We book a room
            transaction.on_commit(lambda: send_room_booking.delay(self.id))
        elif (
            self.__original_defense_date != self.defense_date
            and self.place == self.PROVENCE
            and self.__original_room
        ):
            self.room = None
            transaction.on_commit(
                lambda: send_room_booking.delay(
                    self.id,
                    self.__original_defense_date,
                    self.__original_place_display,
                    self.__original_room,
                ),
            )
            transaction.on_commit(lambda: send_room_booking.delay(self.id))

        if self.grade != self.__original_grade:
            if self.grade:
                if self.grade < 3.5:  # noqa: PLR2004
                    self.result = MasterThesis.FAIL
                elif self.grade < 4:  # noqa: PLR2004
                    self.result = MasterThesis.REMEDIATION
                else:
                    self.result = MasterThesis.PASS
                transaction.on_commit(lambda: self.master_thesis.save())

        if self.result != self.__original_result:
            self.master_thesis.result = self.result
            self.master_thesis.save()

        if self.__original_assessment_submitted != self.assessment_submitted:
            if self.assessment_submitted:
                transaction.on_commit(
                    lambda: send_assessment_confirmation.delay(self.id),
                )
        super().save(*args, **kwargs)

    def __repr__(self):
        return f"<Defense {self.id} : {self.defense_date} {self.master_thesis.attempt.student} ({self.master_thesis.attempt.session})>"  # noqa: E501

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__original_result = self.result
        self.__original_place = self.place
        self.__original_place_display = self.get_place_display()
        self.__original_room = self.room
        self.__original_defense_date = self.defense_date
        self.__original_grade = self.grade
        self.__original_assessment_submitted = self.assessment_submitted

    @property
    def defense_date_end(self):
        return self.defense_date + timedelta(minutes=60)

    @property
    def defense_date_over(self):
        return timezone.now() > self.defense_date + timedelta(minutes=60)

    @property
    def defense_remediation(self):
        return self.result == MasterThesis.REMEDIATION

    @property
    def remediation_objectives_deadline(self):
        return self.defense_date + timedelta(days=5)

    @cached_property
    def all_done(self):
        return bool(
            self.master_thesis.hard_copies_received
            and self.master_thesis.authorization_publish_file
            and self.statement_of_fees_file
            and self.assessment_file,
        )

    def get_result_class(self):
        return MasterThesis.RESULT_CLASSES[self.result]
