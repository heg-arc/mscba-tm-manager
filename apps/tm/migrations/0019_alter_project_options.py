# Generated by Django 3.2.13 on 2022-07-04 16:28

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("tm", "0018_project_feedbacks"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="project",
            options={
                "ordering": ["proposal", "supervisor", "-revision"],
                "permissions": [
                    ("can_view_unpublished_feedbacks", "Can view unpublished feedbacks")
                ],
                "verbose_name": "project",
                "verbose_name_plural": "projects",
            },
        ),
    ]
