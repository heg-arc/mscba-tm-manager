# Generated by Django 4.1.9 on 2023-08-15 09:09

import apps.tm.models
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("tm", "0042_masterthesis_expert_validation_and_more"),
    ]

    operations = [
        migrations.AddField(
            model_name="masterthesis",
            name="confidential_agreement_file",
            field=models.FileField(
                blank=True,
                help_text="If the thesis is confidential, you need to upload the signed confidential agreement form.",
                null=True,
                upload_to=apps.tm.models.thesis_pdf_upload_path,
                verbose_name="confidential agreement form",
            ),
        ),
        migrations.AddField(
            model_name="masterthesis",
            name="hard_copies_received",
            field=models.BooleanField(
                default=False, verbose_name="hard copies received"
            ),
        ),
        migrations.AddField(
            model_name="masterthesis",
            name="hard_copies_received_date",
            field=models.DateTimeField(
                blank=True, null=True, verbose_name="hard copies received date"
            ),
        ),
        migrations.AddField(
            model_name="masterthesis",
            name="hard_copies_received_user",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                related_name="receptions",
                to=settings.AUTH_USER_MODEL,
                verbose_name="hard copies received secretary",
            ),
        ),
        migrations.AlterField(
            model_name="masterthesis",
            name="expert_validation",
            field=models.BooleanField(default=False, verbose_name="expert validated"),
        ),
    ]
