# Generated by Django 4.1.9 on 2023-09-03 14:54

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("tm", "0051_defense_invitation_sent"),
    ]

    operations = [
        migrations.AddField(
            model_name="calendar",
            name="defense_rework_end",
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name="calendar",
            name="defense_rework_start",
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name="calendar",
            name="thesis_rework_end",
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name="calendar",
            name="thesis_rework_start",
            field=models.DateField(blank=True, null=True),
        ),
    ]
