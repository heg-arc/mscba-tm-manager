# Generated by Django 4.1.9 on 2023-10-01 10:28

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("tm", "0065_alter_masterthesis_submission_date"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="session",
            options={
                "ordering": ["start_date"],
                "permissions": [
                    ("can_export_documents", "Can export documents"),
                    ("can_import_documents", "Can import documents"),
                ],
                "verbose_name": "session",
                "verbose_name_plural": "sessions",
            },
        ),
    ]
