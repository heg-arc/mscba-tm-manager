# Generated by Django 4.1.9 on 2023-05-26 15:59

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("tm", "0036_alter_progressreport_validation_date"),
    ]

    operations = [
        migrations.AddField(
            model_name="progressreport",
            name="forced_submission",
            field=models.BooleanField(default=False, verbose_name="forced submission"),
        ),
        migrations.AddField(
            model_name="progressreport",
            name="forced_submission_user",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                related_name="forced_reports",
                to=settings.AUTH_USER_MODEL,
                verbose_name="forced by",
            ),
        ),
    ]
