# Generated by Django 4.1.3 on 2023-01-31 17:11

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("tm", "0025_time_on_deadlines"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="project",
            options={
                "ordering": ["proposal", "supervisor", "-revision"],
                "permissions": [
                    (
                        "can_view_unpublished_feedbacks",
                        "Can view unpublished feedbacks",
                    ),
                    ("can_view_all_projects", "Can view all projects"),
                    ("can_grade_projects", "Can grade projects for majors"),
                ],
                "verbose_name": "project",
                "verbose_name_plural": "projects",
            },
        ),
    ]
