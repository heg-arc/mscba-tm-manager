# Generated by Django 4.1.9 on 2023-09-21 09:56

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("tm", "0062_masterthesis_authorization_publish_and_more"),
    ]

    operations = [
        migrations.AlterField(
            model_name="masterthesis",
            name="authorization_publish",
            field=models.BooleanField(
                choices=[(True, "approved"), (False, "refused"), (None, "undefined")],
                default=None,
                verbose_name="authorization to publish",
            ),
        ),
    ]
