# Generated by Django 4.2.15 on 2024-08-28 07:54

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ("tm", "0075_alter_masterthesis_co_supervisors_and_more"),
    ]

    operations = [
        migrations.AlterField(
            model_name="session",
            name="calendar",
            field=models.ForeignKey(
                default=1,
                on_delete=django.db.models.deletion.PROTECT,
                related_name="session",
                to="tm.calendar",
                verbose_name="calendar",
            ),
            preserve_default=False,
        ),
    ]
