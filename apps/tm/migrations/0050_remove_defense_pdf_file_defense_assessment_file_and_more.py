# Generated by Django 4.1.9 on 2023-08-27 07:27

import apps.tm.models
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("tm", "0049_masterthesis_authorization_publish_file"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="defense",
            name="pdf_file",
        ),
        migrations.AddField(
            model_name="defense",
            name="assessment_file",
            field=models.FileField(
                blank=True,
                null=True,
                upload_to=apps.tm.models.defense_pdf_upload_path,
                verbose_name="thesis assessment file",
            ),
        ),
        migrations.AddField(
            model_name="defense",
            name="statement_of_fees_file",
            field=models.FileField(
                blank=True,
                null=True,
                upload_to=apps.tm.models.defense_pdf_upload_path,
                verbose_name="statement of fees file",
            ),
        ),
    ]
