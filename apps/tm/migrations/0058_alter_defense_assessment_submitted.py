# Generated by Django 4.1.9 on 2023-09-03 19:27

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("tm", "0057_rename_submitted_defense_assessment_submitted_and_more"),
    ]

    operations = [
        migrations.AlterField(
            model_name="defense",
            name="assessment_submitted",
            field=models.BooleanField(
                default=False, verbose_name="assessment submitted"
            ),
        ),
    ]
