import json
import logging
import zipfile
from io import BytesIO

import requests
import xlsxwriter
from dal import autocomplete
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.auth.mixins import UserPassesTestMixin
from django.contrib.auth.models import Group
from django.contrib.messages.views import SuccessMessageMixin
from django.core.exceptions import PermissionDenied
from django.core.files.storage import default_storage
from django.db import transaction
from django.db.models import OuterRef
from django.db.models import Q
from django.db.models import Subquery
from django.http import FileResponse
from django.http import Http404
from django.http import HttpResponse
from django.http import HttpResponseForbidden
from django.http import HttpResponseRedirect
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.shortcuts import redirect
from django.shortcuts import render
from django.urls import reverse
from django.urls import reverse_lazy
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from django.views.generic import CreateView
from django.views.generic import DeleteView
from django.views.generic import DetailView
from django.views.generic import ListView
from django.views.generic import TemplateView
from django.views.generic import UpdateView
from django.views.generic.edit import FormView
from django_filters.views import FilterView
from pdf2image import convert_from_bytes

from apps.users.models import User

from . import forms
from .directory import query_user_by_id
from .directory import query_user_by_name
from .filters import AttemptsFilter
from .models import Assignment
from .models import Attempt
from .models import Calendar
from .models import Defense
from .models import Major
from .models import MasterThesis
from .models import PersonalLeave
from .models import ProgressReport
from .models import Project
from .models import Proposal
from .models import ProposalWorkflow
from .models import Session
from .models import StudentException
from .models import get_current_calendar
from .models import get_current_session
from .models import get_gantt_from_calendar
from .pdf import fill_confidentiality_agreement
from .pdf import fill_publishing_authorization
from .pdf import fill_statement_of_fees
from .pdf import fill_thesis_assessment
from .pdf import split_and_save_pdf_scans
from .tasks import anonymize_data
from .tasks import create_zip_assessment
from .tasks import create_zip_authorization_publish
from .tasks import create_zip_statement_fees
from .tasks import create_zip_thesis
from .tasks import delete_personal_leave
from .tasks import import_students_by_isa

logger = logging.getLogger(__name__)


def current_session(request):
    tm_session = False
    tm_session_id = request.session.get("tm_session_id", False)
    if tm_session_id:
        try:
            tm_session = Session.objects.get(pk=tm_session_id)
        except Session.DoesNotExist:
            msg = (
                f"Session DoesNotExist for {tm_session_id} requested by {request.user}",
            )
            logger.warning(
                msg,
            )

    if not tm_session:
        # We set the session to the current attempt of the user
        if not request.user.is_anonymous:
            if request.user.is_student:
                if request.user.attempt:
                    tm_session = request.user.attempt.session
                else:
                    tm_session = get_current_session()
            else:
                tm_session = get_current_session()
            if tm_session:
                request.session["tm_session_id"] = tm_session.id

    return tm_session


def set_session(request, session_id):
    request.session["tm_session_id"] = session_id
    if request.headers.get("referer", None):
        response = HttpResponseRedirect(request.headers.get("referer"))
    else:
        response = HttpResponseRedirect(reverse("tm:dashboard"))
    return response


def set_major(request, major_id):
    request.session["tm_major_id"] = major_id
    if request.headers.get("referer", None):
        response = HttpResponseRedirect(request.headers.get("referer"))
    else:
        response = HttpResponseRedirect(reverse("tm:dashboard"))
    return response


class HomeView(TemplateView):
    template_name = "pages/home.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["next_url"] = self.request.GET.get("next", "")
        return context


class DashboardView(LoginRequiredMixin, TemplateView):
    template_name = "tm/dashboard.html"

    def get_context_data(self, **kwargs):  # noqa: C901
        context = super().get_context_data(**kwargs)
        session = current_session(self.request)
        attempt = self.request.user.attempt
        calendar = get_current_calendar(session, attempt=attempt)
        context["calendar"] = calendar
        context["attempt"] = attempt
        gantt_data = get_gantt_from_calendar(calendar)
        if gantt_data:
            context["gantt_data"] = gantt_data
        if self.request.user.is_student:
            # We can have students without attempt (visitors for example)
            if attempt:
                # We only display accepted proposals assigned to a supervisor
                proposals = (
                    Proposal.objects.filter(attempt=self.request.user.attempt)
                    .filter(accepted=True)
                    .filter(assignments__accepted=True)
                )
                if proposals.count() == 1:
                    context["project_proposal"] = proposals.first()
                elif proposals.count() > 1:
                    context["project_form"] = forms.CreateProposalFromProject()
                    context["project_form"].fields["proposal"].queryset = proposals

                try:
                    context["disposition"] = (
                        Project.objects.filter(attempt=self.request.user.attempt)
                        .order_by("-revision")[0:1]
                        .get()
                    )
                except Project.DoesNotExist:
                    context["disposition"] = None
                context["submit_disposition"] = False
                if context["disposition"]:
                    if context["disposition"].status == Project.FAIL:
                        context["submit_disposition"] = True
                else:
                    context["submit_disposition"] = True
                context["exceptions"] = StudentException.current.filter(
                    attempt=self.request.user.attempt,
                )

        if self.request.user.is_expert:
            if (
                MasterThesis.objects.filter(expert=self.request.user)
                .filter(expert_validation=True)
                .exists()
            ):
                context["theses"] = MasterThesis.objects.filter(
                    expert=self.request.user,
                ).filter(expert_validation=True)
        return context


class OverviewView(LoginRequiredMixin, UserPassesTestMixin, FilterView):
    template_name = "tm/overview.html"
    filterset_class = AttemptsFilter
    context_object_name = "attempts"
    model = Attempt

    def test_func(self):
        if self.request.user.is_mh or self.request.user.is_supervisor:
            return True
        return bool(self.request.user.has_perm("tm.can_view_all_attempts"))

    def get_queryset(self):
        session = current_session(self.request)

        if self.request.user.has_perm("tm.can_view_all_attempts"):
            qs = (
                super()
                .get_queryset()
                .filter(session=session, archived=False)
                .filter(major__in=self.request.user.majors.all())
                .select_related("student", "session", "major", "calendar")
                .prefetch_related("projects", "theses", "proposals")
                .order_by("student__name")
            )

        elif session:
            if not session.calendar.proposal_rework_due:
                # We list all attempts where the supervisor is assigned to proposal
                attempt_ids = (
                    Assignment.objects.filter(
                        supervisor=self.request.user,
                        validated=True,
                    )
                    .exclude(accepted=False)
                    .exclude(archive_date__isnull=False)
                    .values_list("proposal__attempt_id", flat=True)
                )
            else:
                # We only list attempts where the supervisor is assigned to the project
                attempt_ids = Project.objects.filter(
                    supervisor=self.request.user,
                ).values_list("attempt_id", flat=True)
            qs = (
                super()
                .get_queryset()
                .filter(session=session, archived=False)
                .filter(id__in=attempt_ids)
                .select_related("student", "session", "major")
                .order_by("student__name")
            )
        else:
            return Attempt.objects.none()
        return AttemptsFilter(self.request.GET, queryset=qs).qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        supervisors = []
        students_ids = self.get_queryset().values_list("student_id", flat=True)
        students = User.objects.filter(id__in=students_ids).values_list(
            "email",
            flat=True,
        )
        for attempt in self.get_queryset():
            if attempt.supervisor:
                if attempt.supervisor.email not in supervisors:
                    supervisors.append(attempt.supervisor.email)
            if attempt.co_supervisors:
                for co_supervisor in attempt.co_supervisors.all():
                    if co_supervisor.email not in supervisors:
                        supervisors.append(co_supervisor.email)

        students_email = ";".join(students)
        supervisors_email = ";".join(supervisors)
        context["students_email"] = students_email
        context["supervisors_email"] = supervisors_email
        return context


class ProposalCreateView(LoginRequiredMixin, UserPassesTestMixin, CreateView):
    model = Proposal
    form_class = forms.ProposalForm
    action = _("create")

    def test_func(self):
        return bool(self.request.user.is_student)

    def form_valid(self, form):
        form.instance.attempt = self.request.user.attempt
        messages.add_message(
            self.request,
            messages.SUCCESS,
            _(
                "New pre-proposal saved. You can edit your pre-proposal and submit it "
                "for review when you are done.",
            ),
        )
        response = super().form_valid(form)
        comments = {"delta": "", "html": str(_("New pre-proposal created"))}
        workflow = ProposalWorkflow(
            proposal=self.object,
            user=self.request.user,
            role=ProposalWorkflow.STUDENT,
            comments=json.dumps(comments),
        )
        workflow.save()
        return response


class ProposalUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Proposal
    form_class = forms.ProposalForm
    action = _("update")

    def test_func(self):
        return self.request.user == self.get_object().attempt.student

    def form_valid(self, form):
        messages.add_message(
            self.request,
            messages.SUCCESS,
            _(
                "Pre-proposal saved. You can continue to edit your pre-proposal and submit it for review when you are done.",  # noqa: E501
            ),
        )
        comments = {"delta": "", "html": str(_("Pre-proposal updated"))}
        response = super().form_valid(form)
        if self.object.current_status.status != ProposalWorkflow.DRAFT:
            workflow = ProposalWorkflow(
                proposal=self.object,
                user=self.request.user,
                role=ProposalWorkflow.STUDENT,
                comments=json.dumps(comments),
            )
            workflow.save()
        return response


class ProposalDetailView(LoginRequiredMixin, UserPassesTestMixin, DetailView):
    model = Proposal

    def test_func(self):
        proposal = self.get_object()
        if self.request.user == proposal.attempt.student:
            return True
        if proposal.supervisors:
            if self.request.user in proposal.supervisors:
                return True
        if (
            self.request.user.is_mh
            and proposal.attempt.major in self.request.user.majors.all()
        ):
            return True
        return bool(self.request.user.has_perm("tm.can_view_all_proposals"))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        proposal = self.get_object()
        if self.request.user == proposal.attempt.student:
            context["submission_form"] = forms.StudentWorkflowForm(
                instance=proposal,
                prefix="submission",
            )
            context["withdrawal_form"] = forms.StudentWorkflowForm(
                instance=proposal,
                prefix="withdrawal",
            )
        if not proposal.supervisor:
            context["assignment_form"] = forms.SupervisorSuggestionForm()
            context["assignment_form"].fields["supervisor"].queryset = (
                User.objects.filter(is_supervisor=True)
                .exclude(id__in=proposal.supervisors)
                .order_by("name")
            )
            if self.request.user in proposal.supervisors:
                # We display the assignment acceptation form
                assignment = proposal.assignments.filter(
                    supervisor=self.request.user,
                    validated=True,
                    accepted__isnull=True,
                ).first()
                if assignment:
                    context["acceptation_form"] = forms.SupervisorAcceptationForm()
                    context["assignment_id"] = assignment.id
        return context


class ProposalListView(LoginRequiredMixin, ListView):
    model = Proposal

    def get_queryset(self):
        session = current_session(self.request)
        if self.request.user.has_perm("tm.can_view_all_proposals"):
            return (
                super()
                .get_queryset()
                .filter(attempt__session=session)
                .filter(attempt__major__in=self.request.user.majors.all())
                .exclude(workflow__status=ProposalWorkflow.WITHDRAWN)
                .select_related(
                    "attempt__student",
                    "attempt__session",
                    "attempt__major",
                )
            )
        if self.request.user.is_supervisor:
            proposals_id = (
                Assignment.objects.filter(supervisor=self.request.user, validated=True)
                .filter(Q(accepted=True) | Q(accepted__isnull=True))
                .exclude(archive_date__isnull=False)
                .values_list("proposal_id", flat=True)
            )
            return (
                super()
                .get_queryset()
                .filter(attempt__session=session, id__in=proposals_id)
                .select_related(
                    "attempt__student",
                    "attempt__session",
                    "attempt__major",
                )
            )
        return super().get_queryset().none()


class ProposalExportView(LoginRequiredMixin, ListView):
    model = Proposal
    template_name = "tm/proposal_export.html"

    def get_queryset(self):
        session = current_session(self.request)
        if self.request.user.has_perm("tm.can_view_all_proposals"):
            return (
                super()
                .get_queryset()
                .filter(attempt__session=session)
                .filter(attempt__major__in=self.request.user.majors.all())
                .exclude(workflow__status=ProposalWorkflow.WITHDRAWN)
                .select_related(
                    "attempt__student",
                    "attempt__session",
                    "attempt__major",
                )
            )
        if self.request.user.is_supervisor:
            proposals_id = (
                Assignment.objects.filter(supervisor=self.request.user, validated=True)
                .filter(Q(accepted=True) | Q(accepted__isnull=True))
                .exclude(archive_date__isnull=False)
                .values_list("proposal_id", flat=True)
            )
            return (
                super()
                .get_queryset()
                .filter(attempt__session=session, id__in=proposals_id)
                .select_related(
                    "attempt__student",
                    "attempt__session",
                    "attempt__major",
                )
            )
        return super().get_queryset().none()


class SubmitProposal(LoginRequiredMixin, UserPassesTestMixin, CreateView):
    model = ProposalWorkflow
    form_class = forms.StudentWorkflowForm

    def test_func(self):
        proposal = Proposal.objects.get(pk=self.kwargs["pk"])
        return self.request.user == proposal.attempt.student

    def form_valid(self, form):
        submission_form = forms.StudentWorkflowForm(
            self.request.POST,
            prefix="submission",
        )
        if submission_form.is_valid():
            submission_form.instance.user = self.request.user
            submission_form.instance.role = ProposalWorkflow.STUDENT
            submission_form.instance.status = ProposalWorkflow.SUBMITTED
            submission_form.instance.proposal_id = self.kwargs["pk"]
            messages.add_message(
                self.request,
                messages.SUCCESS,
                _(
                    "Proposal submitted. You receive a notification as soon as your "
                    "proposal has been reviewed.",
                ),
            )
            return super().form_valid(submission_form)
        return self.form_invalid(form)

    def get_success_url(self):
        return reverse("tm:dashboard")


class WithdrawProposal(LoginRequiredMixin, UserPassesTestMixin, CreateView):
    model = ProposalWorkflow
    form_class = forms.StudentWorkflowForm

    def test_func(self):
        proposal = Proposal.objects.get(pk=self.kwargs["pk"])
        return self.request.user == proposal.attempt.student

    def form_valid(self, form):
        withdrawal_form = forms.StudentWorkflowForm(
            self.request.POST,
            prefix="withdrawal",
        )
        if withdrawal_form.is_valid():
            withdrawal_form.instance.user = self.request.user
            withdrawal_form.instance.role = ProposalWorkflow.STUDENT
            withdrawal_form.instance.status = ProposalWorkflow.WITHDRAWN
            withdrawal_form.instance.proposal_id = self.kwargs["pk"]
            messages.add_message(
                self.request,
                messages.SUCCESS,
                _("Proposal withdrawn. You can submit a new proposal at any time."),
            )
            response = super().form_valid(withdrawal_form)
            # Cancel all assignments
            assignments = self.object.proposal.assignments.all()
            for assignment in assignments:
                assignment.archive_date = timezone.now()
                assignment.save()
            return response
        return self.form_invalid(form)

    def get_success_url(self):
        return reverse("tm:dashboard")


class DeleteStepFromProposal(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = ProposalWorkflow

    def test_func(self):
        return self.request.user == self.get_object().user

    def get_success_url(self):
        return reverse("tm:proposal", kwargs={"pk": self.object.proposal_id})


class AddReviewProposal(LoginRequiredMixin, UserPassesTestMixin, CreateView):
    model = ProposalWorkflow
    form_class = forms.WorkflowForm

    def test_func(self):
        proposal = Proposal.objects.get(pk=self.kwargs["pk"])
        if self.request.user == proposal.attempt.student:
            return True
        if proposal.supervisor:
            if self.request.user == proposal.supervisor:
                return True
        if proposal.supervisors:
            if self.request.user in proposal.supervisors:
                return True
        return bool(
            self.request.user.is_mh
            and proposal.attempt.major in self.request.user.majors.all(),
        )

    def form_valid(self, form):
        form.instance.user = self.request.user
        if self.request.user.is_mh:
            form.instance.role = ProposalWorkflow.MH
        elif self.request.user == self.get_object().proposal.attempt.student:
            form.instance.role = ProposalWorkflow.STUDENT
            form.instance.status = (
                ProposalWorkflow.COMMENT
            )  # Students can only add comments
        else:
            form.instance.role = ProposalWorkflow.SUPERVISOR
            form.instance.status = (
                ProposalWorkflow.COMMENT
            )  # Supervisors can only add comments
        form.instance.proposal_id = self.kwargs["pk"]
        messages.add_message(
            self.request,
            messages.SUCCESS,
            _("The new revision was saved."),
        )
        return super().form_valid(form)

    def get_success_url(self):
        return reverse("tm:confirm")


class SupervisorSuggestion(LoginRequiredMixin, UserPassesTestMixin, CreateView):
    model = Assignment
    form_class = forms.SupervisorSuggestionForm

    def test_func(self):
        proposal = Proposal.objects.get(pk=self.kwargs["pk"])
        if self.request.user == proposal.attempt.student:
            return True
        return bool(
            self.request.user.is_mh
            and proposal.attempt.major in self.request.user.majors.all(),
        )

    def form_valid(self, form):
        form.instance.proposal_id = self.kwargs["pk"]
        messages.add_message(
            self.request,
            messages.SUCCESS,
            _("The suggestion has been recorded and will be reviewed shortly."),
        )
        return super().form_valid(form)

    def get_success_url(self):
        return reverse("tm:proposal", kwargs={"pk": self.kwargs["pk"]})


class AssignmentAcceptance(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Assignment
    form_class = forms.SupervisorAcceptationForm

    def test_func(self):
        return self.request.user in self.get_object().proposal.supervisors

    def form_valid(self, form):
        form.instance.response_date = timezone.now().date()
        messages.add_message(
            self.request,
            messages.SUCCESS,
            _("Your decision has been recorded. Thank you for your cooperation."),
        )
        response = super().form_valid(form)
        if self.object.comments != "":
            # We add a ProposalWorkflow with the comment
            workflow = ProposalWorkflow(
                proposal=self.object.proposal,
                user=self.request.user,
                status=ProposalWorkflow.COMMENT,
                role=ProposalWorkflow.SUPERVISOR,
                comments=self.object.comments,
            )
            workflow.save()
        return response

    def get_success_url(self):
        assignment = self.get_object()
        if not assignment.accepted:
            return reverse("tm:dashboard")
        return reverse("tm:proposal", kwargs={"pk": self.get_object().proposal.id})


class AssignmentsListView(LoginRequiredMixin, ListView):
    model = Assignment

    def get_queryset(self):
        session = current_session(self.request)
        if self.request.user.has_perm("tm.can_view_all_assignments"):
            return (
                super()
                .get_queryset()
                .filter(proposal__attempt__session=session)
                .exclude(proposal__workflow__status=ProposalWorkflow.WITHDRAWN)
                .exclude(proposal__workflow__status=ProposalWorkflow.REJECTED)
            )
        if self.request.user.is_mh:
            return (
                super()
                .get_queryset()
                .filter(proposal__attempt__session=session)
                .filter(proposal__attempt__major__in=self.request.user.majors.all())
                .exclude(proposal__workflow__status=ProposalWorkflow.WITHDRAWN)
                .exclude(proposal__workflow__status=ProposalWorkflow.REJECTED)
            )
        return super().get_queryset().none()


class AssignmentsToValidateListView(LoginRequiredMixin, ListView):
    model = Assignment

    def get_queryset(self):
        session = current_session(self.request)
        if self.request.user.has_perm("tm.can_view_all_assignments"):
            return (
                super()
                .get_queryset()
                .filter(proposal__attempt__session=session)
                .exclude(proposal__workflow__status=ProposalWorkflow.WITHDRAWN)
                .exclude(proposal__workflow__status=ProposalWorkflow.REJECTED)
                .exclude(validated__isnull=False)
                .exclude(archive_date__isnull=False)
            )
        if self.request.user.is_mh:
            return (
                super()
                .get_queryset()
                .filter(proposal__attempt__session=session)
                .filter(proposal__attempt__major__in=self.request.user.majors.all())
                .exclude(proposal__workflow__status=ProposalWorkflow.WITHDRAWN)
                .exclude(proposal__workflow__status=ProposalWorkflow.REJECTED)
                .exclude(validated__isnull=False)
                .exclude(archive_date__isnull=False)
            )
        return super().get_queryset().none()


class DispositionListView(LoginRequiredMixin, ListView):
    model = Project

    def get_queryset(self):
        session = current_session(self.request)
        last_revision_ids = (
            Project.objects.filter(
                attempt_id=OuterRef("attempt_id"),
                proposal_id=OuterRef("proposal_id"),
            )
            .order_by("-revision")
            .values("id")
        )
        if self.request.user.has_perm("tm.can_view_all_projects"):
            return (
                super()
                .get_queryset()
                .filter(attempt__session=session)
                .filter(id=Subquery(last_revision_ids[:1]))
                .select_related(
                    "attempt__student",
                    "attempt__calendar",
                    "attempt__major",
                    "supervisor",
                )
                .prefetch_related("co_supervisors")
            )
        if self.request.user.is_mh or self.request.user.has_perm(
            "tm.can_grade_projects",
        ):
            return (
                super()
                .get_queryset()
                .filter(attempt__session=session)
                .filter(id=Subquery(last_revision_ids[:1]))
                .filter(attempt__major__in=self.request.user.majors.all())
                .select_related(
                    "attempt__student",
                    "attempt__calendar",
                    "attempt__major",
                    "supervisor",
                )
                .prefetch_related("co_supervisors")
            )
        if self.request.user.is_supervisor:
            return (
                super()
                .get_queryset()
                .filter(attempt__session=session)
                .filter(id=Subquery(last_revision_ids[:1]))
                .filter(supervisor=self.request.user)
                .select_related(
                    "attempt__student",
                    "attempt__calendar",
                    "attempt__major",
                    "supervisor",
                )
                .prefetch_related("co_supervisors")
            )
        return super().get_queryset().none()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if (
            self.request.user.has_perm("tm.can_view_all_projects")
            or self.request.user.has_perm("tm.can_grade_projects")
            or self.request.user.is_mh
        ):
            context["privileged"] = True
        else:
            context["privileged"] = False
        return context


class DispositionCreateView(LoginRequiredMixin, UserPassesTestMixin, CreateView):
    model = Project
    form_class = forms.CreateProposalFromProject
    action = _("create")

    def test_func(self):
        return bool(self.request.user.is_student)

    def form_valid(self, form):
        form.instance.attempt = self.request.user.attempt
        form.instance.title = form.instance.proposal.title
        form.instance.supervisor = form.instance.proposal.supervisor
        messages.add_message(
            self.request,
            messages.SUCCESS,
            _(
                "New disposition saved. You can edit your disposition and submit it for"
                " review when you are done.",
            ),
        )
        return super().form_valid(form)


class DispositionDetailView(LoginRequiredMixin, UserPassesTestMixin, DetailView):
    model = Project

    def test_func(self):
        project = self.get_object()
        if self.request.user == project.attempt.student:
            return True
        if self.request.user == project.supervisor:
            return True
        if (
            self.request.user.is_mh
            or self.request.user.has_perm("tm.can_grade_projects")
        ) and project.attempt.major in self.request.user.majors.all():
            return True
        return bool(self.request.user.has_perm("tm.can_view_all_projects"))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        project = self.get_object()
        if self.request.user == project.attempt.student:
            context["submission_form"] = forms.SubmitProjectForm(instance=project)
        revision_history = Project.objects.filter(
            attempt=project.attempt,
            proposal=project.proposal,
        ).order_by("-revision")
        context["revision_history"] = revision_history
        context["can_add_supervisor"] = False
        if (
            self.request.user.is_mh
            and project.attempt.major in self.request.user.majors.all()
        ) or self.request.user.has_perm("tm.can_view_all_projects"):
            context["can_add_supervisor"] = True
            context["co_supervisor_form"] = forms.ProjectCoSupervisorForm(
                instance=project,
            )
        return context


class DispositionUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Project
    form_class = forms.UpdateProjectForm
    action = _("update")

    def test_func(self):
        return self.request.user == self.get_object().attempt.student

    def form_valid(self, form):
        messages.add_message(
            self.request,
            messages.SUCCESS,
            _(
                "Disposition saved. You can continue to edit your disposition and submit it for review when you are done.",  # noqa: E501
            ),
        )
        return super().form_valid(form)


class DispositionSubmitView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Project
    form_class = forms.SubmitProjectForm
    action = _("submit")

    def test_func(self):
        return self.request.user == self.get_object().attempt.student

    def form_valid(self, form):
        messages.add_message(
            self.request,
            messages.SUCCESS,
            _("Disposition submitted."),
        )
        form.instance.submitted = True
        form.instance.submission_date = timezone.now().date()
        return super().form_valid(form)


class DispositionEvaluateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Project
    form_class = forms.EvaluateProjectForm
    action = _("submit")

    def test_func(self):
        return bool(self.request.user.has_perm("tm.can_grade_projects"))

    def form_valid(self, form):
        messages.add_message(self.request, messages.SUCCESS, _("Evaluation submitted."))
        form.instance.reviewer_content = self.request.user
        return super().form_valid(form)


class DispositionEvaluateMethodologyView(
    LoginRequiredMixin,
    UserPassesTestMixin,
    UpdateView,
):
    model = Project
    form_class = forms.EvaluateProjectMethodologyForm
    action = _("submit")

    def test_func(self):
        return bool(self.request.user.has_perm("tm.can_grade_projects"))

    def form_valid(self, form):
        messages.add_message(self.request, messages.SUCCESS, _("Evaluation submitted."))
        if form.instance.grade_methodology:
            if form.instance.grade_methodology >= 4.0:  # noqa: PLR2004
                form.instance.pass_methodology = True
            else:
                form.instance.pass_methodology = False
        form.instance.reviewer_methodology = self.request.user
        return super().form_valid(form)


def download_project_zip(request):
    session = current_session(request)
    last_revision_ids = (
        Project.objects.filter(
            attempt_id=OuterRef("attempt_id"),
            proposal_id=OuterRef("proposal_id"),
        )
        .order_by("-revision")
        .values("id")
    )
    if request.user.has_perm("tm.can_view_all_projects"):
        projects = Project.objects.filter(attempt__session=session).filter(
            id=Subquery(last_revision_ids[:1]),
        )
    elif request.user.is_mh or request.user.has_perm("tm.can_grade_projects"):
        projects = (
            Project.objects.filter(attempt__session=session)
            .filter(id=Subquery(last_revision_ids[:1]))
            .filter(attempt__major__in=request.user.majors.all())
        )
    elif request.user.is_supervisor:
        projects = (
            Project.objects.filter(attempt__session=session)
            .filter(id=Subquery(last_revision_ids[:1]))
            .filter(supervisor=request.user)
        )
    else:
        projects = Project.objects.none()

    zip_file = BytesIO()
    with zipfile.ZipFile(zip_file, "w") as zf:
        for project in projects:
            if project.pdf_file:
                file_name = f"MScBA_{project.attempt.major.code}_{project.attempt.session.name}_disposition_{project.attempt.student}.pdf"  # noqa: E501
                zf.writestr(file_name, project.pdf_file.file.read())
    zip_file.seek(0)
    response = FileResponse(zip_file, content_type="application/zip")
    response["Content-Disposition"] = (
        'attachment; filename="MScBA_project_proposals.zip"'
    )
    return response


def excel_projects(request):
    session = current_session(request)
    last_revision_ids = (
        Project.objects.filter(
            attempt_id=OuterRef("attempt_id"),
            proposal_id=OuterRef("proposal_id"),
        )
        .order_by("-revision")
        .values("id")
    )
    if request.user.has_perm("tm.can_view_all_projects"):
        projects = Project.objects.filter(attempt__session=session).filter(
            id=Subquery(last_revision_ids[:1]),
        )
    elif request.user.is_mh or request.user.has_perm("tm.can_grade_projects"):
        projects = (
            Project.objects.filter(attempt__session=session)
            .filter(id=Subquery(last_revision_ids[:1]))
            .filter(attempt__major__in=request.user.majors.all())
        )
    else:
        projects = Project.objects.none()
    xlsx_file = BytesIO()

    workbook = xlsxwriter.Workbook(xlsx_file)
    bold = workbook.add_format({"bold": True, "bottom": 1})
    title = workbook.add_format({"bold": True, "font_size": 14})
    border = workbook.add_format({"bottom": 1})
    worksheet = workbook.add_worksheet("MScBA TM")
    worksheet.write("A1", "MScBA - Master Thesis Dispositions")
    worksheet.write("A3", f"Dispositions - Session {session.name}", title)
    worksheet.write("A6", "Student", bold)
    worksheet.write("B6", "Major", bold)
    worksheet.write("C6", "Retake", bold)
    worksheet.write("D6", "Supervisor", bold)
    worksheet.write("E6", "Title", bold)
    worksheet.write("F6", "Pass Content", bold)
    worksheet.write("G6", "Pass Methodology", bold)
    worksheet.write("H6", "Grade", bold)
    worksheet.write("I6", "Accepted", bold)
    row = 6
    col = 0
    for project in projects:
        worksheet.write(row, col, project.attempt.student.name, border)
        worksheet.write(row, col + 1, project.attempt.major.code, border)
        worksheet.write(
            row,
            col + 2,
            project.attempt.retake if project.attempt.retake else "",
            border,
        )
        worksheet.write(row, col + 3, project.supervisor.name, border)
        worksheet.write(row, col + 4, project.title, border)
        worksheet.write(row, col + 5, project.content_status, border)
        worksheet.write(row, col + 6, project.methodology_status, border)
        worksheet.write(row, col + 7, project.grade_methodology, border)
        worksheet.write(row, col + 8, project.status, border)
        row += 1
    worksheet.autofit()
    worksheet.set_column(4, 4, 80)
    workbook.close()
    xlsx_file.seek(0)
    response = FileResponse(xlsx_file, content_type="application/vnd.ms-excel")
    response["Content-Disposition"] = (
        'attachment; filename="MScBA_project_proposals.xlsx"'
    )
    return response


def publish_evaluations(request):
    session = current_session(request)
    last_revision_ids = (
        Project.objects.filter(
            attempt_id=OuterRef("attempt_id"),
            proposal_id=OuterRef("proposal_id"),
        )
        .order_by("-revision")
        .values("id")
    )
    if request.user.has_perm("tm.can_view_all_projects"):
        projects = Project.objects.filter(attempt__session=session).filter(
            id=Subquery(last_revision_ids[:1]),
        )
    elif request.user.is_mh:
        projects = (
            Project.objects.filter(attempt__session=session)
            .filter(id=Subquery(last_revision_ids[:1]))
            .filter(attempt__major__in=request.user.majors.all())
        )
    else:
        projects = Project.objects.none()
    if projects:
        count = 0
        for project in projects:
            if project.accepted is not None:
                project.publish_feedbacks = True
                project.save()
                count += 1
        messages.add_message(
            request,
            messages.SUCCESS,
            _(f"{count} assessments were published."),  # noqa: INT001
        )
        return redirect("tm:dispositions")
    raise PermissionDenied


def publish_evaluation(request, pk):
    project = get_object_or_404(Project, pk=pk)
    if request.user.is_mh:
        if project.attempt.major in request.user.majors.all():
            project.publish_feedbacks = True
            project.save()
            messages.add_message(
                request,
                messages.SUCCESS,
                _("The assessment is published and the student notified."),
            )
            return redirect("tm:dispositions")
    raise PermissionDenied


class LibraryListView(LoginRequiredMixin, ListView):
    model = MasterThesis
    template_name = "tm/library_list.html"

    def get_queryset(self):
        if self.request.user.has_perm("tm.can_view_confidential_theses"):
            return (
                super()
                .get_queryset()
                .select_related(
                    "attempt",
                    "supervisor",
                    "attempt__student",
                    "attempt__session",
                    "attempt__major",
                )
                .filter(submitted=True)
            )
        return (
            super()
            .get_queryset()
            .select_related(
                "attempt",
                "supervisor",
                "attempt__student",
                "attempt__session",
                "attempt__major",
            )
            .filter(submitted=True, confidential=False, result=MasterThesis.PASS)
        )


class LibraryDetailView(LoginRequiredMixin, UserPassesTestMixin, DetailView):
    model = MasterThesis
    template_name = "tm/library_detail.html"

    def test_func(self):
        thesis = self.get_object()
        if thesis.confidential:
            return bool(self.request.user.has_perm("tm.can_view_confidential_theses"))
        # TODO: Filter current session?
        return True

    def get_queryset(self):
        return (
            super()
            .get_queryset()
            .select_related(
                "attempt",
                "supervisor",
                "attempt__student",
                "attempt__session",
                "attempt__major",
            )
        )

    def get_context_data(self, **kwargs):
        return super().get_context_data(**kwargs)


class CalendarDetailView(DetailView):
    model = Calendar

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        calendar = self.get_object()
        context["gantt_data"] = get_gantt_from_calendar(calendar)
        return context


class AttemptDetailView(LoginRequiredMixin, UserPassesTestMixin, DetailView):
    model = Attempt

    def test_func(self):
        attempt = self.get_object()
        if self.request.user == attempt.student:
            return True
        if attempt.supervisor:
            if self.request.user == attempt.supervisor:
                return True
        if self.request.user.is_mh and attempt.major in self.request.user.majors.all():
            return True
        return bool(self.request.user.has_perm("tm.can_view_all_attempts"))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["exceptions"] = self.object.exceptions(manager="current").all()
        return context


class PersonalLeaveCreateView(LoginRequiredMixin, UserPassesTestMixin, CreateView):
    model = PersonalLeave
    form_class = forms.PersonalLeaveForm

    def test_func(self):
        attempt = Attempt.objects.get(pk=self.kwargs["pk"])
        return bool(
            self.request.user.is_mh
            and attempt.major in self.request.user.majors.all()
            or self.request.user.has_perm("tm.add_personalleave"),
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["attempt"] = Attempt.objects.get(pk=self.kwargs["pk"])
        return context

    def form_valid(self, form):
        form.instance.attempt_id = self.kwargs["pk"]
        form.instance.editor = self.request.user
        messages.add_message(
            self.request,
            messages.SUCCESS,
            _("The new personal leave was saved."),
        )
        return super().form_valid(form)

    def get_success_url(self):
        return reverse("tm:attempt", args=[self.kwargs["pk"]])


class PersonalLeaveListView(PermissionRequiredMixin, ListView):
    model = PersonalLeave
    permission_required = "tm.view_personalleave"

    def get_queryset(self):
        session = current_session(self.request)

        if self.request.user.has_perm("tm.can_view_all_personalleaves"):
            return super().get_queryset().filter(attempt__session=session)
        if self.request.user.is_mh:
            return (
                super()
                .get_queryset()
                .filter(attempt__session=session)
                .filter(attempt__major__in=self.request.user.majors.all())
            )
        return super().get_queryset().none()


class PersonalLeaveUpdateView(PermissionRequiredMixin, UpdateView):
    model = PersonalLeave
    permission_required = "tm.change_personalleave"
    form_class = forms.PersonalLeaveForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["attempt"] = self.get_object().attempt
        return context

    def form_valid(self, form):
        form.instance.editor = self.request.user
        messages.add_message(
            self.request,
            messages.SUCCESS,
            _("The personal leave was saved."),
        )
        return super().form_valid(form)

    def get_success_url(self):
        return reverse("tm:leaves")


class PersonalLeaveDeleteView(PermissionRequiredMixin, DeleteView):
    model = PersonalLeave
    permission_required = "tm.delete_personalleave"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["attempt"] = self.get_object().attempt
        return context

    def form_valid(self, form):
        leave = self.get_object()
        student = leave.attempt.student
        attempt_id = leave.attempt.id
        type_of_leave = leave.get_type_of_leave_display()
        first_day_leave = leave.first_day_leave
        last_day_leave = leave.last_day_leave
        basis = leave.basis
        user_id = self.request.user.id
        delete_personal_leave.delay(
            attempt_id,
            type_of_leave,
            first_day_leave,
            last_day_leave,
            basis,
            user_id,
        )
        messages.add_message(
            self.request,
            messages.SUCCESS,
            _(f"{student}'s personal leave has been deleted."),  # noqa: INT001
        )
        return super().form_valid(form)

    def get_success_url(self):
        return reverse("tm:leaves")


class StudentExceptionDetailView(LoginRequiredMixin, UserPassesTestMixin, DetailView):
    model = StudentException

    def test_func(self):
        attempt = StudentException.objects.get(pk=self.kwargs["pk"]).attempt
        return bool(
            self.request.user.is_mh
            and attempt.major in self.request.user.majors.all()
            or self.request.user.has_perm("tm.view_studentexception"),
        )


class StudentExceptionUpdateView(PermissionRequiredMixin, UpdateView):
    model = StudentException
    form_class = forms.StudentExceptionForm
    permission_required = "tm.view_studentexception"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["update"] = True
        return context


class StudentExceptionCreateView(LoginRequiredMixin, UserPassesTestMixin, CreateView):
    model = StudentException
    form_class = forms.StudentExceptionForm

    def test_func(self):
        attempt = Attempt.objects.get(pk=self.kwargs["pk"])
        return bool(
            self.request.user.is_mh
            and attempt.major in self.request.user.majors.all()
            or self.request.user.has_perm("tm.add_studentexception"),
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["attempt"] = Attempt.objects.get(pk=self.kwargs["pk"])
        return context

    def form_valid(self, form):
        form.instance.attempt_id = self.kwargs["pk"]
        form.instance.created_by = self.request.user
        messages.add_message(
            self.request,
            messages.SUCCESS,
            _("The new student exception was saved."),
        )
        return super().form_valid(form)

    def get_success_url(self):
        return reverse("tm:attempt", args=[self.kwargs["pk"]])


class StudentExceptionListView(PermissionRequiredMixin, ListView):
    model = StudentException
    permission_required = "tm.view_studentexception"

    def get_queryset(self):
        session = current_session(self.request)

        if self.request.user.has_perm("tm.can_view_all_studentsexceptions"):
            return super().get_queryset().filter(attempt__session=session)
        if self.request.user.is_mh:
            return (
                super()
                .get_queryset()
                .filter(attempt__session=session)
                .filter(attempt__major__in=self.request.user.majors.all())
            )
        return super().get_queryset().none()


def clear_exception(request, pk):
    exception = get_object_or_404(StudentException, pk=pk)
    if request.user.has_perm("tm.can_clear_studentsexceptions"):
        exception.cleared = True
        exception.cleared_by = request.user
        exception.cleared_date = timezone.now().date()
        exception.save()
        messages.add_message(
            request,
            messages.SUCCESS,
            _(f"Exception for {exception.attempt.student} has been cleared."),  # noqa: INT001
        )
        return redirect("tm:exceptions")
    raise PermissionDenied


class ExpertListView(LoginRequiredMixin, ListView):
    model = User

    def get_queryset(self):
        if (
            self.request.user.has_perm("users.can_view_all_experts")
            or self.request.user.is_mh
        ):
            return (
                super()
                .get_queryset()
                .filter(is_expert=True)
                .prefetch_related("majors")
                .order_by("name")
            )
        if self.request.user.is_supervisor:
            return (
                super()
                .get_queryset()
                .filter(is_expert=True)
                .filter(majors__in=self.request.user.majors.all())
                .order_by("name")
            )
        return super().get_queryset().none()


@permission_required("users.can_adopt_user")
def adopt_expert(request, user_id):
    major = Major.objects.get(pk=request.session["tm_major_id"])
    expert = get_object_or_404(User, pk=user_id)
    expert.majors.add(major)
    expert.save()
    messages.add_message(
        request,
        messages.SUCCESS,
        _(f"{expert.name} has been adopted."),  # noqa: INT001
    )
    return redirect("tm:experts")


class ProgressReportCreateView(LoginRequiredMixin, UserPassesTestMixin, CreateView):
    model = ProgressReport

    def dispatch(self, request, *args, **kwargs):
        self.attempt = Attempt.objects.get(pk=kwargs["pk"])
        self.thesis = self.attempt.theses.first()
        if self.request.user == self.attempt.student:
            self.role = ProgressReport.STUDENT
            if self.thesis.student_report:
                # There is already a report, we redirect to the detail view
                return redirect(
                    "tm:view-progress-report",
                    pk=self.thesis.student_report.pk,
                )
        elif self.request.user == self.attempt.supervisor:
            self.role = ProgressReport.SUPERVISOR
            if self.thesis.supervisor_report:
                # There is already a report, we redirect to the detail view
                return redirect(
                    "tm:view-progress-report",
                    pk=self.thesis.supervisor_report.pk,
                )
        else:
            self.role = None
        return super().dispatch(request, *args, **kwargs)

    def test_func(self):
        return bool(self.role)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["attempt"] = self.attempt
        context["thesis"] = self.thesis
        context["role"] = self.role
        return context

    def get_form(self):
        if self.role == ProgressReport.STUDENT:
            self.form_class = forms.ProgressReportStudentForm
        elif self.role == ProgressReport.SUPERVISOR:
            self.form_class = forms.ProgressReportSupervisorForm
        return super().get_form()

    def form_valid(self, form):
        form.instance.master_thesis = self.thesis
        form.instance.user = self.request.user
        form.instance.role = self.role
        messages.add_message(
            self.request,
            messages.SUCCESS,
            _("The progress report was saved."),
        )
        return super().form_valid(form)


class ProgressReportUpdateView(
    LoginRequiredMixin,
    UserPassesTestMixin,
    SuccessMessageMixin,
    UpdateView,
):
    model = ProgressReport
    success_message = _("The progress report was updated.")

    def dispatch(self, request, *args, **kwargs):
        self.progress_report = self.get_object()
        self.attempt = self.progress_report.master_thesis.attempt
        if self.request.user == self.attempt.student:
            self.role = ProgressReport.STUDENT
        elif self.request.user == self.attempt.supervisor:
            self.role = ProgressReport.SUPERVISOR
        return super().dispatch(request, *args, **kwargs)

    def test_func(self):
        # Only the author can edit
        return self.request.user == self.get_object().user

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["attempt"] = self.attempt
        context["thesis"] = self.progress_report.master_thesis
        return context

    def get_form(self):
        if self.role == ProgressReport.STUDENT:
            self.form_class = forms.ProgressReportStudentForm
        elif self.role == ProgressReport.SUPERVISOR:
            self.form_class = forms.ProgressReportSupervisorForm
        return super().get_form()


class ProgressReportSubmitView(
    LoginRequiredMixin,
    UserPassesTestMixin,
    SuccessMessageMixin,
    UpdateView,
):
    model = ProgressReport
    form_class = forms.ProgressReportSubmitForm
    success_message = _("The progress report was submitted.")

    def test_func(self):
        # Only the author can edit
        return self.request.user == self.get_object().user

    def form_valid(self, form):
        form.instance.submitted = True
        form.instance.submission_date = timezone.now().date()
        response = super().form_valid(form)
        if self.object.role == ProgressReport.STUDENT:
            # We copy the abstract to the thesis
            master_thesis = self.object.master_thesis
            master_thesis.abstract = self.object.abstract
            master_thesis.save()
        return response


class ProgressReportView(LoginRequiredMixin, UserPassesTestMixin, DetailView):
    model = Attempt
    template_name = "tm/progressreport.html"

    def test_func(self):
        # This view is only for MH and administration
        attempt = self.get_object()
        return bool(
            self.request.user.has_perm("tm.view_progressreport")
            and attempt.major in self.request.user.majors.all(),
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        attempt = self.get_object()
        thesis = attempt.theses.first()
        report_student = thesis.reports.filter(role=ProgressReport.STUDENT).first()
        report_supervisor = thesis.reports.filter(
            role=ProgressReport.SUPERVISOR,
        ).first()
        if report_student and report_supervisor:
            if (
                abs(
                    report_student.global_assessment
                    - report_supervisor.global_assessment,
                )
                > 1
            ):
                context["disagreement"] = True
            else:
                context["disagreement"] = False
        context["report_student"] = report_student
        context["report_supervisor"] = report_supervisor
        context["attempt"] = attempt
        context["thesis"] = thesis
        return context


class ProgressReportDetailView(LoginRequiredMixin, UserPassesTestMixin, DetailView):
    model = ProgressReport

    def dispatch(self, request, *args, **kwargs):
        progress_report = self.get_object()
        self.attempt = progress_report.master_thesis.attempt
        if self.request.user == self.attempt.student:
            self.role = ProgressReport.STUDENT
        elif self.request.user == self.attempt.supervisor:
            self.role = ProgressReport.SUPERVISOR
        else:
            self.role = None
        return super().dispatch(request, *args, **kwargs)

    def test_func(self):
        if self.role:
            # Student and supervisor can always view their own progress report
            return True
        # MH and administration
        return (
            self.request.user.has_perm("tm.view_progressreport")
            and self.attempt.major in self.request.user.majors.all()
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["attempt"] = self.attempt
        context["submit_form"] = forms.ProgressReportSubmitForm(
            instance=self.get_object(),
        )
        return context


class ProgressReportListView(LoginRequiredMixin, ListView):
    model = MasterThesis
    template_name = "tm/progressreport_list.html"

    def get_queryset(self):
        session = current_session(self.request)
        if self.request.user.has_perm("tm.can_view_all_progressreports"):
            return (
                super()
                .get_queryset()
                .filter(attempt__session=session)
                .select_related(
                    "attempt__student",
                    "attempt__calendar",
                    "attempt__major",
                    "supervisor",
                    "expert",
                )
                .prefetch_related("reports", "co_supervisors", "defenses")
            )
        if self.request.user.is_mh:
            return (
                super()
                .get_queryset()
                .filter(attempt__session=session)
                .filter(attempt__major__in=self.request.user.majors.all())
                .select_related(
                    "attempt__student",
                    "attempt__calendar",
                    "attempt__major",
                    "supervisor",
                    "expert",
                )
                .prefetch_related("reports", "co_supervisors", "defenses")
            )
        if self.request.user.is_supervisor:
            return (
                super()
                .get_queryset()
                .filter(attempt__session=session)
                .filter(supervisor=self.request.user)
                .select_related(
                    "attempt__student",
                    "attempt__calendar",
                    "attempt__major",
                    "supervisor",
                    "expert",
                )
                .prefetch_related("reports", "co_supervisors", "defenses")
            )
        return super().get_queryset().none()


class ThesesListView(LoginRequiredMixin, ListView):
    model = MasterThesis

    def get_queryset(self):
        session = current_session(self.request)
        if self.request.user.has_perm("tm.can_view_all_theses"):
            return (
                super()
                .get_queryset()
                .filter(attempt__session=session)
                .select_related(
                    "attempt__student",
                    "attempt__calendar",
                    "attempt__major",
                    "supervisor",
                    "expert",
                )
                .prefetch_related("co_supervisors", "defenses")
            )
        if self.request.user.is_mh:
            return (
                super()
                .get_queryset()
                .filter(attempt__session=session)
                .filter(attempt__major__in=self.request.user.majors.all())
                .select_related(
                    "attempt__student",
                    "attempt__calendar",
                    "attempt__major",
                    "supervisor",
                    "expert",
                )
                .prefetch_related("co_supervisors", "defenses")
            )
        if self.request.user.is_supervisor:
            return (
                super()
                .get_queryset()
                .filter(attempt__session=session)
                .filter(supervisor=self.request.user)
                .select_related(
                    "attempt__student",
                    "attempt__calendar",
                    "attempt__major",
                    "supervisor",
                    "expert",
                )
                .prefetch_related("co_supervisors", "defenses")
            )
        return super().get_queryset().none()


class ThesisUpdateView(
    LoginRequiredMixin,
    UserPassesTestMixin,
    SuccessMessageMixin,
    UpdateView,
):
    model = MasterThesis
    success_message = _("The thesis was updated.")

    def dispatch(self, request, *args, **kwargs):
        if self.request.user.is_authenticated:
            self.master_thesis = self.get_object()
            self.attempt = self.master_thesis.attempt
            if self.request.user == self.attempt.student:
                self.role = ProgressReport.STUDENT
            elif self.request.user == self.attempt.supervisor:
                self.role = ProgressReport.SUPERVISOR
            elif (
                self.request.user.is_mh
                and self.attempt.major in self.request.user.majors.all()
            ):
                self.role = ProgressReport.MH
            else:
                self.role = None
        else:
            self.role = None
        return super().dispatch(request, *args, **kwargs)

    def test_func(self):
        return bool(self.role)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["attempt"] = self.attempt
        context["role"] = self.role
        if self.role in [ProgressReport.SUPERVISOR, ProgressReport.MH]:
            context["form"].fields["expert"].queryset = (
                User.objects.filter(Q(is_expert=True) | Q(is_supervisor=True))
                .filter(majors__in=[self.attempt.major])
                .order_by("name")
            )
        return context

    def get_form(self):
        if self.role == ProgressReport.STUDENT:
            self.form_class = forms.MasterThesisStudentForm
        elif self.role in [ProgressReport.SUPERVISOR, ProgressReport.MH]:
            self.form_class = forms.MasterThesisSupervisorForm
        return super().get_form()


class ThesisDetailView(LoginRequiredMixin, UserPassesTestMixin, DetailView):
    model = MasterThesis

    def dispatch(self, request, *args, **kwargs):
        if self.request.user.is_authenticated:
            master_thesis = self.get_object()
            self.attempt = master_thesis.attempt
            if self.request.user == self.attempt.student:
                self.role = ProgressReport.STUDENT
            elif (
                self.request.user.is_mh
                and self.attempt.major in self.request.user.majors.all()
            ) or (
                self.request.user.has_perm("tm.view_masterthesis")
                and self.attempt.major in self.request.user.majors.all()
            ):
                self.role = ProgressReport.MH
            elif self.request.user == self.attempt.supervisor:
                self.role = ProgressReport.SUPERVISOR
            elif self.request.user == master_thesis.expert:
                return redirect(
                    reverse("tm:expert-view-thesis", args=[master_thesis.pk]),
                )
            else:
                self.role = None
        else:
            self.role = None
        return super().dispatch(request, *args, **kwargs)

    def test_func(self):
        return bool(self.role)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["attempt"] = self.attempt
        context["role"] = self.role
        if self.role in [ProgressReport.STUDENT, ProgressReport.MH]:
            context["publishing_agreement_form"] = forms.AuthorizationPublishForm()
        if self.role in [ProgressReport.EXPERT, ProgressReport.MH]:
            context["thesis_assessment_form"] = forms.ThesisAssessmentForm()
            context["statement_fees_form"] = forms.StatementFeesForm()
        return context


class ThesisExpertDetailView(LoginRequiredMixin, UserPassesTestMixin, DetailView):
    model = MasterThesis
    template_name = "tm/masterthesis_expert_detail.html"

    def dispatch(self, request, *args, **kwargs):
        if self.request.user.is_authenticated:
            master_thesis = self.get_object()
            self.attempt = master_thesis.attempt
            if self.request.user == master_thesis.expert:
                self.role = ProgressReport.EXPERT
            elif (
                self.request.user.is_mh
                and self.attempt.major in self.request.user.majors.all()
            ) or (
                self.request.user.has_perm("tm.view_masterthesis")
                and self.attempt.major in self.request.user.majors.all()
            ):
                self.role = ProgressReport.MH
            else:
                self.role = None
        else:
            self.role = None
        return super().dispatch(request, *args, **kwargs)

    def test_func(self):
        return bool(self.role)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["acceptation_form"] = forms.MasterThesisExpertAcceptForm()
        context["decline_form"] = forms.MasterThesisExpertDeclineForm()
        context["attempt"] = self.attempt
        context["role"] = self.role
        if self.get_object().paper_submission_expert:
            context["paper_submission_expert"] = _("paper hard copy")
        else:
            context["paper_submission_expert"] = _("PDF digital copy")
        context["statement_fees_form"] = forms.StatementFeesForm()
        return context


class ThesisExpertAcceptView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = MasterThesis
    form_class = forms.MasterThesisExpertAcceptForm

    def test_func(self):
        # Only the author can edit
        return self.request.user == self.get_object().expert

    def get_success_url(self):
        return reverse_lazy("tm:expert-view-thesis", kwargs={"pk": self.object.pk})

    def form_valid(self, form):
        from .tasks import send_expert_confirmation

        form.instance.expert_confirmed = True
        form.instance.expert_confirmation_date = timezone.now().date()
        form.instance.expert_name = self.object.expert.name
        response = super().form_valid(form)
        success_message = _(
            "Thank you for agreeing to evaluate %(student)s's master's thesis.",
        )
        messages.success(
            self.request,
            success_message % {"student": self.object.attempt.student.name},
        )
        # send notification
        transaction.on_commit(lambda: send_expert_confirmation.delay(self.object.id))
        return response


class ThesisExpertDeclineView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = MasterThesis
    form_class = forms.MasterThesisExpertDeclineForm

    def test_func(self):
        # Only the author can edit
        return self.request.user == self.get_object().expert

    def get_success_url(self):
        return reverse_lazy("tm:dashboard")

    def form_valid(self, form):
        from .tasks import send_expert_declined

        expert_id = self.object.expert_id
        message = form.cleaned_data["message"]
        form.instance.expert_confirmed = False
        form.instance.expert_confirmation_date = None
        form.instance.expert_name = None
        form.instance.expert = None
        response = super().form_valid(form)
        success_message = _("We have taken note of your decision.")
        messages.success(self.request, success_message)
        # send notification
        transaction.on_commit(
            lambda: send_expert_declined.delay(self.object.id, expert_id, message),
        )
        return response


class ThesisSubmitView(
    LoginRequiredMixin,
    UserPassesTestMixin,
    SuccessMessageMixin,
    UpdateView,
):
    model = MasterThesis
    form_class = forms.ThesisSubmitForm
    success_message = _("The thesis was submitted.")

    def test_func(self):
        # Only the author can edit
        return self.request.user == self.get_object().attempt.student

    def form_valid(self, form):
        form.instance.submitted = True
        form.instance.submission_date = timezone.now()
        return super().form_valid(form)


class ThesesMailingView(LoginRequiredMixin, ListView):
    model = MasterThesis
    template_name = "tm/masterthesis_mailing_list.html"

    def get_queryset(self):
        session = current_session(self.request)
        if self.request.user.has_perm("tm.can_view_all_theses"):
            return (
                super()
                .get_queryset()
                .filter(attempt__session=session)
                .select_related("attempt", "supervisor", "expert")
            ).order_by("attempt__major", "attempt__student")
        if self.request.user.is_mh:
            return (
                super()
                .get_queryset()
                .filter(attempt__session=session)
                .filter(attempt__major__in=self.request.user.majors.all())
                .select_related("attempt", "supervisor", "expert")
            ).order_by("attempt__major", "attempt__student")
        return super().get_queryset().none()


class ThesisSubmitAuthorizationPublish(
    LoginRequiredMixin,
    UserPassesTestMixin,
    UpdateView,
):
    model = MasterThesis
    form_class = forms.AuthorizationPublishForm

    def test_func(self):
        if self.request.user == self.get_object().attempt.student:
            # Only the student can edit
            return True
        return bool(
            self.request.user.is_mh
            and self.get_object().attempt.major in self.request.user.majors.all(),
        )

    def get_success_url(self):
        return reverse_lazy("tm:view-thesis", kwargs={"pk": self.object.pk})

    def form_valid(self, form):
        response = super().form_valid(form)
        success_message = _("Thank you for submitting the publishing agreement.")
        messages.success(
            self.request,
            success_message,
        )
        return response


class ThesisRecordAuthorizationPublish(
    LoginRequiredMixin,
    UserPassesTestMixin,
    UpdateView,
):
    model = MasterThesis
    form_class = forms.RecordAuthorizationPublishForm
    template_name = "tm/masterthesis_record_authorization_publish.html"

    def test_func(self):
        return bool(
            self.request.user.is_mh
            and self.get_object().attempt.major in self.request.user.majors.all(),
        )

    def get_success_url(self):
        return reverse_lazy("tm:defenses")

    def form_valid(self, form):
        response = super().form_valid(form)
        success_message = _(
            "Thank you for recording the publishing agreement decision.",
        )
        messages.success(
            self.request,
            success_message,
        )
        return response


class DefenseSubmitStatementOfFees(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Defense
    form_class = forms.StatementFeesForm

    def test_func(self):
        if self.request.user == self.get_object().master_thesis.expert:
            # Only the expert can submit
            return True
        if self.request.user == self.get_object().master_thesis.supervisor:
            # The supervisor can submit for the expert
            return True
        return bool(
            self.request.user.is_mh
            and self.get_object().master_thesis.attempt.major
            in self.request.user.majors.all(),
        )

    def get_success_url(self):
        if self.request.user == self.object.master_thesis.expert:
            return reverse_lazy(
                "tm:expert-view-thesis",
                kwargs={"pk": self.object.master_thesis.pk},
            )
        return reverse_lazy(
            "tm:view-thesis",
            kwargs={"pk": self.object.master_thesis.pk},
        )

    def form_valid(self, form):
        response = super().form_valid(form)
        success_message = _("Thank you for submitting your statement of fees.")
        messages.success(
            self.request,
            success_message,
        )
        return response


class DefenseSubmitAssessment(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Defense
    form_class = forms.ThesisAssessmentForm
    template_name = "tm/defense_assessment.html"

    def dispatch(self, request, *args, **kwargs):
        self.remediation = None
        return super().dispatch(request, *args, **kwargs)

    def test_func(self):
        defense = self.get_object()
        if self.request.user == defense.master_thesis.supervisor:
            # Only the supervisor can submit
            return True
        return bool(
            self.request.user.is_mh
            and defense.master_thesis.attempt.major in self.request.user.majors.all(),
        )

    def get_success_url(self):
        if self.remediation:
            return reverse_lazy(
                "tm:remediation-objectives",
                kwargs={"pk": self.object.pk},
            )
        return reverse_lazy(
            "tm:view-thesis",
            kwargs={"pk": self.object.master_thesis.pk},
        )

    def form_valid(self, form):
        if 3.5 <= form.instance.grade < 4:  # noqa: PLR2004
            # This is a remediation
            self.remediation = True
        else:
            form.instance.assessment_submitted = True
            form.instance.assessment_submission_date = timezone.now()
            self.remediation = False
        response = super().form_valid(form)
        if self.remediation:
            warning_message = _(
                "Thank you for submitting the assessment. Since this is a remediation, "
                "you must complete and validate the remediation objectives within 5 "
                "days of the defense.",
            )
            messages.warning(
                self.request,
                warning_message,
            )
        else:
            success_message = _("Thank you for submitting the thesis assessment.")
            messages.success(
                self.request,
                success_message,
            )
        return response


class DefenseRemediationObjectives(LoginRequiredMixin, UserPassesTestMixin, DetailView):
    model = Defense
    template_name = "tm/defense_remediation.html"

    def test_func(self):
        defense = self.get_object()
        if self.request.user == defense.master_thesis.supervisor:
            # Only the supervisor can view
            return True
        return bool(
            self.request.user.is_mh
            and defense.master_thesis.attempt.major in self.request.user.majors.all(),
        )


def excel_addresses(request):  # noqa: PLR0915
    session = current_session(request)

    if request.user.has_perm("tm.can_view_all_theses"):
        theses = (
            MasterThesis.objects.filter(attempt__session=session)
            .select_related("attempt", "supervisor", "expert")
            .order_by("attempt__major", "attempt__student")
        )
    elif request.user.is_mh:
        theses = (
            MasterThesis.objects.filter(attempt__session=session)
            .filter(attempt__major__in=request.user.majors.all())
            .select_related("attempt", "supervisor", "expert")
            .order_by("attempt__major", "attempt__student")
        )
    else:
        theses = MasterThesis.objects.none()

    xlsx_file = BytesIO()

    workbook = xlsxwriter.Workbook(xlsx_file)
    bold = workbook.add_format({"bold": True, "bottom": 1})
    title = workbook.add_format({"bold": True, "font_size": 14})
    border = workbook.add_format({"bottom": 1})
    worksheet = workbook.add_worksheet("MScBA TM")
    worksheet.write("A1", "MScBA - Master Thesis")
    worksheet.write("A3", f"Mailing Addresses - Session {session.name}", title)
    worksheet.write("A6", "Student", bold)
    worksheet.write("B6", "Major", bold)
    worksheet.write("C6", "Title", bold)
    worksheet.write("D6", "Role", bold)
    worksheet.write("E6", "Name", bold)
    worksheet.write("F6", "Address", bold)
    worksheet.write("G6", "Additional address", bold)
    worksheet.write("H6", "ZIP code", bold)
    worksheet.write("I6", "City", bold)
    row = 6
    col = 0
    for thesis in theses:
        if thesis.paper_submission_supervisor:
            worksheet.write(row, col, thesis.attempt.student.name, border)
            worksheet.write(row, col + 1, thesis.attempt.major.code, border)
            worksheet.write(row, col + 2, thesis.title, border)
            worksheet.write(row, col + 3, "Supervisor", border)
            worksheet.write(row, col + 4, thesis.supervisor.name, border)
            worksheet.write(row, col + 5, thesis.supervisor.address, border)
            worksheet.write(row, col + 6, thesis.supervisor.additional_address, border)
            worksheet.write(row, col + 7, thesis.supervisor.zip_code, border)
            worksheet.write(row, col + 8, thesis.supervisor.city, border)
            row += 1
        if thesis.paper_submission_expert:
            worksheet.write(row, col, thesis.attempt.student.name, border)
            worksheet.write(row, col + 1, thesis.attempt.major.code, border)
            worksheet.write(row, col + 2, thesis.title, border)
            worksheet.write(row, col + 3, "Expert", border)
            worksheet.write(row, col + 4, thesis.expert.name, border)
            worksheet.write(row, col + 5, thesis.expert.address, border)
            worksheet.write(row, col + 6, thesis.expert.additional_address, border)
            worksheet.write(row, col + 7, thesis.expert.zip_code, border)
            worksheet.write(row, col + 8, thesis.expert.city, border)
            row += 1
    worksheet.autofit()
    worksheet.set_column(2, 2, 80)
    workbook.close()
    xlsx_file.seek(0)
    response = FileResponse(xlsx_file, content_type="application/vnd.ms-excel")
    response["Content-Disposition"] = (
        'attachment; filename="MScBA_mailing_addresses.xlsx"'
    )
    return response


def download_theses_zip(request):
    session = current_session(request)

    if request.user.has_perm("tm.can_export_documents"):
        theses = MasterThesis.objects.filter(
            attempt__session=session,
            submitted=True,
        ).filter(attempt__major__in=request.user.majors.all())
    elif request.user.is_supervisor:
        theses = MasterThesis.objects.filter(
            attempt__session=session,
            submitted=True,
        ).filter(supervisor=request.user)
    else:
        theses = MasterThesis.objects.none()

    zip_file = BytesIO()
    with zipfile.ZipFile(zip_file, "w") as zf:
        for master_thesis in theses:
            if master_thesis.confidential:
                file_name = f"MScBA_{master_thesis.attempt.major.code}_{master_thesis.attempt.session.name}_Thesis_{master_thesis.attempt.student}-CONFIDENTIAL.pdf"  # noqa: E501
            else:
                file_name = f"MScBA_{master_thesis.attempt.major.code}_{master_thesis.attempt.session.name}_Thesis_{master_thesis.attempt.student}.pdf"  # noqa: E501
            zf.writestr(file_name, master_thesis.pdf_file.file.read())
    zip_file.seek(0)
    response = FileResponse(zip_file, content_type="application/zip")
    response["Content-Disposition"] = (
        f'attachment; filename="MScBA_{master_thesis.attempt.session.name}_Theses.zip"'
    )
    return response


def download_thesis_zip(request, pk):
    master_thesis = get_object_or_404(MasterThesis, pk=pk)

    if request.user.has_perm("tm.can_export_documents") or (
        request.user.is_mh and master_thesis.attempt.major in request.user.majors.all()
    ):
        zip_file = create_zip_thesis(master_thesis.id)
        response = FileResponse(zip_file, content_type="application/zip")
        response["Content-Disposition"] = (
            f'attachment; filename="MScBA_{master_thesis.attempt.major.code}_{master_thesis.attempt.session.name}_{master_thesis.attempt.student}.zip"'  # noqa: E501
        )
        return response
    return HttpResponseForbidden()


def download_zip_authorization_publish(request):
    session = current_session(request)
    major_code = ""

    if request.user.has_perm("tm.can_export_documents"):
        major_ids = request.user.majors.all().values_list("id", flat=True)
        if request.GET.get("major"):
            major = get_object_or_404(Major, pk=request.GET.get("major"))
            if major.id in major_ids:
                major_ids = [major.id]
                major_code = f"_{major.code}"
            else:
                HttpResponseForbidden()

        zip_file = create_zip_authorization_publish(session.id, major_ids)
        response = FileResponse(zip_file, content_type="application/zip")
        response["Content-Disposition"] = (
            f'attachment; filename="MScBA_{session.name}{major_code}_Publishing_consent_forms.zip"'  # noqa: E501
        )
        return response
    return HttpResponseForbidden()


def download_zip_statement_fees(request):
    session = current_session(request)
    major_code = ""

    if request.user.has_perm("tm.can_export_documents"):
        major_ids = request.user.majors.all().values_list("id", flat=True)
        if request.GET.get("major"):
            major = get_object_or_404(Major, pk=request.GET.get("major"))
            if major.id in major_ids:
                major_ids = [major.id]
                major_code = f"_{major.code}"
            else:
                HttpResponseForbidden()

        zip_file = create_zip_statement_fees(session.id, major_ids)
        response = FileResponse(zip_file, content_type="application/zip")
        response["Content-Disposition"] = (
            f'attachment; filename="MScBA_{session.name}{major_code}_Statement_of_fees_forms.zip"'  # noqa: E501
        )
        return response
    return HttpResponseForbidden()


def download_zip_assessment(request):
    session = current_session(request)
    major_code = ""

    if request.user.has_perm("tm.can_export_documents"):
        major_ids = request.user.majors.all().values_list("id", flat=True)
        if request.GET.get("major"):
            major = get_object_or_404(Major, pk=request.GET.get("major"))
            if major.id in major_ids:
                major_ids = [major.id]
                major_code = f"_{major.code}"
            else:
                HttpResponseForbidden()

        zip_file = create_zip_assessment(session.id, major_ids)
        response = FileResponse(zip_file, content_type="application/zip")
        response["Content-Disposition"] = (
            f'attachment; filename="MScBA_{session.name}{major_code}_Theses_assessments_forms.zip"'  # noqa: E501
        )
        return response
    return HttpResponseForbidden()


class DefensesListView(LoginRequiredMixin, ListView):
    model = Defense

    def get_queryset(self):
        session = current_session(self.request)
        if self.request.user.has_perm("tm.can_view_all_theses"):
            return (
                super()
                .get_queryset()
                .filter(master_thesis__attempt__session=session)
                .select_related(
                    "master_thesis__attempt__student",
                    "master_thesis__attempt__session",
                    "master_thesis__attempt__major",
                    "master_thesis__expert",
                    "master_thesis__supervisor",
                )
                .prefetch_related("master_thesis__co_supervisors")
            )
        if self.request.user.is_mh:
            return (
                super()
                .get_queryset()
                .filter(master_thesis__attempt__session=session)
                .filter(
                    master_thesis__attempt__major__in=self.request.user.majors.all(),
                )
                .select_related(
                    "master_thesis__attempt__student",
                    "master_thesis__attempt__session",
                    "master_thesis__attempt__major",
                    "master_thesis__expert",
                    "master_thesis__supervisor",
                )
                .prefetch_related("master_thesis__co_supervisors")
            )
        if self.request.user.is_supervisor:
            return (
                super()
                .get_queryset()
                .filter(master_thesis__attempt__session=session)
                .filter(master_thesis__supervisor=self.request.user)
                .select_related(
                    "master_thesis__attempt__student",
                    "master_thesis__attempt__session",
                    "master_thesis__attempt__major",
                    "master_thesis__expert",
                    "master_thesis__supervisor",
                )
                .prefetch_related("master_thesis__co_supervisors")
            )
        return super().get_queryset().none()


class DefenseCreateView(
    LoginRequiredMixin,
    UserPassesTestMixin,
    SuccessMessageMixin,
    CreateView,
):
    model = Defense

    def dispatch(self, request, *args, **kwargs):
        self.thesis = MasterThesis.objects.get(pk=kwargs["pk"])
        return super().dispatch(request, *args, **kwargs)

    def test_func(self):
        return bool(
            self.request.user.is_mh
            and self.thesis.attempt.major in self.request.user.majors.all()
            or self.request.user == self.thesis.supervisor,
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["thesis"] = self.thesis
        return context

    def get_form(self):
        self.form_class = forms.DefenseSupervisorForm
        return super().get_form()

    def get_initial(self):
        initial = super().get_initial()
        initial["master_thesis"] = self.thesis
        return initial

    def form_valid(self, form):
        form.instance.master_thesis = self.thesis
        messages.add_message(
            self.request,
            messages.SUCCESS,
            _(
                "The date of the defense was recorded and a confirmation e-mail was sent to everyone. If the defense takes place at HES-SO Master, an update will be sent as soon as a room has been booked by the administration.",  # noqa: E501
            ),
        )
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy("tm:view-thesis", kwargs={"pk": self.thesis.pk})


class DefenseBookRoomUpdateView(
    LoginRequiredMixin,
    UserPassesTestMixin,
    SuccessMessageMixin,
    UpdateView,
):
    model = Defense

    def test_func(self):
        defense = self.get_object()
        return bool(
            self.request.user.is_mh
            and defense.master_thesis.attempt.major in self.request.user.majors.all(),
        )

    def get_context_data(self, **kwargs):
        defense = self.get_object()
        context = super().get_context_data(**kwargs)
        context["thesis"] = defense.master_thesis
        return context

    def get_form(self):
        self.form_class = forms.DefenseAdminForm
        return super().get_form()

    def form_valid(self, form):
        messages.add_message(
            self.request,
            messages.SUCCESS,
            _(
                "The the defense was updated and a confirmation e-mail was sent "
                "to everyone.",
            ),
        )
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy(
            "tm:view-thesis",
            kwargs={"pk": self.object.master_thesis.pk},
        )


class ImportStudentsView(LoginRequiredMixin, PermissionRequiredMixin, FormView):
    template_name = "tm/import_students.html"
    form_class = forms.ImportStudentsForm
    success_url = reverse_lazy("tm:import-students")  # Replace with your success URL
    permission_required = "tm.can_import_students"

    def dispatch(self, request, *args, **kwargs):
        self.task = None
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context["form"].fields["major_id"].queryset = Major.objects.filter(
            id__in=self.request.user.majors.all(),
        ).order_by("name")
        return context

    def form_valid(self, form):
        isa_ids = form.cleaned_data["isa_ids"].splitlines()
        language_code = form.cleaned_data["language_code"]
        session_id = form.cleaned_data["session_id"].id
        major_id = form.cleaned_data["major_id"].id

        self.task = import_students_by_isa.apply_async(
            (isa_ids, language_code, session_id, major_id),
        )

        return super().form_valid(form)

    def get_success_url(self) -> str:
        return reverse("tm:import-students-progress", kwargs={"task_id": self.task.id})


class ImportStudentsProgressView(
    LoginRequiredMixin,
    PermissionRequiredMixin,
    TemplateView,
):
    template_name = "tm/import_students_progress.html"
    permission_required = "tm.can_import_students"

    def get_context_data(self, **kwargs):
        return super().get_context_data(**kwargs)


class GroupsListView(LoginRequiredMixin, UserPassesTestMixin, TemplateView):
    template_name = "tm/groups_list.html"

    def test_func(self):
        groups = Group.objects.all()
        return self.request.user.groups.filter(pk__in=groups).exists()

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context["majors"] = Major.objects.filter(
            id__in=self.request.user.majors.all(),
        ).order_by("name")
        return context


class ImportExportDocumentsView(
    LoginRequiredMixin,
    PermissionRequiredMixin,
    TemplateView,
):
    template_name = "tm/import_documents.html"
    form_class = forms.ImportStudentsForm
    success_url = reverse_lazy("tm:import-students")
    permission_required = "tm.can_import_students"

    def dispatch(self, request, *args, **kwargs):
        self.task = None
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context["majors"] = Major.objects.filter(
            id__in=self.request.user.majors.all(),
        ).order_by("name")
        return context


class AnonymizeDataView(LoginRequiredMixin, UserPassesTestMixin, FormView):
    template_name = "tm/anonymize_data.html"
    form_class = forms.AnonymizeDataForm

    def test_func(self):
        return (
            settings.SETTINGS_MODULE != "config.settings.production"
            and self.request.user.is_staff
        )

    def dispatch(self, request, *args, **kwargs):
        self.task = None
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context["default_password"] = settings.DEFAULT_PASSWORD
        return context

    def form_valid(self, form):
        anonymize = form.cleaned_data["anonymize"]
        if anonymize:
            self.task = anonymize_data.apply_async()
            return super().form_valid(form)
        return None

    def get_success_url(self) -> str:
        return reverse("tm:anonymize-data-progress", kwargs={"task_id": self.task.id})


class AnonymizeDataProgressView(LoginRequiredMixin, UserPassesTestMixin, TemplateView):
    template_name = "tm/anonymize_data_progress.html"

    def test_func(self):
        return (
            settings.SETTINGS_MODULE != "config.settings.production"
            and self.request.user.is_staff
        )


@permission_required("tm.can_import_documents")
def file_upload(request):
    if request.method == "POST":
        new_file = request.FILES.get("file")
        pdf_file_bytes = BytesIO()
        for chunk in new_file.chunks():
            pdf_file_bytes.write(chunk)
        pdf_file_bytes.seek(0)
        imports = split_and_save_pdf_scans(pdf_file_bytes)
        response = f"The file <b>'{new_file.name}'</b> was processed<ul>{imports}</ul>"
        return HttpResponse(response)
    return JsonResponse({"post": "false"})


def validate_progress_reports(request, pk):
    attempt = get_object_or_404(Attempt, pk=pk)
    if request.user.is_mh:
        if attempt.major in request.user.majors.all():
            thesis = attempt.theses.first()
            report_student = thesis.reports.filter(role=ProgressReport.STUDENT).first()
            report_supervisor = thesis.reports.filter(
                role=ProgressReport.SUPERVISOR,
            ).first()
            if report_student.submitted and report_supervisor.submitted:
                report_student.validated = True
                report_student.validation_date = timezone.now()
                report_student.save()
                report_supervisor.validated = True
                report_supervisor.validation_date = timezone.now()
                report_supervisor.save()
                messages.add_message(
                    request,
                    messages.SUCCESS,
                    _("The progress reports were validated."),
                )
                return redirect("tm:progress-reports")
            messages.add_message(
                request,
                messages.WARNING,
                _("You can only validate progress reports if both are submitted!"),
            )
            return redirect(reverse("tm:check-progress-report", args=[pk]))
    raise PermissionDenied


def force_submission_progress_report(request, pk):
    from apps.tm.tasks import overridden_notification_report

    report = get_object_or_404(ProgressReport, pk=pk)
    if request.user.is_mh:
        if report.master_thesis.attempt.major in request.user.majors.all():
            if not report.submitted:
                report.submitted = True
                report.submission_date = timezone.now()
                report.forced_submission = True
                report.forced_submission_user = request.user
                report.save()
                transaction.on_commit(
                    lambda: overridden_notification_report.delay(report.id),
                )
                messages.add_message(
                    request,
                    messages.SUCCESS,
                    _(
                        "The submission of the progress report was overridden and the "
                        "user was notified.",
                    ),
                )
                return redirect(
                    reverse(
                        "tm:check-progress-report",
                        args=[report.master_thesis.attempt.id],
                    ),
                )
            messages.add_message(
                request,
                messages.WARNING,
                _("The progress report was already submitted!"),
            )
            return redirect(
                reverse(
                    "tm:check-progress-report",
                    args=[report.master_thesis.attempt.id],
                ),
            )
    raise PermissionDenied


def send_reminder_progress_reports(request, pk=None):
    if pk:
        # We send a reminder for a specific attempt
        from apps.tm.tasks import send_notification_report

        attempt = get_object_or_404(Attempt, pk=pk)
        thesis = attempt.theses.first()
        report_student = thesis.reports.filter(role=ProgressReport.STUDENT).first()
        report_supervisor = thesis.reports.filter(
            role=ProgressReport.SUPERVISOR,
        ).first()
        student = not report_student.submitted if report_student else True
        supervisor = not report_supervisor.submitted if report_supervisor else True
        send_notification_report.delay(
            pk,
            reminder=True,
            student=student,
            supervisor=supervisor,
        )
        if student and supervisor:
            messages.add_message(
                request,
                messages.SUCCESS,
                _("The reminder was sent to the student and the supervisor."),
            )
        elif student:
            messages.add_message(
                request,
                messages.SUCCESS,
                _("The reminder was sent to the student."),
            )
        elif supervisor:
            messages.add_message(
                request,
                messages.SUCCESS,
                _("The reminder was sent to the supervisor."),
            )
        else:
            messages.add_message(
                request,
                messages.WARNING,
                _("No reminder was sent, all reports are submitted."),
            )
    else:
        # We send a reminder for all attempts
        from apps.tm.tasks import send_notification_all_report

        send_notification_all_report.delay()
        messages.add_message(
            request,
            messages.SUCCESS,
            _("Reminders were sent for all missing reports."),
        )
    return redirect("tm:progress-reports")


def test_username(username):
    url = f"https://people.hes-so.ch/fr/profile/{username}"
    r = requests.get(url, timeout=5)
    return r.status_code != 404  # noqa: PLR2004


@user_passes_test(lambda u: u.has_perm("users.can_search_directory"))
@login_required
def link_account(request, pk):  # noqa: PLR0912
    if not request.user.is_mh:
        raise PermissionDenied

    assignment = get_object_or_404(Assignment, pk=pk)

    if request.method == "POST":
        form = forms.LinkAccountForm(request.POST)
        if form.is_valid():
            submit_action = form.cleaned_data["submit_action"]
            if submit_action == "create":
                isa_id = form.cleaned_data["hesso_id"]
                user = None

                if isa_id:
                    # We check if the user already exists
                    try:
                        user = User.objects.get(username=f"{isa_id}@hes-so.ch")
                        user.is_supervisor = True
                        user.save()
                        user.majors.add(assignment.proposal.attempt.major)
                    except User.DoesNotExist:
                        # We retrieve the user information from the directory
                        supervisor = query_user_by_id(isa_id)
                        if supervisor:
                            # We create the user
                            user = User.objects.create(
                                username=f"{isa_id}@hes-so.ch",
                                first_name=supervisor["first_name"],
                                last_name=supervisor["last_name"],
                                email=supervisor["email"],
                                name=supervisor["display_name"],
                                gender=supervisor["gender"],
                                is_supervisor=True,
                            )
                            user.majors.add(assignment.proposal.attempt.major)
                else:
                    messages.add_message(
                        request,
                        messages.ERROR,
                        _(
                            "You need to select a name from the HES-SO directory in "
                            "order to create a new account.",
                        ),
                    )

                if user:
                    assignment.supervisor = user
                    assignment.validated = True
                    assignment.request_date = timezone.now().date()
                    assignment.save()
                    msg = f"{_('A new account was created for')} {user.name}"
                    messages.add_message(
                        request,
                        messages.SUCCESS,
                        msg,
                    )
                else:
                    messages.add_message(
                        request,
                        messages.ERROR,
                        _(
                            "The provided HES-SO ID {}@hes-so.ch is not valid!".format(  # noqa: INT002
                                isa_id,
                            ),
                        ),
                    )

            elif submit_action == "reject":
                assignment.validated = False
                assignment.request_date = timezone.now().date()
                assignment.save()
                messages.add_message(
                    request,
                    messages.SUCCESS,
                    _(f"Assignment of {assignment.suggestion} has been rejected."),  # noqa: INT001
                )
            else:
                raise Http404(_("Wrong action."))

            return redirect("tm:confirm")

    else:
        form = forms.LinkAccountForm()
    return render(
        request,
        "tm/link_account.html",
        {"form": form, "assignment": assignment},
    )


@user_passes_test(lambda u: u.has_perm("users.can_search_directory"))
@login_required
def create_user_hes_so(request):  # noqa: C901, PLR0912
    if request.method == "POST":
        form = forms.CreateAccountForm(request.POST)
        if form.is_valid():
            isa_id = form.cleaned_data["hesso_id"]
            user = None

            if isa_id:
                # We check if the user already exists
                try:
                    user = User.objects.get(username=f"{isa_id}@hes-so.ch")
                    if form.cleaned_data["is_supervisor"]:
                        user.is_supervisor = True
                    if form.cleaned_data["is_expert"]:
                        user.is_expert = True
                    if form.cleaned_data["is_student"]:
                        user.is_student = True
                    if form.cleaned_data["is_mh"]:
                        user.is_mh = True
                    user.save()
                    if form.cleaned_data["majors"]:
                        user.majors.add(*form.cleaned_data["majors"])
                    else:
                        user.majors.add(*request.user.majors.all())
                    messages.add_message(
                        request,
                        messages.INFO,
                        _(f"User {user} has been updated."),  # noqa: INT001
                    )
                except User.DoesNotExist:
                    # We retrieve the user information from the directory
                    supervisor = query_user_by_id(isa_id)
                    if supervisor:
                        # We create the user
                        user = User.objects.create(
                            username=f"{isa_id}@hes-so.ch",
                            first_name=supervisor["first_name"],
                            last_name=supervisor["last_name"],
                            email=supervisor["email"],
                            name=supervisor["display_name"],
                            gender=supervisor["gender"],
                        )
                        if form.cleaned_data["is_supervisor"]:
                            user.is_supervisor = True
                        if form.cleaned_data["is_expert"]:
                            user.is_expert = True
                        if form.cleaned_data["is_student"]:
                            user.is_student = True
                        if form.cleaned_data["is_mh"]:
                            user.is_mh = True
                        user.save()
                        if form.cleaned_data["majors"]:
                            user.majors.add(*form.cleaned_data["majors"])
                        else:
                            user.majors.add(*request.user.majors.all())
                        messages.add_message(
                            request,
                            messages.INFO,
                            _(f"User {user} has been created."),  # noqa: INT001
                        )
                    else:
                        messages.add_message(
                            request,
                            messages.ERROR,
                            _(
                                f"Unable to retrieve user profile for {isa_id}@hes-so.ch.",  # noqa: E501, INT001
                            ),
                        )
            else:
                messages.add_message(
                    request,
                    messages.ERROR,
                    _(
                        "You need to select a name from the HES-SO directory in order "
                        "to create a new account.",
                    ),
                )
        return redirect("tm:dashboard")

    form = forms.CreateAccountForm()
    return render(request, "tm/create_account.html", {"form": form})


@user_passes_test(lambda u: u.has_perm("users.can_search_directory"))
@login_required
def search_hesso_directory(request):
    people = request.GET.get("people", "")
    if people:
        if len(people) > 3:  # noqa: PLR2004
            result = query_user_by_name(people)
            return JsonResponse({"results": result})
    return JsonResponse({"results": None})


def validate_supervisor(request, pk, decision):
    assignment = get_object_or_404(Assignment, pk=pk)
    if (
        request.user.is_mh
        and assignment.proposal.attempt.major in request.user.majors.all()
    ):
        if decision == "validated":
            assignment.validated = True
            messages.add_message(
                request,
                messages.SUCCESS,
                _(f"Assignment of {assignment.supervisor} has been validated."),  # noqa: INT001
            )
        elif decision == "rejected":
            assignment.validated = False
            messages.add_message(
                request,
                messages.SUCCESS,
                _(f"Assignment of {assignment.supervisor} has been rejected."),  # noqa: INT001
            )
        else:
            raise Http404(_("Wrong decision."))
        assignment.request_date = timezone.now().date()
        assignment.save()
        return redirect("tm:proposal", pk=assignment.proposal_id)
    raise PermissionDenied


def validate_expert(request, pk):
    from apps.tm.tasks import send_expert_validation

    master_thesis = get_object_or_404(MasterThesis, pk=pk)
    if request.user.is_mh and master_thesis.attempt.major in request.user.majors.all():
        master_thesis.expert_validation = True
        master_thesis.expert_validation_date = timezone.now().date()
        master_thesis.save()
        expert = master_thesis.expert
        if not expert.is_expert:
            expert.is_expert = True
            expert.save()
        transaction.on_commit(lambda: send_expert_validation.delay(master_thesis.id))
        messages.add_message(
            request,
            messages.SUCCESS,
            _(
                f"{master_thesis.expert} is confirmed as expert for {master_thesis.attempt.student}'s master's thesis.",  # noqa: E501, INT001
            ),
        )

        return redirect("tm:theses")
    raise PermissionDenied


@login_required
@permission_required("tm.can_cancel_defense", raise_exception=True)
def cancel_defense(request, pk):
    defense = get_object_or_404(Defense, pk=pk)
    thesis_id = defense.master_thesis.id
    if defense.master_thesis.attempt.major in request.user.majors.all():
        if not defense.confirmed:
            # Wa can just delete the defense, nobody received an invitation
            student = defense.master_thesis.attempt.student.name
            defense.delete()
            messages.add_message(
                request,
                messages.SUCCESS,
                _(
                    f"{student}'s Master's thesis defense was canceled. No notification was sent.",  # noqa: E501, INT001
                ),
            )
        else:
            # We need to send a cancellation for the defense (event)
            # transaction.on_commit(lambda: send_expert_validation.delay(master_thesis.id))  # noqa: E501, ERA001
            student = defense.master_thesis.attempt.student.name
            messages.add_message(
                request,
                messages.WARNING,
                _(
                    f"{student}'s Master's thesis defense was NOT canceled. Invitation was already sent to the participants.",  # noqa: E501, INT001
                ),
            )
        return redirect(reverse("tm:view-thesis", args=[thesis_id]))
    raise PermissionDenied


def send_reminder_experts(request, pk=None):
    if pk:
        # We send a reminder for a specific attempt
        from apps.tm.tasks import send_notification_expert

        master_thesis = get_object_or_404(MasterThesis, pk=pk)
        if not master_thesis.expert:
            expert_deadline = master_thesis.attempt.current_calendar.expert_deadline
            send_notification_expert.delay(pk, expert_deadline, reminder=True)
            messages.add_message(
                request,
                messages.SUCCESS,
                _("The reminder was sent to the supervisor."),
            )
    else:
        # We send a reminder for all attempts
        from apps.tm.tasks import send_notification_expert_all

        send_notification_expert_all.delay()
        messages.add_message(
            request,
            messages.SUCCESS,
            _("Reminders were sent for all missing experts."),
        )
    return redirect("tm:theses")


def send_reminder_defenses(request, pk=None):
    if pk:
        # We send a reminder for a specific attempt
        from apps.tm.tasks import send_notification_defense

        master_thesis = get_object_or_404(MasterThesis, pk=pk)
        if not master_thesis.defenses.exists():
            send_notification_defense.delay(pk, reminder=True)
            messages.add_message(
                request,
                messages.SUCCESS,
                _("The reminder was sent to the supervisor."),
            )
    else:
        # We send a reminder for all attempts
        from apps.tm.tasks import send_notification_defense_all

        send_notification_defense_all.delay()
        messages.add_message(
            request,
            messages.SUCCESS,
            _("Reminders were sent for all missing experts."),
        )
    return redirect("tm:theses")


def send_defense_invitations(request):
    # We send an invitation for all defenses
    from apps.tm.tasks import cron_send_defense_invitations

    cron_send_defense_invitations.delay()
    messages.add_message(
        request,
        messages.SUCCESS,
        _("Invitations where sent for all defenses."),
    )
    return redirect("tm:theses")


def send_reminder_defense_documents(request, pk):
    # We send an invitation for all defenses
    from apps.tm.tasks import reminder_defense_documents

    reminder_defense_documents.delay(pk)
    messages.add_message(
        request,
        messages.SUCCESS,
        _("Reminder(s) was/were sent for the missing document(s)."),
    )
    return redirect("tm:defenses")


def send_reminder_expert_profile(request, user_id):
    from apps.users.tasks import send_expert_welcome_message

    user = User.objects.get(pk=user_id)
    send_expert_welcome_message.delay(user.id, request.user.id, reminder=True)

    messages.add_message(
        request,
        messages.SUCCESS,
        _(f"A reminder was sent to {user.name} on your behalf."),  # noqa: INT001
    )
    return redirect("tm:theses")


def receive_hard_copies(request, pk):
    master_thesis = get_object_or_404(MasterThesis, pk=pk)
    if request.user.is_mh and master_thesis.attempt.major in request.user.majors.all():
        master_thesis.hard_copies_received = True
        master_thesis.hard_copies_received_date = timezone.now()
        master_thesis.hard_copies_received_user = request.user
        master_thesis.save()
        messages.add_message(
            request,
            messages.SUCCESS,
            _("Hard copies reception confirmed for {}.").format(master_thesis.title),
        )
        return redirect("tm:theses")
    raise PermissionDenied


def get_pdf_publishing_authorization(request, pk):
    master_thesis = get_object_or_404(MasterThesis, pk=pk)
    if (
        request.user.is_mh and master_thesis.attempt.major in request.user.majors.all()
    ) or (request.user == master_thesis.attempt.student):
        pdf = fill_publishing_authorization(master_thesis.id)
        filename = f"MScBA_P.{master_thesis.id}_{master_thesis.attempt.major.code}_{master_thesis.attempt.session.name}_Thesis_{master_thesis.attempt.student}_consent_to_publish.pdf"  # noqa: E501

        response = HttpResponse(pdf, content_type="application/pdf")
        response["Content-Disposition"] = 'attachment; filename="' + filename + '"'
        return response
    raise PermissionDenied


def get_pdf_confidentiality_agreement(request, pk):
    master_thesis = get_object_or_404(MasterThesis, pk=pk)
    if (
        request.user.is_mh and master_thesis.attempt.major in request.user.majors.all()
    ) or (request.user == master_thesis.attempt.student):
        pdf = fill_confidentiality_agreement(master_thesis.id)
        filename = f"MScBA_C.{master_thesis.id}_{master_thesis.attempt.major.code}_{master_thesis.attempt.session.name}_Thesis_{master_thesis.attempt.student}_confidentiality_agreement.pdf"  # noqa: E501

        response = HttpResponse(pdf, content_type="application/pdf")
        response["Content-Disposition"] = 'attachment; filename="' + filename + '"'
        return response
    raise PermissionDenied


def get_pdf_statement_of_fees(request, pk):
    defense = get_object_or_404(Defense, pk=pk)
    master_thesis = defense.master_thesis
    if (
        request.user.is_mh and master_thesis.attempt.major in request.user.majors.all()
    ) or (request.user == master_thesis.expert):
        pdf = fill_statement_of_fees(defense.id)
        filename = f"MScBA_F.{defense.id}_{defense.master_thesis.attempt.major.code}_{defense.master_thesis.attempt.session.name}_Defense_{defense.defense_date.strftime('%Y-%m-%d')}_{defense.master_thesis.attempt.student}_{defense.master_thesis.expert}_statement_of_fees.pdf"  # noqa: E501

        response = HttpResponse(pdf, content_type="application/pdf")
        response["Content-Disposition"] = 'attachment; filename="' + filename + '"'
        return response
    raise PermissionDenied


def get_pdf_thesis_assessment(request, pk):
    defense = get_object_or_404(Defense, pk=pk)
    master_thesis = defense.master_thesis
    if (
        request.user.is_mh
        and master_thesis.attempt.major in request.user.majors.all()
        or request.user in (master_thesis.supervisor, master_thesis.expert)
    ):
        pdf = fill_thesis_assessment(defense.id)
        filename = f"MScBA_A.{defense.id}_{defense.master_thesis.attempt.major.code}_{defense.master_thesis.attempt.session.name}_Defense_{defense.defense_date.strftime('%Y-%m-%d')}_{defense.master_thesis.attempt.student}_ASSESSMENT.pdf"  # noqa: E501

        response = HttpResponse(pdf, content_type="application/pdf")
        response["Content-Disposition"] = 'attachment; filename="' + filename + '"'
        return response
    raise PermissionDenied


def submit_remediation_objectives(request, pk):
    defense = get_object_or_404(Defense, pk=pk)

    if request.user == defense.master_thesis.supervisor or request.user.is_mh:
        defense.assessment_submitted = True
        defense.assessment_submission_date = timezone.now()
        defense.save()
        messages.add_message(
            request,
            messages.SUCCESS,
            _(
                "The remediation objectives are submitted and will be validated shortly by the TM Manager. Once validated, you will receive a message with the remediation procedure.",  # noqa: E501
            ),
        )

        return redirect("tm:theses")
    raise PermissionDenied


def excel_master_theses(request):  # noqa: C901, PLR0912, PLR0915
    session = current_session(request)

    if request.user.has_perm("tm.can_view_all_theses"):
        theses = (
            MasterThesis.objects.filter(attempt__session=session)
            .select_related("attempt", "supervisor", "expert")
            .order_by("attempt__major", "attempt__student")
        )
    elif request.user.is_mh:
        theses = (
            MasterThesis.objects.filter(attempt__session=session)
            .filter(attempt__major__in=request.user.majors.all())
            .select_related("attempt", "supervisor", "expert")
            .order_by("attempt__major", "attempt__student")
        )
    else:
        theses = MasterThesis.objects.none()

    if theses:
        xlsx_file = BytesIO()

        workbook = xlsxwriter.Workbook(xlsx_file, {"remove_timezone": True})

        bold = workbook.add_format({"bold": True, "bottom": 1})
        title = workbook.add_format({"bold": True, "font_size": 14})
        border = workbook.add_format({"bottom": 1})
        worksheet = workbook.add_worksheet("MScBA TM")
        worksheet.write("A1", "MScBA - Master Thesis")
        worksheet.write("A3", f"Master's Thesis Assessments {session.name}", title)
        worksheet.write("A6", "Student", bold)
        worksheet.write("B6", "Major", bold)
        worksheet.write("C6", "Title", bold)
        worksheet.write("D6", "Supervisor", bold)
        worksheet.write("E6", "Expert", bold)
        worksheet.write("F6", "Defense date", bold)
        worksheet.write("G6", "Grade", bold)
        worksheet.write("H6", "Result", bold)
        row = 6
        col = 0
        for thesis in theses:
            worksheet.write(row, col, thesis.attempt.student.name, border)
            worksheet.write(row, col + 1, thesis.attempt.major.code, border)
            worksheet.write(row, col + 2, thesis.title, border)
            if thesis.supervisor:
                worksheet.write(row, col + 3, thesis.supervisor.name, border)
            else:
                worksheet.write(row, col + 3, None, border)
            if thesis.expert:
                worksheet.write(row, col + 4, thesis.expert.name, border)
            else:
                worksheet.write(row, col + 4, None, border)
            if thesis.defenses.exists():
                if thesis.defenses.last().defense_date:
                    worksheet.write(
                        row,
                        col + 5,
                        thesis.defenses.last().defense_date.strftime("%d.%m.%Y"),
                        border,
                    )
                else:
                    worksheet.write(row, col + 5, _("Not planned"), border)
                if thesis.defenses.last().grade:
                    worksheet.write(row, col + 6, thesis.defenses.last().grade, border)
                else:
                    worksheet.write(row, col + 6, None, border)
            else:
                worksheet.write(row, col + 5, None, border)
                worksheet.write(row, col + 6, None, border)
            if thesis.result:
                worksheet.write(row, col + 7, thesis.get_result_display(), border)
            else:
                worksheet.write(row, col + 7, None, border)
            row += 1
        worksheet.autofit()
        worksheet.set_column(2, 2, 60)
        workbook.close()
        xlsx_file.seek(0)
        response = FileResponse(xlsx_file, content_type="application/vnd.ms-excel")
        response["Content-Disposition"] = (
            'attachment; filename="MScBA_master_theses_assessments.xlsx"'
        )
        return response

    # We return a 403 if the user is not allowed to view theses
    raise PermissionDenied


def toggle_assignment_reminders(request, pk):
    assignment = get_object_or_404(Assignment, pk=pk)
    if (
        request.user.is_mh
        and assignment.proposal.attempt.major in request.user.majors.all()
    ):
        if assignment.reminders:
            assignment.reminders = False
            assignment.save()
            messages.add_message(
                request,
                messages.SUCCESS,
                _(f"Reminders are now disabled for {assignment.supervisor}."),  # noqa: INT001
            )
        else:
            assignment.reminders = True
            assignment.save()
            messages.add_message(
                request,
                messages.SUCCESS,
                _(f"Reminders are now enabled for {assignment.supervisor}."),  # noqa: INT001
            )
        return redirect("tm:proposal", pk=assignment.proposal_id)
    raise PermissionDenied


def add_supervisor(request, pk):
    if request.method == "POST":
        project = get_object_or_404(Project, pk=pk)
        if request.user.is_mh and project.attempt.major in request.user.majors.all():
            form = forms.ProjectCoSupervisorForm(request.POST)
            if form.is_valid():
                co_supervisors = form.cleaned_data["co_supervisors"]
                for supervisor in co_supervisors:
                    project.co_supervisors.add(supervisor)
                messages.add_message(
                    request,
                    messages.SUCCESS,
                    _(
                        f"Co-supervisor(s) {co_supervisors} has/have been added to {project.title}.",  # noqa: E501, INT001
                    ),
                )
                return redirect("tm:disposition", pk=project.id)
            raise Http404(_(f"You need to select a co-supervisor. {form.errors}"))  # noqa: INT001
        raise PermissionDenied
    raise PermissionDenied


def authorization_publish2img(request, pk):
    thesis = get_object_or_404(MasterThesis, pk=pk)
    if request.user.is_mh and thesis.attempt.major in request.user.majors.all():
        if thesis.authorization_publish_file:
            images = convert_from_bytes(
                default_storage.open(thesis.authorization_publish_file.name).read(),
            )
            if images:
                # Get the first image
                image = images[0]
                # Create an in-memory buffer to save the image
                image_buffer = BytesIO()
                image.save(image_buffer, format="PNG")
                image_buffer.seek(0)
                # Return the image as an HTTP response
                return HttpResponse(image_buffer, content_type="image/png")
        msg = "Authorization to publish does not exist for this thesis!"
        raise Http404(msg)
    raise PermissionDenied


class AttemptAutocomplete(PermissionRequiredMixin, autocomplete.Select2QuerySetView):
    permission_required = "tm.can_search_attempt"

    def get_result_label(self, attempt):
        return f"{attempt.student.name} - {attempt.major.code} ({attempt.session.name})"

    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return Attempt.objects.none()

        qs = (
            Attempt.objects.all()
            .filter(major__in=self.request.user.majors.all())
            .order_by("student__name", "major__code", "session__name")
        )

        if self.q:
            qs = qs.filter(student__name__icontains=self.q)

        return qs


class SupervisorAutocomplete(PermissionRequiredMixin, autocomplete.Select2QuerySetView):
    permission_required = "tm.can_search_attempt"

    def get_result_label(self, user):
        return f"{user.name}"

    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return User.objects.none()

        qs = User.objects.all().filter(is_supervisor=True).order_by("name")

        if self.q:
            qs = qs.filter(name__icontains=self.q)

        return qs
