import django_filters

from .models import Attempt


class AttemptsFilter(django_filters.FilterSet):
    class Meta:
        model = Attempt
        fields = ["major"]
