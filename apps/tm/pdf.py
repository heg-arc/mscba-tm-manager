import logging
import re
import subprocess
import tempfile
from io import BytesIO
from pathlib import Path

import fitz
import qrcode
from django.contrib.staticfiles.storage import staticfiles_storage
from django.core.files.base import ContentFile
from django.utils import timezone
from fillpdf import fillpdfs
from PIL import Image
from PIL import ImageEnhance
from pyzbar.pyzbar import ZBarSymbol
from pyzbar.pyzbar import decode

logger = logging.getLogger(__name__)


def stamp_pdf(qr_data, filled_form, stamp_position):
    qr = qrcode.QRCode(
        version=1,
        error_correction=qrcode.constants.ERROR_CORRECT_Q,
        box_size=10,
        border=2,
    )
    qr.add_data(qr_data)
    qr.make(fit=True)

    qr_img = qr.make_image(fill_color="black", back_color="white")
    qr_bytes = BytesIO()
    qr_img.save(qr_bytes, format="PNG")
    qr_bytes.seek(0)

    pdf_document = fitz.open(stream=filled_form, filetype="pdf")
    page = pdf_document.load_page(0)
    page_width = page.rect.width
    page_height = page.rect.height
    qr_size = 70

    if stamp_position == "TL":
        x0 = 45
        y0 = 30
        x1 = 45 + qr_size
        y1 = 30 + qr_size
    elif stamp_position == "TC":
        x0 = page_width / 2 - qr_size / 2
        y0 = 30
        x1 = page_width / 2 + qr_size / 2
        y1 = 30 + qr_size
    elif stamp_position == "TR":
        x0 = page_width - qr_size - 50
        y0 = 30
        x1 = page_width - 50
        y1 = 30 + qr_size
    else:
        # We stamp the qrcode in the middle of the page
        x0 = page_width / 2 - qr_size / 2
        y0 = page_height / 2 - qr_size / 2
        x1 = page_width / 2 + qr_size / 2
        y1 = page_height / 2 + qr_size / 2

    qr_image_rect = fitz.Rect(x0, y0, x1, y1)
    page.insert_image(qr_image_rect, stream=qr_bytes)

    stamped_pdf = BytesIO()
    pdf_document.save(stamped_pdf)
    pdf_document.close()
    stamped_pdf.seek(0)

    return stamped_pdf


def fill_publishing_authorization(thesis_id):
    from .models import MasterThesis

    master_thesis = MasterThesis.objects.get(pk=thesis_id)

    if master_thesis.confidential:
        confidential = "yes"
        publication = "no"
    else:
        confidential = "no"
        publication = "yes"

    supervisor = master_thesis.supervisor.name if master_thesis.supervisor else ""
    if master_thesis.co_supervisors:
        for co_supervisor in master_thesis.co_supervisors.all():
            supervisor += f", {co_supervisor.name}"

    data = {
        "name": master_thesis.attempt.student.name,
        "field_of_study": "Business Administration",
        "major": master_thesis.attempt.major.name,
        "title": master_thesis.title,
        "supervisor": supervisor,
        "confidential": confidential,
        "publication": publication,
        "date": timezone.now().strftime("%d/%m/%Y"),
    }
    filled_form = BytesIO()
    fillpdfs.write_fillable_pdf(
        staticfiles_storage.open("forms/accord_publication_tm_2023.pdf"),
        filled_form,
        data,
    )
    stamped_pdf = stamp_pdf(f"MScBA_P.{master_thesis.id}", filled_form, "TL")
    return stamped_pdf.getvalue()


def fill_statement_of_fees(defense_id):
    from .models import Defense

    defense = Defense.objects.get(pk=defense_id)
    master_thesis = defense.master_thesis

    if defense.place == Defense.OTHER:
        defense_place = defense.other_location
    else:
        defense_place = defense.get_place_display()

    data = {
        "name": master_thesis.expert.name if master_thesis.expert else "",
        "address": master_thesis.expert.address if master_thesis.expert else "",
        "place": (
            f"{master_thesis.expert.zip_code} {master_thesis.expert.city}"
            if master_thesis.expert
            else ""
        ),
        "occupation": master_thesis.expert.current_job if master_thesis.expert else "",
        "field": "Business Administration",
        "student": master_thesis.attempt.student.name,
        "title": master_thesis.title,
        "defense": (
            "{} {}".format(defense.defense_date.strftime("%d/%m/%Y"), defense_place)
            if defense
            else ""
        ),
        "date": (
            "{}, {}".format(
                master_thesis.expert.city,
                timezone.now().strftime("%d/%m/%Y"),
            )
            if defense
            else ""
        ),
    }

    filled_form = BytesIO()
    fillpdfs.write_fillable_pdf(
        staticfiles_storage.open("forms/décompte_honoraire_expert_tm_2023.pdf"),
        filled_form,
        data,
    )
    stamped_pdf = stamp_pdf(f"MScBA_F.{defense.id}", filled_form, "TC")
    return stamped_pdf.getvalue()


def fill_thesis_assessment(defense_id):
    from .models import Defense

    defense = Defense.objects.get(pk=defense_id)
    master_thesis = defense.master_thesis

    supervisor = master_thesis.supervisor.name if master_thesis.supervisor else ""
    if master_thesis.co_supervisors:
        for co_supervisor in master_thesis.co_supervisors.all():
            supervisor += f", {co_supervisor.name}"

    data = {
        "major": master_thesis.attempt.major.name,
        "name": master_thesis.attempt.student.name,
        "title": master_thesis.title,
        "supervisor": supervisor,
        "expert": master_thesis.expert.name if master_thesis.expert else "",
        "date": defense.defense_date.strftime("%d/%m/%Y") if defense else "",
        "date_assessment": defense.defense_date.strftime("%d/%m/%Y") if defense else "",
    }

    filled_form = BytesIO()
    fillpdfs.write_fillable_pdf(
        staticfiles_storage.open("forms/thesis_assessment_tm_2024.pdf"),
        filled_form,
        data,
    )
    stamped_pdf = stamp_pdf(f"MScBA_A.{defense.id}", filled_form, "TR")
    return stamped_pdf.getvalue()


def fill_confidentiality_agreement(thesis_id):
    from .models import MasterThesis

    master_thesis = MasterThesis.objects.get(pk=thesis_id)

    data = {
        "student_name": master_thesis.attempt.student.name,
        "student_date": timezone.now().strftime("%d/%m/%Y"),
        "supervisor_name": (
            master_thesis.supervisor.name if master_thesis.supervisor else ""
        ),
        "supervisor_address": (
            master_thesis.supervisor.address_line if master_thesis.supervisor else ""
        ),
        "expert_name": master_thesis.expert.name if master_thesis.expert else "",
        "expert_address": (
            master_thesis.expert.address_line if master_thesis.expert else ""
        ),
        "title": master_thesis.title,
    }
    filled_form = BytesIO()
    fillpdfs.write_fillable_pdf(
        staticfiles_storage.open("forms/accord_confidentialite_tm_2024.pdf"),
        filled_form,
        data,
    )
    stamped_pdf = stamp_pdf(f"MScBA_C.{master_thesis.id}", filled_form, "TL")
    return stamped_pdf.getvalue()


def flatten_pdf(pdf_file):
    # Use temporary files to handle the pdfcairo processing
    with (
        tempfile.NamedTemporaryFile(
            suffix=".pdf",
        ) as input_temp_file,
        tempfile.NamedTemporaryFile(
            suffix=".pdf",
        ) as output_temp_file,
    ):
        # Write input PDF bytes to the temporary input file
        pdf_bytes = pdf_file.read()
        input_temp_file.write(pdf_bytes)
        input_temp_file.flush()

        # Run pdftocairo to flatten the PDF
        try:
            subprocess.run(  # noqa: S603
                ["pdftocairo", "-pdf", input_temp_file.name, output_temp_file.name],  # noqa: S607
                check=True,
            )

            # Read the flattened output from the temporary file into a BytesIO object
            with Path.open(output_temp_file.name, "rb") as f:
                flattened_pdf_bytes = f.read()
                flattened_pdf_stream = BytesIO(flattened_pdf_bytes)

        except subprocess.CalledProcessError as e:
            msg = f"Error: Failed to flatten the PDF with pdfcairo. {e}"
            logger.exception(msg)
            return None
        else:
            logger.info("PDF successfully flattened using pdfcairo.")
            return flattened_pdf_stream


def extract_qr_text_from_image(image_data):
    """Detect and extract QR code text from an image."""
    image = Image.open(BytesIO(image_data))
    x, y = image.size
    # Depending on the resolution of the image, we may need to scale it down
    for scalar in [1, 0.5, 0.2, 2]:
        image_scaled = image.resize((int(round(x * scalar)), int(round(y * scalar))))
        image_scaled_sharp = ImageEnhance.Sharpness(image_scaled).enhance(
            2,
        )  # Enhance sharpness, most effective
        qr_codes = decode(image_scaled_sharp, symbols=[ZBarSymbol.QRCODE])
        if qr_codes:
            for qr in qr_codes:
                msg = f"QR decoding successful with scalar={scalar}."
                logger.info(msg)
                return qr.data.decode("utf-8")
    return None


def save_pages_as_pdf(doc, start_page, end_page, qr_code_text, full_document=False):  # noqa: C901, FBT002, PLR0912, PLR0915
    """Save specified range of pages from the PDF document as a new PDF file."""
    from .models import Defense
    from .models import MasterThesis

    response = f"<span class='text-danger'>Error: Invalid QR code format ({qr_code_text}).</span>"  # noqa: E501

    # First, we decode filename to get the type and id of the document
    file_name = qr_code_text.split("_")
    if len(file_name) >= 2:  # noqa: PLR2004
        if file_name[0] == "MScBA":
            if re.fullmatch(r"[PAF]\.\d*", file_name[1]):
                document = file_name[1].split(".")
                document_name = document[0]
                document_id = document[1]

                if full_document:
                    # Directly save the original document as it's the whole document
                    pdf_file = doc
                else:
                    # Create a new PDF file with specified range
                    output_pdf = fitz.open()  # Create a new blank PDF document
                    for page_num in range(start_page, end_page):
                        output_pdf.insert_pdf(doc, from_page=page_num, to_page=page_num)
                    pdf_bytes = BytesIO()
                    output_pdf.save(pdf_bytes)
                    output_pdf.close()
                    pdf_bytes.seek(0)
                    pdf_file = ContentFile(pdf_bytes.read())

                pdf_file_name = f"{qr_code_text}.pdf"

                if document_name == "P":
                    try:
                        thesis = MasterThesis.objects.get(pk=document_id)
                    except MasterThesis.DoesNotExist:
                        thesis = None
                    if thesis:
                        thesis.authorization_publish_file.save(
                            pdf_file_name,
                            pdf_file,
                            save=True,
                        )
                        response = f"Publishing agreement for {thesis.attempt.student} uploaded."  # noqa: E501
                    else:
                        response = f"<span class='text-danger'>Error: Publishing agreement for thesis with ID {document_id} not found.</span>"  # noqa: E501

                elif document_name == "A":
                    try:
                        defense = Defense.objects.get(pk=document_id)
                    except Defense.DoesNotExist:
                        defense = None
                    if defense:
                        defense.assessment_file.save(pdf_file_name, pdf_file, save=True)
                        response = f"Assessment for {defense.master_thesis.attempt.student} uploaded."  # noqa: E501
                    else:
                        response = f"<span class='text-danger'>Error: Assessment for defense with ID {document_id} not found.</span>"  # noqa: E501

                elif document_name == "F":
                    try:
                        defense = Defense.objects.get(pk=document_id)
                    except Defense.DoesNotExist:
                        defense = None
                    if defense:
                        defense.statement_of_fees_file.save(
                            pdf_file_name,
                            pdf_file,
                            save=True,
                        )
                        response = f"Statement of fees for {defense.master_thesis.attempt.student} uploaded."  # noqa: E501
                    else:
                        response = f"<span class='text-danger'>Error: Statement of fees for defense with ID {document_id} not found.</span>"  # noqa: E501

    return f"<li>{response}</li>"


def split_and_save_pdf_scans(pdf_file):
    # We have to first flatten the PDF to protect filled form data
    # In the process, we loose the electronic signature certificate
    flattened_pdf = flatten_pdf(pdf_file)
    if not flattened_pdf:
        return "<li><span class='text-danger'>Error: Failed to flatten the PDF.</span></li>"  # noqa: E501

    doc = fitz.open(stream=flattened_pdf, filetype="pdf")
    total_pages = doc.page_count
    start_page = 0
    qr_code_filename = None
    imported = []

    for i in range(total_pages):
        page = doc.load_page(i)

        # Convert page to an image
        pix = page.get_pixmap()
        img_data = pix.tobytes("png")  # Get image data as PNG

        # Check for QR code in the image data
        qr_text = extract_qr_text_from_image(img_data)

        if qr_text:
            # Found a QR code, save the previous section
            if qr_code_filename:
                document = save_pages_as_pdf(
                    doc,
                    start_page,
                    i,
                    qr_code_filename,
                    full_document=False,
                )
                imported.append(document)

            # Update start_page and qr_code_filename for the next section
            start_page = i
            qr_code_filename = qr_text

    # Save the last section if any
    if qr_code_filename:
        # Check if start_page and end_page cover the whole document
        if start_page == 0:
            # This is the only section, save the *original* document
            # so we conserve the electronic signature certificate
            document = save_pages_as_pdf(
                pdf_file,
                start_page,
                total_pages,
                qr_code_filename,
                full_document=True,
            )
        else:
            document = save_pages_as_pdf(
                doc,
                start_page,
                total_pages,
                qr_code_filename,
                full_document=False,
            )
        imported.append(document)

    doc.close()

    if len(imported) == 0:
        imported.append(
            "<li><span class='text-danger'>Error: No valid QR code found in the document.</span></li>",  # noqa: E501
        )
    return "".join(imported)
