from django import template
from libgravatar import Gravatar

register = template.Library()


@register.filter(name="gravatar")
def gravatar_url(value):
    g = Gravatar(value)
    return g.get_image(default="retro")
