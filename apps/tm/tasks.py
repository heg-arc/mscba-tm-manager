import logging
import time
import zipfile
from datetime import date
from datetime import datetime
from datetime import timedelta
from io import BytesIO

from celery_progress.backend import ProgressRecorder
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.sites.models import Site
from django.core.files.storage import default_storage
from django.db.models.fields.related import ManyToOneRel
from django.urls import reverse
from django.utils import timezone
from django.utils import translation
from django.utils.translation import gettext_lazy as _
from icalendar import Calendar
from icalendar import Event
from icalendar import vCalAddress
from icalendar import vText
from sesame.utils import get_query_string
from templated_email import get_templated_mail
from templated_email import send_templated_mail

from config import celery_app

from .pdf import fill_confidentiality_agreement
from .pdf import fill_publishing_authorization
from .pdf import fill_statement_of_fees
from .pdf import fill_thesis_assessment

logger = logging.getLogger(__name__)

User = get_user_model()


@celery_app.task()
def send_notification_validated(assignment_id, reminder=False):  # noqa: FBT002
    from .models import Assignment

    current_site = Site.objects.get_current()
    assignment = Assignment.objects.get(pk=assignment_id)

    if assignment.supervisor and assignment.supervisor.language:
        translation.activate(assignment.supervisor.language)
    else:
        translation.activate("fr")

    academic_group = assignment.proposal.attempt.major.academic_group
    cc = [user.email for user in academic_group.user_set.all()]

    send_templated_mail(
        template_name="assignment_notification",
        from_email=settings.DEFAULT_FROM_EMAIL,
        recipient_list=[assignment.supervisor.email],
        cc=cc,
        context={
            "assignment": assignment,
            "reminder": reminder,
            "url": f"https://{current_site.domain}{assignment.proposal.get_absolute_url()}",
        },
    )

    assignment.last_reminder = timezone.now().date()
    assignment.save()


@celery_app.task()
def send_notification_decision(assignment_id):
    from .models import Assignment

    current_site = Site.objects.get_current()
    assignment = Assignment.objects.get(pk=assignment_id)

    if assignment.proposal.attempt.student.language:
        translation.activate(assignment.proposal.attempt.student.language)
    else:
        translation.activate("fr")

    cc = [assignment.supervisor.email]

    academic_group = assignment.proposal.attempt.major.academic_group
    for user in academic_group.user_set.all():
        cc.append(user.email)  # noqa: PERF401

    # We remove duplicates emails
    cc = list(set(cc))

    send_templated_mail(
        template_name="assignment_decision_notification",
        from_email=settings.DEFAULT_FROM_EMAIL,
        recipient_list=[assignment.proposal.attempt.student.email],
        cc=cc,
        context={
            "assignment": assignment,
            "url": f"https://{current_site.domain}{assignment.proposal.get_absolute_url()}",
        },
    )


@celery_app.task()
def send_notification_archive(assignment_id, supervisor=False):  # noqa: FBT002
    from .models import Assignment

    current_site = Site.objects.get_current()
    assignment = Assignment.objects.get(pk=assignment_id)

    if assignment.supervisor.language:
        translation.activate(assignment.supervisor.language)
    else:
        translation.activate("fr")

    academic_group = assignment.proposal.attempt.major.academic_group
    cc = [user.email for user in academic_group.user_set.all()]

    send_templated_mail(
        template_name="assignment_decision_archive",
        from_email=settings.DEFAULT_FROM_EMAIL,
        recipient_list=[assignment.supervisor.email],
        cc=cc,
        context={
            "assignment": assignment,
            "supervisor": supervisor,
            "url": f"https://{current_site.domain}{assignment.proposal.get_absolute_url()}",
        },
    )


@celery_app.task()
def send_notification_workflow(workflow_id):  # noqa: C901
    from .models import ProposalWorkflow

    current_site = Site.objects.get_current()
    workflow = ProposalWorkflow.objects.get(pk=workflow_id)

    if workflow.proposal.attempt.student.language:
        translation.activate(workflow.proposal.attempt.student.language)
    else:
        translation.activate("fr")

    recipient = workflow.proposal.attempt.student
    cc = []

    if workflow.proposal.supervisor:
        cc.append(workflow.proposal.supervisor.email)

    academic_group = workflow.proposal.attempt.major.academic_group
    for user in academic_group.user_set.all():
        cc.append(user.email)  # noqa: PERF401

    if workflow.status == ProposalWorkflow.ACCEPTED:
        notification = "accepted"
    elif workflow.status == ProposalWorkflow.REJECTED:
        notification = "rejected"
    elif workflow.status == ProposalWorkflow.REWORK:
        notification = "rework"
    elif workflow.status == ProposalWorkflow.COMMENT:
        notification = "comment"
    elif workflow.status == ProposalWorkflow.SUBMITTED:
        notification = "submitted"
        recipient = workflow.proposal.attempt.major.coordinator
    elif workflow.status == ProposalWorkflow.WITHDRAWN:
        notification = "withdrawn"
    else:
        notification = None

    cc = list(set(cc))

    if notification:
        send_templated_mail(
            template_name="proposal_workflow",
            from_email=settings.DEFAULT_FROM_EMAIL,
            recipient_list=[recipient.email],
            cc=cc,
            context={
                "workflow": workflow,
                "notification": notification,
                "recipient": recipient,
                "url": f"https://{current_site.domain}{workflow.proposal.get_absolute_url()}",
            },
        )


@celery_app.task()
def send_notification_disposition(project_id):
    from .models import Project

    current_site = Site.objects.get_current()
    project = Project.objects.get(pk=project_id)

    if project.attempt.student.language:
        translation.activate(project.attempt.student.language)
    else:
        translation.activate("fr")

    cc = [project.supervisor.email]
    if project.co_supervisors:
        for co_supervisor in project.co_supervisors.all():
            cc.append(co_supervisor.email)  # noqa: PERF401
    # We add the graders of the proposal (content and methodology)
    if project.reviewer_content:
        cc.append(project.reviewer_content.email)
    if project.reviewer_methodology:
        cc.append(project.reviewer_methodology.email)
    # We add MH and secretary
    academic_group = project.attempt.major.academic_group
    for user in academic_group.user_set.all():
        cc.append(user.email)  # noqa: PERF401
    secretary = project.attempt.major.secretary
    if secretary:
        cc.append(secretary.email)
    # We remove duplicates emails
    cc = list(set(cc))

    send_templated_mail(
        template_name="disposition_decision_notification",
        from_email=settings.DEFAULT_FROM_EMAIL,
        recipient_list=[project.attempt.student.email],
        cc=cc,
        context={
            "project": project,
            "url": f"https://{current_site.domain}{project.get_absolute_url()}",
        },
    )


def get_all_leaves_from_attempt(attempt_id):
    from .models import PersonalLeave

    # We retrieve all leaves from the attempt
    leaves = PersonalLeave.objects.filter(attempt_id=attempt_id).order_by(
        "first_day_leave",
    )
    all_leaves = []
    last_leave = None
    for leave in leaves:
        if not last_leave:
            # This is the first leave, we add it to the list
            effective_from = leave.first_day_leave
            effective_to = leave.last_day_leave
        elif leave.first_day_leave > last_leave.last_day_leave:
            # There is no overlap between the leaves, we take all days into account
            effective_from = leave.first_day_leave
            effective_to = leave.last_day_leave
        elif (
            leave.first_day_leave
            <= last_leave.last_day_leave + timedelta(days=1)
            <= leave.last_day_leave
        ):
            # There is an overlap, we only take into account the "new" days of leave
            effective_from = last_leave.last_day_leave + timedelta(days=1)
            effective_to = leave.last_day_leave
        else:
            # The leave is shorter than the last_leave, we do nothing
            effective_from = None
            effective_to = None

        if effective_from and effective_to:
            effective_duration = effective_to - effective_from + timedelta(days=1)
            leave.effective_days = effective_duration.days
            leave.save()
            all_leaves.append(
                {
                    "effective_from": effective_from,
                    "effective_to": effective_to,
                    "effective_days": leave.effective_days,
                    "leave": leave,
                },
            )
            last_leave = leave
        else:
            leave.effective_days = 0
            leave.save()
    return all_leaves


def update_calendar_with_leaves(current_calendar, all_leaves):
    """Update the calendar based on the personal leave effective duration."""
    date_fields = [
        "proposal_start",
        "proposal_end",
        "proposal_rework_start",
        "proposal_rework_end",
        "project_start",
        "project_end",
        "project_rework_start",
        "project_rework_end",
        "report_start",
        "report_end",
        "thesis_start",
        "thesis_end",
        "thesis_rework_start",
        "thesis_rework_end",
        "defense_start",
        "defense_end",
        "defense_rework_start",
        "defense_rework_end",
    ]

    for leave_dict in all_leaves:
        for field in date_fields:
            calendar_value = getattr(current_calendar, field)
            if isinstance(calendar_value, datetime):
                if calendar_value.date() >= leave_dict["effective_from"]:
                    new_value = calendar_value + timedelta(
                        days=leave_dict["effective_days"],
                    )
                    setattr(current_calendar, field, new_value)
            elif isinstance(calendar_value, date):
                if calendar_value >= leave_dict["effective_from"]:
                    new_value = calendar_value + timedelta(
                        days=leave_dict["effective_days"],
                    )
                    setattr(current_calendar, field, new_value)
    current_calendar.save()


@celery_app.task()
def update_calendar(personal_leave_id):
    from .models import PersonalLeave

    personal_leave = PersonalLeave.objects.get(pk=personal_leave_id)
    attempt = personal_leave.attempt
    all_leaves = get_all_leaves_from_attempt(attempt.id)
    original_calendar = attempt.session.calendar
    current_calendar = attempt.current_calendar

    if attempt.student.language:
        translation.activate(attempt.student.language)
    else:
        translation.activate("fr")
    current_site = Site.objects.get_current()

    if original_calendar == current_calendar:
        # There is no custom calendar, we need to create a new one for the attempt
        current_calendar.pk = None
        current_calendar.custom = True
        current_calendar.label = f"{attempt.student} - {attempt.session}"
        current_calendar.save()
        attempt.calendar_id = current_calendar.id
        attempt.save()
    else:
        # We reset the current_calendar with the values of the original calendar
        fields = [
            f.name
            for f in current_calendar._meta.get_fields()  # noqa: SLF001
            if f.name not in ["id", "custom", "label", "last_update"]
            and not isinstance(f, ManyToOneRel)
        ]
        for field in fields:
            setattr(current_calendar, field, getattr(original_calendar, field))
        current_calendar.save()

    update_calendar_with_leaves(current_calendar, all_leaves)

    cc = [personal_leave.editor.email]
    if attempt.supervisor:
        cc.append(attempt.supervisor.email)

    administration_group = personal_leave.attempt.major.admin_group
    for user in administration_group.user_set.all():
        cc.append(user.email)  # noqa: PERF401

    academic_group = personal_leave.attempt.major.academic_group
    for user in academic_group.user_set.all():
        cc.append(user.email)  # noqa: PERF401

    secretary = personal_leave.attempt.major.secretary
    if secretary:
        cc.append(secretary.email)

    # We remove duplicates emails
    cc = list(set(cc))

    send_templated_mail(
        template_name="personal_leave_notification",
        from_email=settings.DEFAULT_FROM_EMAIL,
        recipient_list=[attempt.student.email],
        cc=cc,
        context={
            "personal_leave": personal_leave,
            "calendar": current_calendar,
            "url": f"https://{current_site.domain}{current_calendar.get_absolute_url()}",
            "url_student": f"https://{current_site.domain}{attempt.get_absolute_url()}",
        },
    )


@celery_app.task()
def delete_personal_leave(  # noqa: PLR0913
    attempt_id,
    type_of_leave,
    first_day_leave,
    last_day_leave,
    basis,
    user_id,
):
    from .models import Attempt

    user = User.objects.get(pk=user_id)
    attempt = Attempt.objects.get(pk=attempt_id)
    all_leaves = get_all_leaves_from_attempt(attempt.id)
    original_calendar = attempt.session.calendar
    current_calendar = attempt.current_calendar

    if attempt.student.language:
        translation.activate(attempt.student.language)
    else:
        translation.activate("fr")
    current_site = Site.objects.get_current()

    if original_calendar == current_calendar:
        # There is no custom calendar, we need to create a new one for the attempt
        current_calendar.pk = None
        current_calendar.custom = True
        current_calendar.label = f"{attempt.student} - {attempt.session}"
        current_calendar.save()
        attempt.calendar_id = current_calendar.id
        attempt.save()
    else:
        # We reset the current_calendar with the values of the original calendar
        fields = [
            f.name
            for f in current_calendar._meta.get_fields()  # noqa: SLF001
            if f.name not in ["id", "custom", "label", "last_update"]
            and not isinstance(f, ManyToOneRel)
        ]
        for field in fields:
            setattr(current_calendar, field, getattr(original_calendar, field))
        current_calendar.save()

    update_calendar_with_leaves(current_calendar, all_leaves)

    cc = [user.email]
    if attempt.supervisor:
        cc.append(attempt.supervisor.email)

    administration_group = attempt.major.admin_group
    for user in administration_group.user_set.all():
        cc.append(user.email)  # noqa: PERF401

    academic_group = attempt.major.academic_group
    for user in academic_group.user_set.all():
        cc.append(user.email)  # noqa: PERF401

    secretary = attempt.major.secretary
    if secretary:
        cc.append(secretary.email)

    # We remove duplicates emails
    cc = list(set(cc))

    send_templated_mail(
        template_name="personal_leave_deletion_notification",
        from_email=settings.DEFAULT_FROM_EMAIL,
        recipient_list=[attempt.student.email],
        cc=cc,
        context={
            "attempt": attempt,
            "type_of_leave": type_of_leave,
            "first_day_leave": first_day_leave,
            "last_day_leave": last_day_leave,
            "basis": basis,
            "user": user,
            "calendar": current_calendar,
            "url": f"https://{current_site.domain}{current_calendar.get_absolute_url()}",
            "url_student": f"https://{current_site.domain}{attempt.get_absolute_url()}",
        },
    )


@celery_app.task()
def cron_send_notification_report():
    from .models import Attempt
    from .models import MasterThesis

    current_attempts = Attempt.objects.select_related("calendar").filter(done=False)
    for attempt in current_attempts:
        if attempt.current_calendar.report_start == timezone.now().date():
            send_notification_report.delay(
                attempt.id,
                reminder=False,
                student=True,
                supervisor=True,
            )
        elif (
            attempt.current_calendar.report_end.date()
            == timezone.now().date() + timedelta(days=6)
        ) or (
            attempt.current_calendar.report_end.date() + timedelta(days=2)
            >= timezone.now().date()
            > attempt.current_calendar.report_end.date() - timedelta(days=1)
        ):
            thesis = attempt.theses.filter(result=MasterThesis.NOT_ASSESSED).first()
            if thesis:
                student = True
                supervisor = True

                if thesis.student_report:
                    if thesis.student_report.submitted:
                        student = False
                if thesis.supervisor_report:
                    if thesis.supervisor_report.submitted:
                        supervisor = False

                send_notification_report.delay(
                    attempt.id,
                    reminder=True,
                    student=student,
                    supervisor=supervisor,
                )


@celery_app.task()
def send_notification_all_report():
    from .models import Attempt
    from .models import MasterThesis

    current_attempts = Attempt.objects.select_related("calendar").filter(done=False)
    for attempt in current_attempts:
        thesis = attempt.theses.filter(result=MasterThesis.NOT_ASSESSED).first()
        if thesis:
            student = True
            supervisor = True

            if thesis.student_report:
                if thesis.student_report.submitted:
                    student = False
            if thesis.supervisor_report:
                if thesis.supervisor_report.submitted:
                    supervisor = False

            send_notification_report.delay(
                attempt.id,
                reminder=True,
                student=student,
                supervisor=supervisor,
            )


@celery_app.task()
def send_notification_report(attempt_id, reminder=False, student=True, supervisor=True):  # noqa: FBT002
    from .models import Attempt

    current_site = Site.objects.get_current()
    attempt = Attempt.objects.get(pk=attempt_id)

    if attempt.student.language:
        translation.activate(attempt.student.language)
    else:
        translation.activate("fr")

    report_url = reverse("tm:add-progress-report", kwargs={"pk": attempt.id})

    if student and supervisor:
        to = [attempt.student.email, attempt.supervisor.email]
        if attempt.co_supervisors:
            cc = [co_supervisor.email for co_supervisor in attempt.co_supervisors.all()]

    elif student:
        to = [attempt.student.email]
        cc = [attempt.supervisor.email]
    elif supervisor:
        to = [attempt.supervisor.email]
        cc = []
        if attempt.co_supervisors:
            for co_supervisor in attempt.co_supervisors.all():
                cc.append(co_supervisor.email)

    send_templated_mail(
        template_name="report_notification",
        from_email=settings.DEFAULT_FROM_EMAIL,
        recipient_list=to,
        cc=cc,
        context={
            "attempt": attempt,
            "reminder": reminder,
            "student": student,
            "supervisor": supervisor,
            "url_report": f"https://{current_site.domain}{report_url}",
            "url_student": f"https://{current_site.domain}{attempt.get_absolute_url()}",
        },
    )


@celery_app.task()
def overridden_notification_report(progress_report_id):
    from .models import ProgressReport

    current_site = Site.objects.get_current()
    report = ProgressReport.objects.get(pk=progress_report_id)

    if report.user.language:
        translation.activate(report.user.language)
    else:
        translation.activate("fr")

    report_url = report.get_absolute_url()

    to = [report.user.email]
    cc = [report.forced_submission_user.email]

    academic_group = report.master_thesis.attempt.major.academic_group
    for user in academic_group.user_set.all():
        cc.append(user.email)  # noqa: PERF401

    # We remove duplicates emails
    cc = list(set(cc))

    send_templated_mail(
        template_name="report_overridden_notification",
        from_email=settings.DEFAULT_FROM_EMAIL,
        recipient_list=to,
        cc=cc,
        context={
            "report": report,
            "attempt": report.master_thesis.attempt,
            "url_report": f"https://{current_site.domain}{report_url}",
        },
    )


@celery_app.task()
def cron_send_notification_expert():
    from .models import Attempt
    from .models import MasterThesis

    current_attempts = Attempt.objects.select_related("calendar").filter(done=False)
    for attempt in current_attempts:
        expert_deadline = attempt.current_calendar.expert_deadline
        if (
            expert_deadline == timezone.now().date() + timedelta(days=30)
            or (expert_deadline == timezone.now().date() + timedelta(days=7))
            or (
                expert_deadline + timedelta(days=2)
                >= timezone.now().date()
                > expert_deadline - timedelta(days=1)
            )
        ):
            thesis = attempt.theses.filter(result=MasterThesis.NOT_ASSESSED).first()
            if thesis:
                if not thesis.expert:
                    send_notification_expert.delay(thesis.id, expert_deadline)


@celery_app.task()
def send_notification_expert_all():
    from .models import Attempt
    from .models import MasterThesis

    current_attempts = Attempt.objects.select_related("calendar").filter(done=False)
    for attempt in current_attempts:
        expert_deadline = attempt.current_calendar.expert_deadline
        thesis = attempt.theses.filter(result=MasterThesis.NOT_ASSESSED).first()

        if thesis:
            if not thesis.expert:
                send_notification_expert.delay(thesis.id, expert_deadline)


@celery_app.task()
def send_notification_expert(thesis_id, expert_deadline):
    from .models import MasterThesis

    current_site = Site.objects.get_current()
    master_thesis = MasterThesis.objects.get(pk=thesis_id)

    if master_thesis.attempt.student.language:
        translation.activate(master_thesis.attempt.student.language)
    else:
        translation.activate("fr")

    if expert_deadline <= timezone.now().date() + timedelta(days=7):
        reminder = True
    else:
        reminder = False

    cc = []
    if master_thesis.co_supervisors:
        for co_supervisor in master_thesis.co_supervisors.all():
            cc.append(co_supervisor.email)  # noqa: PERF401

    send_templated_mail(
        template_name="expert_notification",
        from_email=settings.DEFAULT_FROM_EMAIL,
        recipient_list=[master_thesis.supervisor.email],
        cc=cc,
        context={
            "master_thesis": master_thesis,
            "expert_deadline": expert_deadline,
            "reminder": reminder,
            "url": f"https://{current_site.domain}{master_thesis.get_absolute_url()}",
        },
    )


@celery_app.task()
def send_expert_validation(master_thesis_id):
    from .models import MasterThesis

    current_site = Site.objects.get_current()
    master_thesis = MasterThesis.objects.select_related("expert").get(
        pk=master_thesis_id,
    )

    if master_thesis.defenses.exists():
        defense = master_thesis.defenses.order_by("defense_date").last()
        if not defense.confirmed:
            defense = None
    else:
        defense = None

    if master_thesis.expert.language:
        translation.activate(master_thesis.expert.language)
    else:
        translation.activate("fr")

    url = reverse("tm:expert-view-thesis", kwargs={"pk": master_thesis_id})
    url += get_query_string(master_thesis.expert)

    send_templated_mail(
        template_name="expert_validation_notification",
        from_email=settings.DEFAULT_FROM_EMAIL,
        recipient_list=[master_thesis.expert.email],
        context={
            "master_thesis": master_thesis,
            "defense": defense,
            "sesame_url": True,
            "url": f"https://{current_site.domain}{url}",
        },
    )

    cc = []
    if master_thesis.co_supervisors:
        for co_supervisor in master_thesis.co_supervisors.all():
            cc.append(co_supervisor.email)  # noqa: PERF401

    send_templated_mail(
        template_name="expert_validation_notification",
        from_email=settings.DEFAULT_FROM_EMAIL,
        recipient_list=[master_thesis.supervisor.email],
        cc=cc,
        context={
            "master_thesis": master_thesis,
            "defense": defense,
            "sesame_url": False,
            "url": f"https://{current_site.domain}{master_thesis.get_absolute_url()}",
        },
    )


@celery_app.task()
def send_expert_confirmation(master_thesis_id):
    from .models import MasterThesis

    current_site = Site.objects.get_current()
    master_thesis = MasterThesis.objects.select_related("expert").get(
        pk=master_thesis_id,
    )

    if master_thesis.defenses.exists():
        defense = master_thesis.defenses.order_by("defense_date").last()
        if not defense.confirmed:
            defense = None
    else:
        defense = None

    if master_thesis.attempt.student.language:
        translation.activate(master_thesis.attempt.student.language)
    else:
        translation.activate("fr")

    cc = [master_thesis.supervisor.email, master_thesis.expert.email]
    if master_thesis.co_supervisors:
        for co_supervisor in master_thesis.co_supervisors.all():
            cc.append(co_supervisor.email)  # noqa: PERF401

    send_templated_mail(
        template_name="expert_confirmation_notification",
        from_email=settings.DEFAULT_FROM_EMAIL,
        recipient_list=[master_thesis.attempt.student.email],
        cc=cc,
        context={
            "master_thesis": master_thesis,
            "defense": defense,
            "url": f"https://{current_site.domain}{master_thesis.get_absolute_url()}",
        },
    )


@celery_app.task()
def send_expert_declined(master_thesis_id, expert_id, message):
    from .models import MasterThesis

    expert = User.objects.get(pk=expert_id)

    current_site = Site.objects.get_current()
    master_thesis = MasterThesis.objects.get(pk=master_thesis_id)

    if master_thesis.attempt.student.language:
        translation.activate(master_thesis.attempt.student.language)
    else:
        translation.activate("fr")

    cc = [expert.email]
    if master_thesis.co_supervisors:
        for co_supervisor in master_thesis.co_supervisors.all():
            cc.append(co_supervisor.email)  # noqa: PERF401

    send_templated_mail(
        template_name="expert_declined_notification",
        from_email=settings.DEFAULT_FROM_EMAIL,
        recipient_list=[master_thesis.supervisor.email],
        cc=cc,
        context={
            "master_thesis": master_thesis,
            "expert": expert,
            "message": message,
            "url": f"https://{current_site.domain}{master_thesis.get_absolute_url()}",
        },
    )


@celery_app.task()
def send_room_booking(defense_id, defense_date=None, place=None, room=None):
    from .models import Defense

    current_site = Site.objects.get_current()
    defense = Defense.objects.get(pk=defense_id)

    translation.activate("fr")

    notification_type = "cancellation" if defense_date and place and room else "booking"

    recipients = []
    if defense.master_thesis.attempt.major.secretary:
        recipients.append(defense.master_thesis.attempt.major.secretary.email)
    else:
        administration_group = defense.master_thesis.attempt.major.admin_group
        for user in administration_group.user_set.all():
            recipients.append(user.email)  # noqa: PERF401

    send_templated_mail(
        template_name="room_booking_notification",
        from_email=settings.DEFAULT_FROM_EMAIL,
        recipient_list=recipients,
        context={
            "defense": defense,
            "notification_type": notification_type,
            "defense_date": defense_date,
            "place": place,
            "room": room,
            "url": f"https://{current_site.domain}{defense.master_thesis.get_absolute_url()}",
        },
    )


@celery_app.task()
def send_record_publication_notification(master_thesis_id):
    from .models import MasterThesis

    current_site = Site.objects.get_current()
    master_thesis = MasterThesis.objects.get(pk=master_thesis_id)

    translation.activate("fr")

    recipients = []
    if master_thesis.attempt.major.secretary:
        recipients.append(master_thesis.attempt.major.secretary.email)
    else:
        administration_group = master_thesis.attempt.major.admin_group
        for user in administration_group.user_set.all():
            recipients.append(user.email)  # noqa: PERF401

    record_url = reverse(
        "tm:record-publishing-authorization",
        kwargs={"pk": master_thesis.id},
    )
    send_templated_mail(
        template_name="record_publication_notification",
        from_email=settings.DEFAULT_FROM_EMAIL,
        recipient_list=recipients,
        context={
            "master_thesis": master_thesis,
            "url": f"https://{current_site.domain}{record_url}",
        },
    )


@celery_app.task()
def send_confidentiality_agreement(master_thesis_id):
    from .models import MasterThesis

    master_thesis = MasterThesis.objects.get(pk=master_thesis_id)
    current_site = Site.objects.get_current()

    if master_thesis.attempt.student.language:
        translation.activate(master_thesis.attempt.student.language)
    else:
        translation.activate("fr")

    cc = [
        master_thesis.supervisor.email,
        master_thesis.attempt.major.secretary.email,
    ]

    if master_thesis.expert:
        cc.append(master_thesis.expert.email)

    message = get_templated_mail(
        template_name="confidentiality_agreement_notification",
        from_email=settings.DEFAULT_FROM_EMAIL,
        to=[master_thesis.attempt.student.email],
        cc=cc,
        context={
            "master_thesis": master_thesis,
            "url": f"https://{current_site.domain}{master_thesis.get_absolute_url()}",
        },
    )

    message.attach(
        f"MScBA_C.{master_thesis.id}_{master_thesis.attempt.major.code}_{master_thesis.attempt.session.name}_Thesis_{master_thesis.attempt.student}_confidentiality_agreement.pdf",
        fill_confidentiality_agreement(master_thesis.id),
        "application/pdf",
    )

    message.send(fail_silently=False)


@celery_app.task()
def cron_send_notification_confidentiality_agreement():
    from .models import Attempt

    current_attempts = Attempt.objects.select_related("calendar").filter(
        done=False,
        calendar__isnull=False,
    )
    for attempt in current_attempts:
        thesis_deadline = attempt.current_calendar.thesis_end.date()
        if thesis_deadline == timezone.now().date() + timedelta(days=30):
            thesis = attempt.theses.filter(submitted=False).first()
            if thesis.confidential and not thesis.confidential_agreement_file:
                send_confidentiality_agreement.delay(thesis.id)


@celery_app.task()
def cron_send_notification_thesis_deadline():
    from .models import Attempt

    current_attempts = Attempt.objects.select_related("calendar").filter(
        done=False,
        calendar__isnull=False,
    )
    for attempt in current_attempts:
        thesis_deadline = attempt.current_calendar.thesis_end.date()
        if thesis_deadline == timezone.now().date() + timedelta(days=1):
            thesis = attempt.theses.filter(submitted=False).first()
            if thesis:
                send_notification_thesis_deadline.delay(thesis.id)


@celery_app.task()
def send_notification_defense_all():
    from .models import Attempt
    from .models import MasterThesis

    current_attempts = Attempt.objects.select_related("calendar").filter(done=False)
    for attempt in current_attempts:
        # expert_deadline = attempt.current_calendar.expert_deadline  # noqa: ERA001
        thesis = attempt.theses.filter(result=MasterThesis.NOT_ASSESSED).first()

        if thesis:
            if not thesis.attempt.block_defense:
                if thesis.expert and not thesis.defenses.exists():
                    send_notification_defense.delay(thesis.id)


@celery_app.task()
def send_notification_defense(thesis_id, reminder=False):  # noqa: FBT002
    from .models import MasterThesis

    current_site = Site.objects.get_current()
    master_thesis = MasterThesis.objects.get(pk=thesis_id)

    if master_thesis.attempt.student.language:
        translation.activate(master_thesis.attempt.student.language)
    else:
        translation.activate("fr")

    cc = []
    if master_thesis.co_supervisors:
        for co_supervisor in master_thesis.co_supervisors.all():
            cc.append(co_supervisor.email)  # noqa: PERF401

    send_templated_mail(
        template_name="defense_notification",
        from_email=settings.DEFAULT_FROM_EMAIL,
        recipient_list=[master_thesis.supervisor.email],
        cc=cc,
        context={
            "master_thesis": master_thesis,
            "reminder": reminder,
            "url": f"https://{current_site.domain}{master_thesis.get_absolute_url()}",
        },
    )


@celery_app.task()
def cron_send_reminder_documents():
    from .models import Defense

    defenses = Defense.objects.exclude(
        master_thesis__hard_copies_received=True,
        master_thesis__authorization_publish_file__isnull=False,
        statement_of_fees_file__isnull=False,
        assessment_file__isnull=False,
    )

    for defense in defenses:
        # We send a reminder 5 days before, the day of the defense the day after the
        # defense and 3 and 5 days after
        if (
            (defense.defense_date.date() == timezone.now().date() + timedelta(days=5))
            or (defense.defense_date.date() == timezone.now().date())
            or (
                defense.defense_date.date() == timezone.now().date() - timedelta(days=1)
            )
            or (
                defense.defense_date.date() == timezone.now().date() - timedelta(days=3)
            )
            or (
                defense.defense_date.date() == timezone.now().date() - timedelta(days=5)
            )
        ):
            reminder_defense_documents.delay(defense.id)


@celery_app.task()
def reminder_defense_documents(defense_id):
    from .models import Defense

    defense = Defense.objects.get(pk=defense_id)
    documents = {"copies": False, "publish": False, "fees": False, "assessment": False}

    # We have to found the missing documents first
    if not defense.master_thesis.hard_copies_status:
        documents["copies"] = True
    if not defense.master_thesis.authorization_publish_file:
        documents["publish"] = True
    if not defense.statement_of_fees_file:
        documents["fees"] = True
    if not defense.assessment_file and defense.defense_date < timezone.now():
        documents["assessment"] = True

    if documents["copies"] or documents["publish"]:
        # We send a reminder to the student
        to = defense.master_thesis.attempt.student.email
        send_reminder_defense_documents.delay(defense_id, documents, to, "student")
    if documents["fees"]:
        # We send a reminder to the expert
        to = defense.master_thesis.expert.email
        send_reminder_defense_documents.delay(defense_id, documents, to, "expert")
    if documents["assessment"]:
        # We send a reminder to the supervisor
        to = defense.master_thesis.supervisor.email
        send_reminder_defense_documents.delay(defense_id, documents, to, "supervisor")


@celery_app.task()
def send_reminder_defense_documents(defense_id, documents, to, recipient):
    from .models import Defense

    current_site = Site.objects.get_current()
    defense = Defense.objects.get(pk=defense_id)

    if defense.master_thesis.attempt.student.language:
        translation.activate(defense.master_thesis.attempt.student.language)
    else:
        translation.activate("fr")

    url = defense.master_thesis.get_absolute_url()
    if recipient == "expert":
        url += get_query_string(defense.master_thesis.expert)

    cc = [
        defense.master_thesis.attempt.major.secretary.email,
        defense.master_thesis.attempt.major.coordinator.email,
    ]

    if recipient == "supervisor":
        if defense.master_thesis.co_supervisors:
            for co_supervisor in defense.master_thesis.co_supervisors.all():
                cc.append(co_supervisor.email)  # noqa: PERF401

    message = get_templated_mail(
        template_name="defense_documents_reminder",
        from_email=settings.DEFAULT_FROM_EMAIL,
        to=[to],
        cc=cc,
        context={
            "defense": defense,
            "documents": documents,
            "recipient": recipient,
            "url": f"https://{current_site.domain}{url}",
        },
    )

    if recipient == "student" and documents["publish"]:
        message.attach(
            f"MScBA_P.{defense.master_thesis.id}_{defense.master_thesis.attempt.major.code}_{defense.master_thesis.attempt.session.name}_Thesis_{defense.master_thesis.attempt.student}_consent_to_publish.pdf",
            fill_publishing_authorization(defense.master_thesis_id),
            "application/pdf",
        )
    if recipient == "expert" and documents["fees"]:
        message.attach(
            f"MScBA_F.{defense.id}_{defense.master_thesis.attempt.major.code}_{defense.master_thesis.attempt.session.name}_Defense_{defense.defense_date.strftime('%Y-%m-%d')}_{defense.master_thesis.attempt.student}_{defense.master_thesis.expert}_statement_of_fees.pdf",
            fill_statement_of_fees(defense.id),
            "application/pdf",
        )
    if recipient == "supervisor" and documents["assessment"]:
        message.attach(
            f"MScBA_A.{defense.id}_{defense.master_thesis.attempt.major.code}_{defense.master_thesis.attempt.session.name}_Defense_{defense.defense_date.strftime('%Y-%m-%d')}_{defense.master_thesis.attempt.student}_ASSESSMENT.pdf",
            fill_thesis_assessment(defense.id),
            "application/pdf",
        )

    message.send(fail_silently=False)


@celery_app.task()
def send_compilatio_certificate(thesis_id):
    from .models import MasterThesis

    current_site = Site.objects.get_current()
    master_thesis = MasterThesis.objects.get(pk=thesis_id)

    if master_thesis.attempt.student.language:
        translation.activate(master_thesis.attempt.student.language)
    else:
        translation.activate("fr")

    cc = []
    if master_thesis.co_supervisors:
        for co_supervisor in master_thesis.co_supervisors.all():
            cc.append(co_supervisor.email)  # noqa: PERF401

    message = get_templated_mail(
        template_name="compilatio_report_notification",
        from_email=settings.DEFAULT_FROM_EMAIL,
        to=[master_thesis.attempt.student.email, master_thesis.supervisor.email],
        cc=cc,
        context={
            "master_thesis": master_thesis,
            "url": f"https://{current_site.domain}{master_thesis.get_absolute_url()}",
        },
    )

    filename = master_thesis.compilatio_report.name
    message.attach(filename, default_storage.open(filename).read(), "application/pdf")

    message.send(fail_silently=False)


def create_defense_ics(defense_id):
    from .models import Defense

    defense = Defense.objects.get(pk=defense_id)
    if defense.master_thesis.attempt.student.language:
        translation.activate(defense.master_thesis.attempt.student.language)
    else:
        translation.activate("fr")

    cal = Calendar()
    cal.add("prodid", "-//MScBA Theses Organizer//mscba.hes-so.ch//")
    cal.add("version", "2.0")
    cal.add("method", "REQUEST")

    event = Event()
    event.add(
        "summary",
        _(f"Master's thesis defense - {defense.master_thesis.attempt.student.name}"),  # noqa: INT001
    )
    event.add("dtstart", defense.defense_date)
    event.add("dtend", defense.defense_date_end)
    event.add("dtstamp", timezone.now())
    event.add("status", "CONFIRMED")
    event.add("category", "Event")

    supervisor = defense.master_thesis.supervisor
    if supervisor:
        organizer = vCalAddress(f"MAILTO:{supervisor.email}")
        organizer.params["cn"] = vText(supervisor.name)
        organizer.params["role"] = vText("CHAIR")
        event["organizer"] = organizer

    if defense.place == "Other":
        event["location"] = vText(defense.other_location)
    else:
        location = defense.get_place_display() + ", "
        if defense.room:
            location += _(f"room {defense.room}")  # noqa: INT001
        else:
            location += _("room to be confirmed")
        event["location"] = vText(location)

    event["uid"] = f"{defense.id}@mscba.hes-so.ch"
    event.add("priority", 1)

    participants = [defense.master_thesis.attempt.student]

    if defense.master_thesis.expert:
        participants.append(defense.master_thesis.expert)

    if defense.master_thesis.co_supervisors:
        for co_supervisor in defense.master_thesis.co_supervisors.all():
            participants.append(co_supervisor)  # noqa: PERF402

    for participant in participants:
        attendee = vCalAddress(f"MAILTO:{participant.email}")
        attendee.params["cn"] = vText(participant.name)
        attendee.params["ROLE"] = vText("REQ-PARTICIPANT")
        event.add("attendee", attendee, encode=0)
        event.add(
            "ATTENDEE;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;RSVP=FALSE",
            participant.email,
        )

    cal.add_component(event)
    return cal.to_ical().decode("utf-8")


@celery_app.task()
def send_defense_schedule(defense_id, update=None):
    from .models import Defense

    defense = Defense.objects.get(pk=defense_id)

    if defense.master_thesis.attempt.student.language:
        translation.activate(defense.master_thesis.attempt.student.language)
    else:
        translation.activate("fr")

    to = [
        defense.master_thesis.attempt.student.email,
        defense.master_thesis.supervisor.email,
        defense.master_thesis.expert.email,
    ]
    if defense.master_thesis.co_supervisors:
        for co_supervisor in defense.master_thesis.co_supervisors.all():
            to.append(co_supervisor.email)  # noqa: PERF401

    cc = []
    if defense.master_thesis.attempt.major.secretary:
        cc.append(defense.master_thesis.attempt.major.secretary.email)
    else:
        administration_group = defense.master_thesis.attempt.major.admin_group
        for user in administration_group.user_set.all():
            cc.append(user.email)  # noqa: PERF401

    message = get_templated_mail(
        template_name="defense_schedule_notification",
        from_email=settings.DEFAULT_FROM_EMAIL,
        to=to,
        cc=cc,
        context={
            "defense": defense,
            "update": update,
        },
    )
    message.attach(
        "invite.ics",
        create_defense_ics(defense.id),
        'text/calendar; charset="utf-8"; method=REQUEST',
    )
    message.send(fail_silently=False)


@celery_app.task()
def send_defense_invitation(defense_id):
    from .models import Defense

    current_site = Site.objects.get_current()
    defense = Defense.objects.get(pk=defense_id)

    if defense.master_thesis.attempt.student.language:
        translation.activate(defense.master_thesis.attempt.student.language)
    else:
        translation.activate("fr")

    # We send one email to each participant due to the fact that we need to send a
    # different link to each of them
    thesis_file = defense.master_thesis.pdf_file.name
    if defense.master_thesis.confidential:
        thesis_file_name = f"MScBA_{defense.master_thesis.attempt.major.code}_{defense.master_thesis.attempt.session.name}_Thesis_{defense.master_thesis.attempt.student}-CONFIDENTIAL.pdf"  # noqa: E501
    else:
        thesis_file_name = f"MScBA_{defense.master_thesis.attempt.major.code}_{defense.master_thesis.attempt.session.name}_Thesis_{defense.master_thesis.attempt.student}.pdf"  # noqa: E501

    ical = create_defense_ics(defense.id)

    # Student
    # We send the thesis and the publication form
    recipient = "student"

    # We fill the form
    pdf = fill_publishing_authorization(defense.master_thesis_id)

    message = get_templated_mail(
        template_name="defense_invitation",
        from_email=settings.DEFAULT_FROM_EMAIL,
        to=[defense.master_thesis.attempt.student.email],
        cc=[defense.master_thesis.attempt.major.secretary.email],
        context={
            "defense": defense,
            "recipient": recipient,
            "url": f"https://{current_site.domain}{defense.master_thesis.get_absolute_url()}",
        },
        headers={"Content-class": "urn:content-classes:calendarmessage"},
    )
    message.attach(
        f"MScBA_P.{defense.master_thesis.id}_{defense.master_thesis.attempt.major.code}_{defense.master_thesis.attempt.session.name}_Thesis_{defense.master_thesis.attempt.student}_consent_to_publish.pdf",
        pdf,
        "application/pdf",
    )

    message.attach(
        thesis_file_name,
        default_storage.open(thesis_file).read(),
        "application/pdf",
    )

    message.attach(
        "invite.ics",
        ical,
        'text/calendar; charset="utf-8"; method=REQUEST',
    )

    message.send(fail_silently=False)

    # Expert
    # We send the thesis and the statement of fees form
    recipient = "expert"

    pdf = fill_statement_of_fees(defense.id)

    url = reverse("tm:expert-view-thesis", kwargs={"pk": defense.master_thesis_id})
    url += get_query_string(defense.master_thesis.expert)

    message = get_templated_mail(
        template_name="defense_invitation",
        from_email=settings.DEFAULT_FROM_EMAIL,
        to=[defense.master_thesis.expert.email],
        cc=[defense.master_thesis.attempt.major.secretary.email],
        context={
            "defense": defense,
            "recipient": recipient,
            "url": f"https://{current_site.domain}{url}",
        },
    )
    message.attach(
        f"MScBA_F.{defense.id}_{defense.master_thesis.attempt.major.code}_{defense.master_thesis.attempt.session.name}_Defense_{defense.defense_date.strftime('%Y-%m-%d')}_{defense.master_thesis.attempt.student}_{defense.master_thesis.expert}_statement_of_fees.pdf",
        pdf,
        "application/pdf",
    )
    message.attach(
        thesis_file_name,
        default_storage.open(thesis_file).read(),
        "application/pdf",
    )
    message.attach(
        "invite.ics",
        ical,
        'text/calendar; charset="utf-8"; method=REQUEST',
    )
    message.send(fail_silently=False)

    # Supervisor
    # We send the thesis and the assessment form
    recipient = "supervisor"

    pdf = fill_thesis_assessment(defense.id)

    to = [defense.master_thesis.supervisor.email]

    if defense.master_thesis.co_supervisors:
        for co_supervisor in defense.master_thesis.co_supervisors.all():
            to.append(co_supervisor.email)  # noqa: PERF401

    message = get_templated_mail(
        template_name="defense_invitation",
        from_email=settings.DEFAULT_FROM_EMAIL,
        to=to,
        cc=[defense.master_thesis.attempt.major.secretary.email],
        context={
            "defense": defense,
            "recipient": recipient,
            "url": f"https://{current_site.domain}{defense.master_thesis.get_absolute_url()}",
        },
    )
    message.attach(
        f"MScBA_A.{defense.id}_{defense.master_thesis.attempt.major.code}_{defense.master_thesis.attempt.session.name}_Defense_{defense.defense_date.strftime('%Y-%m-%d')}_{defense.master_thesis.attempt.student}_ASSESSMENT.pdf",
        pdf,
        "application/pdf",
    )
    message.attach(
        thesis_file_name,
        default_storage.open(thesis_file).read(),
        "application/pdf",
    )
    message.attach(
        "invite.ics",
        ical,
        'text/calendar; charset="utf-8"; method=REQUEST',
    )
    message.send(fail_silently=False)


@celery_app.task()
def cron_send_defense_invitations():
    from .models import Defense

    defenses = Defense.objects.filter(
        invitation_sent__isnull=True,
        master_thesis__submitted=True,
        master_thesis__expert__isnull=False,
        defense_date__isnull=False,
    )

    # We select only the defenses where the thesis is due and where there is no
    # exceptions (mainly missing credits)
    # We cannot filter on properties, so we need to loop over the queryset
    for defense in defenses:
        if (
            not defense.master_thesis.attempt.block_defense
            and defense.master_thesis.attempt.current_calendar.thesis_due
        ):
            send_defense_invitation.delay(defense.id)
            defense.invitation_sent = timezone.now()
            defense.save()


@celery_app.task()
def send_assessment_confirmation(defense_id):
    from .models import Defense

    defense = Defense.objects.get(pk=defense_id)

    if defense.master_thesis.attempt.student.language:
        translation.activate(defense.master_thesis.attempt.student.language)
    else:
        translation.activate("fr")

    to = [defense.master_thesis.supervisor.email, defense.master_thesis.expert.email]
    if defense.master_thesis.co_supervisors:
        for co_supervisor in defense.master_thesis.co_supervisors.all():
            to.append(co_supervisor.email)  # noqa: PERF401

    cc = []

    administration_group = defense.master_thesis.attempt.major.admin_group
    for user in administration_group.user_set.all():
        cc.append(user.email)  # noqa: PERF401

    academic_group = defense.master_thesis.attempt.major.academic_group
    for user in academic_group.user_set.all():
        cc.append(user.email)  # noqa: PERF401

    secretary = defense.master_thesis.attempt.major.secretary
    if secretary:
        cc.append(secretary.email)

    # We remove duplicates emails
    cc = list(set(cc))

    if defense.master_thesis.attempt.major.coordinator:
        coordinator = defense.master_thesis.attempt.major.coordinator.name
    else:
        coordinator = ""

    message = get_templated_mail(
        template_name="defense_assessment",
        from_email=settings.DEFAULT_FROM_EMAIL,
        to=to,
        cc=cc,
        context={
            "defense": defense,
            "coordinator": coordinator,
        },
    )

    message.send(fail_silently=False)


def create_zip_thesis(thesis_id):  # noqa: C901
    from .models import MasterThesis

    master_thesis = MasterThesis.objects.get(pk=thesis_id)

    # We collect all files and prepare a ZIP file with all documents
    zip_file = BytesIO()
    with zipfile.ZipFile(zip_file, "w") as zf:
        # Disposition
        if master_thesis.project.pdf_file:
            file_name = f"MScBA_{master_thesis.attempt.major.code}_{master_thesis.attempt.session.name}_Disposition_{master_thesis.attempt.student}.pdf"  # noqa: E501
            zf.writestr(file_name, master_thesis.project.pdf_file.file.read())
        # Master's Thesis
        if master_thesis.pdf_file:
            if master_thesis.confidential:
                file_name = f"MScBA_{master_thesis.attempt.major.code}_{master_thesis.attempt.session.name}_Thesis_{master_thesis.attempt.student}-CONFIDENTIAL.pdf"  # noqa: E501
            else:
                file_name = f"MScBA_{master_thesis.attempt.major.code}_{master_thesis.attempt.session.name}_Thesis_{master_thesis.attempt.student}.pdf"  # noqa: E501
            zf.writestr(file_name, master_thesis.pdf_file.file.read())
        # Master's Thesis ZIP
        if master_thesis.zip_file:
            file_name = f"MScBA_{master_thesis.attempt.major.code}_{master_thesis.attempt.session.name}_Thesis_{master_thesis.attempt.student}_appendices.zip"  # noqa: E501
            zf.writestr(file_name, master_thesis.zip_file.file.read())
        # Confidentiality agreement
        if master_thesis.confidential_agreement_file:
            file_name = f"MScBA_{master_thesis.attempt.major.code}_{master_thesis.attempt.session.name}_Thesis_{master_thesis.attempt.student}_confidentiality_agreement.pdf"  # noqa: E501
            zf.writestr(
                file_name,
                master_thesis.confidential_agreement_file.file.read(),
            )
        # Authorization to publish
        if master_thesis.authorization_publish_file:
            file_name = f"MScBA_P.{master_thesis.id}_{master_thesis.attempt.major.code}_{master_thesis.attempt.session.name}_Thesis_{master_thesis.attempt.student}_consent_to_publish.pdf"  # noqa: E501
            zf.writestr(file_name, master_thesis.authorization_publish_file.file.read())
        # Master's Thesis remediation
        if master_thesis.pdf_retake_file:
            file_name = f"MScBA_{master_thesis.attempt.major.code}_{master_thesis.attempt.session.name}_Thesis_REMEDIATION_{master_thesis.attempt.student}.pdf"  # noqa: E501
            zf.writestr(file_name, master_thesis.pdf_retake_file.file.read())
        # Master's Thesis ZIP remediation
        if master_thesis.zip_retake_file:
            file_name = f"MScBA_{master_thesis.attempt.major.code}_{master_thesis.attempt.session.name}_Thesis_REMEDIATION_{master_thesis.attempt.student}_appendices.zip"  # noqa: E501
            zf.writestr(file_name, master_thesis.zip_retake_file.file.read())
        # We add files from each defense
        for defense in master_thesis.defenses.all():
            # Statement of fees
            if defense.statement_of_fees_file:
                file_name = f"MScBA_F.{defense.id}_{master_thesis.attempt.major.code}_{master_thesis.attempt.session.name}_Defense_{defense.defense_date.strftime('%Y-%m-%d')}_{master_thesis.attempt.student}_{master_thesis.expert}_statement_of_fees.pdf"  # noqa: E501
                zf.writestr(file_name, defense.statement_of_fees_file.file.read())
            # Statement of fees
            if defense.assessment_file:
                file_name = f"MScBA_A.{defense.id}_{master_thesis.attempt.major.code}_{master_thesis.attempt.session.name}_Defense_{defense.defense_date.strftime('%Y-%m-%d')}_{master_thesis.attempt.student}_ASSESSMENT.pdf"  # noqa: E501
                zf.writestr(file_name, defense.assessment_file.file.read())

    zip_file.seek(0)
    return zip_file


def validate_language_code(language_code):
    from django.conf import settings

    valid_language_codes = [code for code, name in settings.LANGUAGES]

    if language_code in valid_language_codes:
        return language_code
    return "fr"


@celery_app.task(bind=True)
def import_students_by_isa(self, isa_ids, language_code, session_id, major_id):
    """
    Based on a list of ISA IDs, we create the users and the attempts
    :param isa_ids: a list of ISA IDs one per line
    :param language_code: the language code of the students
    :param session_id: the session ID
    :param major_id: the major ID
    :return: attempts a list of attempts, errors a list of ISA
    IDs that could not be imported
    """
    from .directory import query_user_by_id
    from .models import Attempt

    errors = []
    attempts = []

    progress_recorder = ProgressRecorder(self)
    total_isa_ids = len(isa_ids)
    progress = 0

    language = validate_language_code(language_code)

    for isa_id in isa_ids:
        # We check if the student already exists
        try:
            user = User.objects.get(username=f"{isa_id}@hes-so.ch")
            user.majors.add(major_id)
        except User.DoesNotExist:
            # We retrieve the student information from the directory
            student = query_user_by_id(isa_id)
            if student:
                # We create the student
                user = User.objects.create(
                    username=f"{isa_id}@hes-so.ch",
                    first_name=student["first_name"],
                    last_name=student["last_name"],
                    email=student["email"],
                    name=student["display_name"],
                    gender=student["gender"],
                    is_student=True,
                    language=language,
                )
                user.majors.add(major_id)
            else:
                errors.append(isa_id)
                user = None

        if user:
            # We create the attempt
            attempt = Attempt.objects.create(
                student=user,
                session_id=session_id,
                major_id=major_id,
            )
            attempts.append({"attempt_id": attempt.id, "student": attempt.student.name})

        progress += 1  # noqa: SIM113
        progress_recorder.set_progress(progress, total_isa_ids)
        time.sleep(3)

    if len(errors) == 0:
        errors = None

    return {"attempts": attempts, "errors": errors}


@celery_app.task()
def send_thesis_submitted_notification(thesis_id):
    from .models import MasterThesis

    current_site = Site.objects.get_current()
    master_thesis = MasterThesis.objects.get(pk=thesis_id)

    if master_thesis.attempt.student.language:
        translation.activate(master_thesis.attempt.student.language)
    else:
        translation.activate("fr")

    cc = [master_thesis.supervisor.email]
    if master_thesis.co_supervisors:
        for co_supervisor in master_thesis.co_supervisors.all():
            cc.append(co_supervisor.email)  # noqa: PERF401

    send_templated_mail(
        template_name="thesis_submitted_notification",
        from_email=settings.DEFAULT_FROM_EMAIL,
        recipient_list=[master_thesis.attempt.student.email],
        cc=cc,
        context={
            "master_thesis": master_thesis,
            "url": f"https://{current_site.domain}{master_thesis.get_absolute_url()}",
        },
    )


@celery_app.task()
def send_notification_thesis_deadline(thesis_id):
    from .models import MasterThesis

    master_thesis = MasterThesis.objects.get(pk=thesis_id)

    current_site = Site.objects.get_current()

    if master_thesis.attempt.student.language:
        translation.activate(master_thesis.attempt.student.language)
    else:
        translation.activate("fr")

    cc = [master_thesis.supervisor.email]
    if master_thesis.co_supervisors:
        for co_supervisor in master_thesis.co_supervisors.all():
            cc.append(co_supervisor.email)  # noqa: PERF401

    send_templated_mail(
        template_name="thesis_deadline_notification",
        from_email=settings.DEFAULT_FROM_EMAIL,
        recipient_list=[master_thesis.attempt.student.email],
        cc=cc,
        context={
            "master_thesis": master_thesis,
            "url": f"https://{current_site.domain}{master_thesis.get_absolute_url()}",
        },
    )


def create_zip_authorization_publish(session_id, major_ids):
    from .models import MasterThesis

    logger.info(f"Preparing ZIP file for session {session_id} and majors {major_ids}")  # noqa: G004

    master_theses = MasterThesis.objects.filter(attempt__session_id=session_id)

    if major_ids:
        master_theses = master_theses.filter(attempt__major_id__in=major_ids)

    zip_file = BytesIO()
    with zipfile.ZipFile(zip_file, "w") as zf:
        for master_thesis in master_theses:
            if master_thesis.authorization_publish_file:
                file_name = f"MScBA_P.{master_thesis.id}_{master_thesis.attempt.major.code}_{master_thesis.attempt.session.name}_Thesis_{master_thesis.attempt.student}_consent_to_publish.pdf"  # noqa: E501
                zf.writestr(
                    file_name,
                    master_thesis.authorization_publish_file.file.read(),
                )

    zip_file.seek(0)
    return zip_file


def create_zip_statement_fees(session_id, major_ids=None):
    from .models import Defense

    logger.info(f"Preparing ZIP file for session {session_id} and majors {major_ids}")  # noqa: G004
    defenses = Defense.objects.filter(master_thesis__attempt__session_id=session_id)

    if major_ids:
        defenses = defenses.filter(master_thesis__attempt__major_id__in=major_ids)

    zip_file = BytesIO()
    with zipfile.ZipFile(zip_file, "w") as zf:
        for defense in defenses:
            if defense.statement_of_fees_file:
                file_name = f"MScBA_F.{defense.id}_{defense.master_thesis.attempt.major.code}_{defense.master_thesis.attempt.session.name}_Defense_{defense.defense_date.strftime('%Y-%m-%d')}_{defense.master_thesis.attempt.student}_{defense.master_thesis.expert}_statement_of_fees.pdf"  # noqa: E501
                zf.writestr(file_name, defense.statement_of_fees_file.file.read())

    zip_file.seek(0)
    return zip_file


def create_zip_assessment(session_id, major_ids=None):
    from .models import Defense

    logger.info(f"Preparing ZIP file for session {session_id} and majors {major_ids}")  # noqa: G004
    defenses = Defense.objects.filter(master_thesis__attempt__session_id=session_id)

    if major_ids:
        defenses = defenses.filter(master_thesis__attempt__major_id__in=major_ids)

    zip_file = BytesIO()
    with zipfile.ZipFile(zip_file, "w") as zf:
        for defense in defenses:
            if defense.assessment_file:
                file_name = f"MScBA_A.{defense.id}_{defense.master_thesis.attempt.major.code}_{defense.master_thesis.attempt.session.name}_Defense_{defense.defense_date.strftime('%Y-%m-%d')}_{defense.master_thesis.attempt.student}_ASSESSMENT.pdf"  # noqa: E501
                zf.writestr(file_name, defense.assessment_file.file.read())

    zip_file.seek(0)
    return zip_file


@celery_app.task(bind=True)
def anonymize_data(self):  # noqa: C901, PLR0912, PLR0915
    """
    Anonymize production database to use it in development and testing
    :return: None
    """
    from django.contrib.staticfiles.storage import staticfiles_storage
    from faker import Faker

    from .models import Calendar
    from .models import Defense
    from .models import MasterThesis

    progress_recorder = ProgressRecorder(self)
    fake = Faker()

    # We anonymize the users
    progress_recorder.set_progress(1, 3, description="Anonymizing users")
    users = User.objects.all()
    i = 0
    tot = users.count()
    progress_recorder.set_progress(i, tot)

    for user in users:
        Faker.seed(user.id)
        if user.groups.count() == 0:
            user.username = fake.user_name()
        user.first_name = fake.first_name()
        user.last_name = fake.last_name()
        user.email = fake.email()
        user.name = fake.name()
        if user.address:
            user.address = fake.street_address()
            user.city = fake.city()
            user.zip_code = fake.zipcode()
        if user.current_job:
            user.current_job = fake.job()
            user.current_company = fake.company()
            user.profile_linkedin = "https://www.linkedin.com"
        if not user.is_staff:
            user.set_password(settings.DEFAULT_PASSWORD)
        user.save()
        i += 1
        progress_recorder.set_progress(i, tot)

    # We anonymize the calendars
    progress_recorder.set_progress(2, 3, description="Anonymizing calendars")
    calendars = Calendar.objects.filter(custom=True)
    i = 0
    tot = calendars.count()
    progress_recorder.set_progress(i, tot)

    for calendar in calendars:
        Faker.seed(calendar.id)
        attempt = calendar.attempt.all()[0]
        calendar.label = f"{attempt.student.name} - {attempt.session.name}"
        calendar.save()
        i += 1
        progress_recorder.set_progress(i, tot)

    # We anonymize the theses
    progress_recorder.set_progress(3, 4, description="Anonymizing theses")
    theses = MasterThesis.objects.all()
    i = 0
    tot = theses.count()
    progress_recorder.set_progress(i, tot)

    for thesis in theses:
        Faker.seed(thesis.id)
        title = fake.sentence(nb_words=8)

        # Theses
        thesis.title = title
        if thesis.pdf_file:
            thesis.pdf_file = staticfiles_storage.open("tests/thesis.pdf")
        if thesis.zip_file:
            thesis.zip_file = staticfiles_storage.open("tests/thesis_appendices.zip")
        if thesis.pdf_retake_file:
            thesis.pdf_retake_file = staticfiles_storage.open("tests/thesis.pdf")
        if thesis.zip_retake_file:
            thesis.zip_retake_file = staticfiles_storage.open(
                "tests/thesis_appendices.zip",
            )
        if thesis.confidential_agreement_file:
            thesis.confidential_agreement_file = staticfiles_storage.open(
                "tests/confidentiality_agreement.pdf",
            )
        if thesis.authorization_publish_file:
            thesis.authorization_publish_file = staticfiles_storage.open(
                "tests/accord_publication_tm_2023.pdf",
            )
        if thesis.compilatio_report:
            thesis.compilatio_report = staticfiles_storage.open("tests/compilatio.pdf")
        thesis.save()

        # Project
        project = thesis.project
        project.title = title
        if project.pdf_file:
            project.pdf_file = staticfiles_storage.open("tests/project_tm_2023.pdf")
        project.save()

        # Proposal
        proposal = project.proposal
        proposal.title = title
        proposal.save()

        i += 1
        progress_recorder.set_progress(i, tot)

    # We anonymize the defenses
    progress_recorder.set_progress(4, 4, description="Anonymizing defenses")
    defenses = Defense.objects.all()
    i = 0
    tot = defenses.count()
    progress_recorder.set_progress(i, tot)

    for defense in defenses:
        if defense.statement_of_fees_file:
            defense.statement_of_fees_file = staticfiles_storage.open(
                "tests/statement_of_fees_tm_2023.pdf",
            )
        if defense.assessment_file:
            defense.assessment_file = staticfiles_storage.open(
                "tests/thesis_assessment_2023.pdf",
            )
        defense.save()
        i += 1
        progress_recorder.set_progress(i, tot)

    progress_recorder.set_progress(4, 4, description="All done!")

    return {"attempts": None, "errors": None}


@celery_app.task()
def reminder_assignments():
    from .models import Assignment

    # We retrieve all assignments that have not been accepted/declined yet and that
    # have reminders enabled
    assignments = Assignment.objects.filter(
        accepted__isnull=True,
        reminders=True,
        archive_date__isnull=True,
        validated=True,
    )

    for assignment in assignments:
        # We send a reminder every 4 days after the initial notification
        if assignment.last_reminder:
            last_notification = assignment.last_reminder
        else:
            last_notification = assignment.request_date
        if last_notification <= timezone.now().date() + timedelta(days=4):
            send_notification_validated.delay(assignment.id, reminder=True)


@celery_app.task()
def exception_notification(exception_id):
    from .models import StudentException

    student_exception = StudentException.objects.get(pk=exception_id)
    current_site = Site.objects.get_current()

    if student_exception.attempt.student.language:
        translation.activate(student_exception.attempt.student.language)
    else:
        translation.activate("fr")

    cc = [student_exception.created_by.email]
    if student_exception.attempt.major.secretary:
        cc.append(student_exception.attempt.major.secretary.email)
    if student_exception.attempt.major.coordinator:
        cc.append(student_exception.attempt.major.coordinator.email)
    # We remove duplicates emails
    cc = list(set(cc))

    recipient_list = [student_exception.attempt.student.email]

    # We add the supervisor and co-supervisors
    if student_exception.attempt.supervisor:
        recipient_list.append(student_exception.attempt.supervisor.email)
    if student_exception.attempt.co_supervisors:
        recipient_list.extend(
            [
                co_supervisor.email
                for co_supervisor in student_exception.attempt.co_supervisors.all()
            ],
        )

    url = f"https://{current_site.domain}{student_exception.attempt.get_absolute_url()}"

    send_templated_mail(
        template_name="exception_notification",
        from_email=settings.DEFAULT_FROM_EMAIL,
        recipient_list=recipient_list,
        cc=cc,
        context={
            "student_exception": student_exception,
            "url": url,
        },
    )


@celery_app.task()
def send_exception_cleared_notification(exception_id):
    from .models import StudentException

    student_exception = StudentException.objects.get(pk=exception_id)
    current_site = Site.objects.get_current()

    if student_exception.attempt.student.language:
        translation.activate(student_exception.attempt.student.language)
    else:
        translation.activate("fr")

    cc = [student_exception.created_by.email, student_exception.cleared_by.email]
    if student_exception.attempt.major.secretary:
        cc.append(student_exception.attempt.major.secretary.email)
    if student_exception.attempt.major.coordinator:
        cc.append(student_exception.attempt.major.coordinator.email)
    # We remove duplicates emails
    cc = list(set(cc))

    master_thesis = student_exception.attempt.theses.last()
    recipient_list = [student_exception.attempt.student.email]

    if master_thesis:
        recipient_list.append(master_thesis.supervisor.email)
        if master_thesis.co_supervisors:
            for co_supervisor in master_thesis.co_supervisors.all():
                recipient_list.append(co_supervisor.email)  # noqa: PERF401
        url = f"https://{current_site.domain}{master_thesis.get_absolute_url()}"
    else:
        url = f"https://{current_site.domain}{student_exception.attempt.get_absolute_url()}"

    send_templated_mail(
        template_name="exception_cleared_notification",
        from_email=settings.DEFAULT_FROM_EMAIL,
        recipient_list=recipient_list,
        cc=cc,
        context={
            "student_exception": student_exception,
            "url": url,
        },
    )


@celery_app.task()
def exception_cleared_notification(exception_id):
    from .models import StudentException

    student_exception = StudentException.objects.get(pk=exception_id)

    # We check if there are other exceptions for the same student
    exceptions = StudentException.objects.filter(
        attempt=student_exception.attempt,
        cleared=False,
    )

    if exceptions.count() == 0:
        # We send a notification to the student
        send_exception_cleared_notification.delay(exception_id)
