from http import HTTPStatus
from unittest.mock import patch

from django.conf import settings
from django.test import TestCase
from django.urls import reverse

from apps.users.tests.factories import UserFactory


class ShibbolethLoginViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.url = reverse("shibboleth:login")
        cls.login_url = settings.LOGIN_URL

    def test_get_redirected(self):
        response = self.client.get(self.url)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith(f"{self.login_url}?target=")

    def test_get_with_target(self):
        target = "/some/other/url/"
        response = self.client.get(f"{self.url}?target={target}")
        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith(f"{self.login_url}?target={target}")


class ShibbolethLogoutView(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.url = reverse("shibboleth:logout")
        cls.logout_url = settings.SHIBBOLETH_LOGOUT_URL.split("%s")[0]

    def test_get(self):
        response = self.client.get(self.url)
        assert response.status_code == HTTPStatus.FOUND
        assert response.url.startswith(self.logout_url)


class ShibbolethViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.url = reverse("shibboleth:info")
        cls.user = UserFactory()

    def setUp(self):
        self.client.force_login(self.user)

    @patch("apps.utils.context_processors.current_session")
    def test_redirect_with_next(self, mock_current_session):
        mock_current_session.return_value = MockSession()  # Mock a valid session
        next_url = "/some/next/page/"
        response = self.client.get(f"{self.url}?next={next_url}")
        assert response.status_code == HTTPStatus.FOUND
        assert response.url == next_url

    @patch("apps.utils.context_processors.current_session")
    def test_get(self, mock_current_session):
        mock_current_session.return_value = MockSession()  # Mock a valid session
        response = self.client.get(self.url)
        assert response.status_code == HTTPStatus.OK
        self.assertTemplateUsed(response, "shibboleth/user_info.html")

    @patch("apps.utils.context_processors.current_session")
    def test_context_user(self, mock_current_session):
        mock_current_session.return_value = MockSession()  # Mock a valid session
        response = self.client.get(self.url)
        assert response.status_code == HTTPStatus.OK
        assert response.context["user"] == self.user


class MockSession:
    id = 1
