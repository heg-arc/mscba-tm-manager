from django.contrib.auth.models import AbstractUser
from django.db import models
from django.urls import reverse
from django.utils.functional import cached_property
from django.utils.translation import gettext_lazy as _


class User(AbstractUser):
    """
    Default custom user model for MScBA TM Manager.
    If adding fields that need to be filled at user signup,
    check forms.SignupForm and forms.SocialSignupForms accordingly.
    """

    UNKNOWN = 0
    MALE = 1
    FEMALE = 2
    NA = 9
    GENDER = [
        (UNKNOWN, _("not known")),
        (MALE, _("male")),
        (FEMALE, _("female")),
        (NA, _("not applicable")),
    ]

    name = models.CharField(_("Name of User"), blank=True, max_length=255)
    gender = models.IntegerField(
        _("gender"),
        blank=True,
        default=UNKNOWN,
        choices=GENDER,
    )
    is_student = models.BooleanField(default=False)
    is_supervisor = models.BooleanField(default=False)
    is_expert = models.BooleanField(default=False)
    is_mh = models.BooleanField(default=False)
    profile_linkedin = models.URLField(
        max_length=200,
        verbose_name=_("LinkedIn profile"),
        null=True,
        blank=True,
    )

    majors = models.ManyToManyField("tm.Major", blank=True)

    attempt = models.ForeignKey(
        "tm.Attempt",
        related_name="users",
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        verbose_name=_("attempt"),
    )

    # Personal information for experts
    address = models.CharField(_("address"), max_length=255, null=True, blank=True)
    additional_address = models.CharField(
        _("additional address"),
        max_length=255,
        blank=True,
        null=True,
    )
    zip_code = models.CharField(
        _("zip code"),
        max_length=15,
        null=True,
        blank=True,
        help_text=_(
            "Please add the country code before the postal code if you are outside of "
            "Switzerland (e.g. F-71640).",
        ),
    )
    city = models.CharField(_("city"), max_length=255, null=True, blank=True)

    highest_degree = models.CharField(
        _("highest degree"),
        max_length=255,
        null=True,
        blank=True,
    )
    year_of_graduation = models.IntegerField(
        _("year of graduation"),
        null=True,
        blank=True,
    )
    current_job = models.CharField(
        _("current job"),
        max_length=255,
        null=True,
        blank=True,
    )
    current_company = models.CharField(
        _("current company"),
        max_length=255,
        null=True,
        blank=True,
    )

    profile_validated = models.BooleanField(default=False)
    profile_validation_date = models.DateTimeField(null=True, blank=True)

    language = models.CharField(_("language"), max_length=10, default="fr")

    class Meta:
        permissions = [
            ("can_search_directory", _("Can search peolple in the HES-SO directory")),
            ("can_adopt_user", _("Can adopt a user")),
            ("can_view_all_experts", _("Can view all experts")),
        ]

    @cached_property
    def address_line(self):
        line = ""
        if self.address:
            line += self.address + ", "
        if self.additional_address:
            line += self.additional_address + ", "
        if self.zip_code:
            line += self.zip_code + " "
        if self.city:
            line += self.city
        return line

    def __str__(self):
        return f"{self.name}"

    def get_absolute_url(self):
        """Get url for user's detail view.

        Returns:
            str: URL for user detail.

        """
        return reverse("users:detail", kwargs={"username": self.username})
