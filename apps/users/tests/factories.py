from collections.abc import Sequence
from datetime import timedelta
from typing import Any

from django.contrib.auth import get_user_model
from django.utils import timezone
from factory import Faker
from factory import SubFactory
from factory import post_generation
from factory.django import DjangoModelFactory

from apps.tm.models import Session
from apps.tm.tests.factories import CalendarFactory


class UserFactory(DjangoModelFactory):
    username = Faker("user_name")
    email = Faker("email")
    name = Faker("name")

    @post_generation
    def password(self, create: bool, extracted: Sequence[Any], **kwargs):  # noqa: FBT001
        password = (
            extracted
            if extracted
            else Faker(
                "password",
                length=42,
                special_chars=True,
                digits=True,
                upper_case=True,
                lower_case=True,
            ).evaluate(None, None, extra={"locale": None})
        )
        self.set_password(password)

    class Meta:
        model = get_user_model()
        django_get_or_create = ["username"]


class SessionFactory(DjangoModelFactory):
    name = f"{timezone.now().year}-{timezone.now().year + 1}"
    start_date = timezone.now().date() - timedelta(days=100)
    end_date = timezone.now().date() + timedelta(days=100)
    calendar = SubFactory(CalendarFactory)

    class Meta:
        model = Session
