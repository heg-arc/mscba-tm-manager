from django.urls import path

from apps.users.views import ExpertAutocomplete
from apps.users.views import ExpertCreateView
from apps.users.views import ExpertStandaloneCreateView
from apps.users.views import user_detail_view
from apps.users.views import user_redirect_view
from apps.users.views import user_update_view
from apps.users.views import validate_expert

app_name = "users"
urlpatterns = [
    path(
        "expert-autocomplete/<int:attempt_id>",
        ExpertAutocomplete.as_view(),
        name="expert-autocomplete",
    ),
    path("expert-create/", ExpertCreateView.as_view(), name="expert-create"),
    path(
        "expert-standalone-create/",
        ExpertStandaloneCreateView.as_view(),
        name="expert-standalone-create",
    ),
    path("~redirect/", view=user_redirect_view, name="redirect"),
    path("~update/", view=user_update_view, name="update"),
    path("~validate/", validate_expert, name="expert-validation"),
    path("<str:username>/", view=user_detail_view, name="detail"),
]
