from dal import autocomplete
from django.contrib import messages
from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.db import transaction
from django.db.models import Q
from django.shortcuts import redirect
from django.urls import reverse
from django.urls import reverse_lazy
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from django.views.generic import CreateView
from django.views.generic import DetailView
from django.views.generic import RedirectView
from django.views.generic import UpdateView
from django_addanother.views import CreatePopupMixin

from .forms import ExpertCreateForm  # , ExpertValidateForm
from .forms import ExpertUpdateForm  # , ExpertValidateForm
from .tasks import send_expert_welcome_message

User = get_user_model()


class UserDetailView(LoginRequiredMixin, DetailView):
    model = User
    slug_field = "username"
    slug_url_kwarg = "username"


user_detail_view = UserDetailView.as_view()


class UserUpdateView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = User
    success_message = _("Information successfully updated")

    def get_success_url(self):
        assert (
            self.request.user.is_authenticated
        )  # for mypy to know that the user is authenticated
        return self.request.user.get_absolute_url()

    def get_object(self):
        return self.request.user

    def get_form(self):
        if self.request.user.is_expert:
            self.form_class = ExpertUpdateForm
        else:
            self.form_class = ExpertUpdateForm
        return super().get_form()


user_update_view = UserUpdateView.as_view()


class UserRedirectView(LoginRequiredMixin, RedirectView):
    permanent = False

    def get_redirect_url(self):
        return reverse("users:detail", kwargs={"username": self.request.user.username})


user_redirect_view = UserRedirectView.as_view()


class ExpertAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        from apps.tm.models import Attempt

        if not self.request.user.is_authenticated:
            return User.objects.none()

        attempt_id = self.kwargs.get("attempt_id", None)

        qs = User.objects.filter(Q(is_expert=True) | Q(is_supervisor=True)).order_by(
            "name",
        )

        if attempt_id:
            # Get the attempt
            attempt = Attempt.objects.get(id=attempt_id)

            # Filter the queryset based on the major of the attempt
            qs = qs.filter(majors__in=[attempt.major])

        if self.q:
            qs = qs.filter(
                Q(name__istartswith=self.q)
                | Q(first_name__istartswith=self.q)
                | Q(last_name__istartswith=self.q),
            )

        return qs


class ExpertCreateView(
    LoginRequiredMixin,
    SuccessMessageMixin,
    CreatePopupMixin,
    CreateView,
):
    model = User
    form_class = ExpertCreateForm
    success_message = _("Information successfully updated")
    template_name = "users/expert_form.html"

    def form_valid(self, form):
        form.instance.is_expert = True
        form.instance.username = (
            f"{form.instance.first_name.lower()}.{form.instance.last_name.lower()}"
        )
        form.instance.name = f"{form.instance.first_name} {form.instance.last_name}"
        response = super().form_valid(form)
        self.object.set_unusable_password()
        self.object.majors.add(*self.request.user.majors.all())
        self.object.language = self.request.user.language
        self.object.save()
        transaction.on_commit(
            lambda: send_expert_welcome_message.delay(
                self.object.id,
                self.request.user.id,
            ),
        )
        return response


class ExpertStandaloneCreateView(LoginRequiredMixin, SuccessMessageMixin, CreateView):
    model = User
    form_class = ExpertCreateForm
    success_message = _("Information successfully updated")
    template_name = "users/expert_form.html"
    success_url = reverse_lazy("tm:experts")

    def form_valid(self, form):
        form.instance.is_expert = True
        form.instance.username = (
            f"{form.instance.first_name.lower()}.{form.instance.last_name.lower()}"
        )
        form.instance.name = f"{form.instance.first_name} {form.instance.last_name}"
        response = super().form_valid(form)
        self.object.set_unusable_password()
        self.object.majors.add(*self.request.user.majors.all())
        self.object.language = self.request.user.language
        self.object.save()
        transaction.on_commit(
            lambda: send_expert_welcome_message.delay(
                self.object.id,
                self.request.user.id,
            ),
        )
        return response


def validate_expert(request):
    user = request.user
    user.profile_validated = True
    user.profile_validation_date = timezone.now()
    user.save()
    messages.add_message(
        request,
        messages.SUCCESS,
        _("Your profile has been validated. Thank you!."),
    )
    return redirect("users:detail", username=user.username)
