from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.sites.models import Site
from django.urls import reverse
from django.utils import translation
from sesame.utils import get_query_string
from templated_email import send_templated_mail

from config import celery_app

User = get_user_model()


@celery_app.task()
def send_expert_welcome_message(user_id, supervisor_id, reminder=False):  # noqa: FBT002
    """After a new expert is created, send them a welcome message."""

    current_site = Site.objects.get_current()
    user = User.objects.get(pk=user_id)
    supervisor = User.objects.get(pk=supervisor_id)

    translation.activate(user.language)

    url = reverse("users:detail", kwargs={"username": user.username})
    url += get_query_string(user)

    send_templated_mail(
        template_name="expert_welcome_notification",
        from_email=settings.DEFAULT_FROM_EMAIL,
        recipient_list=[user.email],
        context={
            "user": user,
            "supervisor": supervisor,
            "contact": settings.DEFAULT_FROM_EMAIL,
            "reminder": reminder,
            "url": f"https://{current_site.domain}{url}",
        },
    )
