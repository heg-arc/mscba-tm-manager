from django.contrib import admin
from django.contrib.auth import admin as auth_admin
from django.contrib.auth import get_user_model
from django.utils.translation import gettext_lazy as _

from apps.users.forms import UserAdminChangeForm
from apps.users.forms import UserAdminCreationForm

User = get_user_model()


@admin.register(User)
class UserAdmin(auth_admin.UserAdmin):
    form = UserAdminChangeForm
    add_form = UserAdminCreationForm
    fieldsets = (
        (None, {"fields": ("username", "password")}),
        (_("Personal info"), {"fields": ("name", "first_name", "last_name", "email")}),
        (
            _("Permissions"),
            {
                "fields": (
                    "is_active",
                    "is_staff",
                    "is_superuser",
                    "groups",
                    "user_permissions",
                ),
            },
        ),
        (
            _("Roles"),
            {
                "fields": (
                    "is_student",
                    "is_supervisor",
                    "is_expert",
                    "is_mh",
                    "majors",
                ),
            },
        ),
        (
            _("Expert information"),
            {
                "fields": (
                    "address",
                    "additional_address",
                    "zip_code",
                    "city",
                    "highest_degree",
                    "year_of_graduation",
                    "current_job",
                    "current_company",
                    "profile_linkedin",
                    "profile_validated",
                    "profile_validation_date",
                ),
            },
        ),
        (
            _("Current Attempt"),
            {
                "fields": ("attempt", "language"),
            },
        ),
        (_("Important dates"), {"fields": ("last_login", "date_joined")}),
    )
    list_display = ["username", "name", "is_superuser"]
    search_fields = ["name", "username", "first_name", "last_name"]
