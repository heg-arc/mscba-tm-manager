# Generated by Django 4.1.7 on 2023-04-17 10:03

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("users", "0007_user_language"),
    ]

    operations = [
        migrations.AlterField(
            model_name="user",
            name="language",
            field=models.CharField(
                default="fr", max_length=10, verbose_name="language"
            ),
        ),
    ]
