# Generated by Django 3.2.13 on 2022-06-17 21:30

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("users", "0002_roles"),
    ]

    operations = [
        migrations.AddField(
            model_name="user",
            name="profile_hesso",
            field=models.URLField(blank=True, null=True, verbose_name="people HES-SO"),
        ),
    ]
