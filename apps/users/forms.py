from allauth.account.forms import SignupForm
from allauth.socialaccount.forms import SignupForm as SocialSignupForm
from crispy_forms.bootstrap import StrictButton
from crispy_forms.helper import FormHelper
from crispy_forms.layout import HTML
from crispy_forms.layout import Button
from crispy_forms.layout import Column
from crispy_forms.layout import Div
from crispy_forms.layout import Layout
from crispy_forms.layout import Row
from crispy_forms.layout import Submit
from django import forms
from django.contrib.auth import forms as admin_forms
from django.contrib.auth import get_user_model
from django.forms import ModelForm
from django.utils.translation import gettext_lazy as _

User = get_user_model()


class UserAdminChangeForm(admin_forms.UserChangeForm):
    class Meta(admin_forms.UserChangeForm.Meta):
        model = User


class UserAdminCreationForm(admin_forms.UserCreationForm):
    """
    Form for User Creation in the Admin Area.
    To change user signup, see UserSignupForm and UserSocialSignupForm.
    """

    class Meta(admin_forms.UserCreationForm.Meta):
        model = User

        error_messages = {
            "username": {"unique": _("This username has already been taken.")},
        }


class UserSignupForm(SignupForm):
    """
    Form that will be rendered on a user sign up section/screen.
    Default fields will be added automatically.
    Check UserSocialSignupForm for accounts created from social.
    """


class UserSocialSignupForm(SocialSignupForm):
    """
    Renders the form when user has signed up using social accounts.
    Default fields will be added automatically.
    See UserSignupForm otherwise.
    """


class ExpertCreateForm(ModelForm):
    def clean(self):
        cleaned_data = super().clean()
        first_name = cleaned_data.get("first_name")
        last_name = cleaned_data.get("last_name")
        email = cleaned_data.get("email")
        username = f"{first_name.lower()}.{last_name.lower()}"

        if User.objects.filter(username=username).exists():
            user_obj = User.objects.get(username=username)
            user = f"{user_obj.first_name} {user_obj.last_name} ({user_obj.email})"
            raise forms.ValidationError(
                _(
                    f"We already have a user with this name: {user}. Please select it from the list instead of creating a new one.",  # noqa: E501, INT001, S608
                ),
            )

        if User.objects.filter(email=email).exists():
            user_obj = User.objects.get(email=email)
            user = f"{user_obj.first_name} {user_obj.last_name} ({user_obj.email})"
            raise forms.ValidationError(
                _(
                    f"We already have a user with this name: {user}. Please select it from the list instead of creating a new one.",  # noqa: E501, INT001, S608
                ),
            )

    class Meta:
        model = User
        fields = ["first_name", "last_name", "email"]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.add_input(
            Button(
                "cancel",
                _("Cancel"),
                css_class="btn-outline-secondary",
                onclick="window.close()",
            ),
        )
        self.helper.add_input(Submit("submit", _("Submit")))


class ExpertUpdateForm(ModelForm):
    first_name = forms.CharField(
        label=_("First name(s)"),
        max_length=100,
        required=True,
    )
    last_name = forms.CharField(label=_("Last name"), max_length=100, required=True)
    email = forms.CharField(label=_("Email"), max_length=100, required=True)
    current_job = forms.CharField(
        label=_("Current occupation"),
        max_length=255,
        required=True,
    )
    current_company = forms.CharField(
        label=_("Current company"),
        max_length=255,
        required=True,
    )
    profile_linkedin = forms.URLField(
        label=_("LinkedIn profile"),
        max_length=255,
        required=False,
    )
    highest_degree = forms.CharField(
        label=_("Highest degree"),
        max_length=255,
        required=True,
    )
    year_of_graduation = forms.IntegerField(
        label=_("Year of graduation"),
        required=True,
    )
    address = forms.CharField(label=_("Address"), max_length=255, required=True)
    zip_code = forms.CharField(label=_("ZIP code"), max_length=100, required=True)
    city = forms.CharField(label=_("City"), max_length=255, required=True)

    class Meta:
        model = User
        fields = [
            "first_name",
            "last_name",
            "email",
            "profile_linkedin",
            "current_job",
            "current_company",
            "highest_degree",
            "year_of_graduation",
            "address",
            "additional_address",
            "zip_code",
            "city",
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = True
        self.helper.layout = Layout(
            Div(
                Div(
                    Div(
                        Div(
                            HTML(_("Contact Information")),
                            css_class="h3",
                        ),
                        Div(
                            Row(
                                Column("first_name", css_class="col-sm-6 col-xs-12"),
                                Column("last_name", css_class="col-sm-6 col-xs-12"),
                            ),
                            Row(
                                Column("email", css_class="col-lg-12 col-sm-12"),
                            ),
                        ),
                        css_class="card-body",
                    ),
                    css_class="card",
                ),
                Div(
                    Div(
                        Div(
                            HTML(_("Occupation")),
                            css_class="h3",
                        ),
                        Div(
                            Row(
                                Column("current_job", css_class="col-xs-12"),
                            ),
                            Row(
                                Column("current_company", css_class="col-xs-12"),
                            ),
                            Row(
                                Column("profile_linkedin", css_class="col-xs-12"),
                            ),
                        ),
                        css_class="card-body",
                    ),
                    css_class="card",
                ),
                Div(
                    Div(
                        Div(
                            HTML(_("Education")),
                            css_class="h3",
                        ),
                        Div(
                            Row(
                                Column(
                                    "highest_degree",
                                    css_class="col-lg-10 col-sm-10 col-xs-8",
                                ),
                                Column(
                                    "year_of_graduation",
                                    css_class="col-lg-2 col-sm-2 col-xs-4",
                                ),
                            ),
                        ),
                        css_class="card-body",
                    ),
                    css_class="card",
                ),
                Div(
                    Div(
                        Div(
                            HTML(_("Correspondence Address")),
                            css_class="h2",
                        ),
                        Div(
                            Row(
                                Column("address", css_class="col-sm-12"),
                            ),
                            Row(
                                Column("additional_address", css_class="col-sm-12"),
                            ),
                            Row(
                                Column(
                                    "zip_code",
                                    css_class="col-lg-4 col-sm-4 col-xs-4",
                                ),
                                Column("city", css_class="col-lg-8 col-sm-8 col-xs-8"),
                            ),
                        ),
                        css_class="card-body",
                    ),
                    css_class="card",
                ),
            ),
            StrictButton(_("Save"), type="submit", css_class="btn-success float-right"),
        )


class ExpertValidateForm(ModelForm):
    class Meta:
        model = User
        fields = ["profile_validated"]
        widgets = {
            "profile_validated": forms.HiddenInput(),
        }
