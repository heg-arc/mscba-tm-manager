from io import StringIO

from django.core.management import call_command
from django.test import TestCase


class PendingMigrationsTests(TestCase):
    def test_no_pending_migrations(self):
        out = StringIO()
        try:
            call_command(
                "makemigrations",
                "--check",
                stdout=out,
                stderr=StringIO(),
            )
        except SystemExit:
            output = out.getvalue()
            # Filter out the 'templated_email' migration output
            if "templated_email" not in output:
                raise AssertionError("Pending migrations:\n" + output) from None
