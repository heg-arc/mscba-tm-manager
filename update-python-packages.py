import os
import shutil
import subprocess
import sys
from pathlib import Path

import gitlab
import structlog

logger = structlog.get_logger(__name__)


def handle_branch(project, branch_name, default_branch):
    try:
        branch = project.branches.get(branch_name)
        msg = f"Branch '{branch_name}' already exists. Deleting it."
        logger.info(msg)
        branch.delete()
    except gitlab.exceptions.GitlabGetError:
        msg = f"Branch '{branch_name}' does not exist."
        logger.info(msg)

    msg = f"Creating branch '{branch_name}'."
    logger.info(msg)
    return project.branches.create({"branch": branch_name, "ref": default_branch})


def commit_and_create_merge_request(
    project,
    branch_name,
    default_branch,
):
    data = {
        "branch": branch_name,
        "commit_message": "Updated python packages",
        "actions": [
            {
                "action": "update",
                "file_path": "requirements/dev.txt",
                "content": Path.open("requirements/dev.txt").read(),
            },
            {
                "action": "update",
                "file_path": "requirements/base.txt",
                "content": Path.open("requirements/base.txt").read(),
            },
            {
                "action": "update",
                "file_path": "requirements/local.txt",
                "content": Path.open("requirements/local.txt").read(),
            },
            {
                "action": "update",
                "file_path": "requirements/production.txt",
                "content": Path.open("requirements/production.txt").read(),
            },
            {
                "action": "update",
                "file_path": ".pre-commit-config.yaml",
                "content": Path.open(".pre-commit-config.yaml").read(),
            },
        ],
    }
    project.commits.create(data)
    project.mergerequests.create(
        {
            "source_branch": branch_name,
            "target_branch": default_branch,
            "title": "New version of python packages and pre-commit hooks",
            "remove_source_branch": True,
        },
    )


def update_python_packages():
    gl = gitlab.Gitlab(
        "https://gitlab.com",
        private_token=os.getenv("PRIVATE_PIPUPDATE_TOKEN"),
    )
    project = gl.projects.get(os.getenv("CI_PROJECT_ID"))
    default_branch = os.getenv("CI_DEFAULT_BRANCH")
    branch_name = "python-packages-update"
    handle_branch(project, branch_name, default_branch)

    try:
        git_executable = shutil.which("git")
        if not git_executable:
            msg = "Git is not installed or not found in PATH"
            raise OSError(msg)
        subprocess.run([git_executable, "fetch", "origin", branch_name], check=True)  # noqa: S603
        subprocess.run([git_executable, "checkout", branch_name], check=True)  # noqa: S603
    except subprocess.CalledProcessError as e:
        msg = f"Error checking out branch '{branch_name}': {e}"
        logger.info(msg)
        sys.exit(1)

    commit_and_create_merge_request(project, branch_name, default_branch)


if __name__ == "__main__":
    update_python_packages()
